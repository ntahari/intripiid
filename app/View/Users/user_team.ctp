<div class="visible-lg"></br>
</br>
</br>
</div>
<div class="container master mabordure">
 <div class="mabordure-avatars visible-lg visible-md">
    <div align="right" class="mabordure-avatars-droite">
    <img src="<?php echo $this->Html->url($avatar_droite); ?>"></img>
    </div>
    <div align="left" class="mabordure-avatars-gauche">
    <img src="<?php echo $this->Html->url($avatar_gauche); ?>"></img>
    </div>
 </div>
  <nav class="navbar nav-selection" role="navigation">
   <div class="container-fluid">

     <!-- appear for medium and large screens -->
     <div class="visible-md visible-lg visible-sm visible-xs">

        <h1 class="navbar-text navbar-left"><?php echo $equipe['Team']['name'] ?> : <?php echo __("Sorties a Paris, stats, photos, fan club");?> </h1>
        <?php
        if ($my_team['TeamsHasPlayers']['team_id'] == $equipe['Team']['id'] AND $my_team['TeamsHasPlayers']['captain']==1 ) { ?>

           <button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#parametres"><?php echo __("Réglages");?></button>
<!--            <ul class="nav navbar-nav navbar-right">
<li class="dropdown">
  <button class="btn btn-default dropdown-toggle navbar-btn navbar-right" type="button" data-toggle="dropdown">
    <?php echo __("Notifications");?> <?php if ( $notifications >= 1 ) { ?><span class="label label-danger"><?php echo $notifications; ?> !</span><?php }  ?><span class="caret"></span>
  </button>
  <ul class="dropdown-menu" style="height:550px;width:400px;">
     <?php if ( isset($notifs[0]['playerid']) ) { ?>
         <div style="height:100%;overflow-y:auto;">
   <?php foreach($notifs as $n) { ?>
     <div class="notification <?php if ($owner['Player']['id'] == $player['Player']['id'] ) { if ( $n['vue'] == 0 ) { echo "notification-pasvue";} else echo "notification-vue"; } else echo "notification-vue"; ?>"><div class="container-fluid"><div class="row">


      <?php if ( $n['type'] == 'tripmedia' )  { ?>
       <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
       <a class="fancybox_galerie visible-lg visible-md hidden-sm hidden-xs" href="<?php echo $this->Html->url("/user/medias/details/".$n['media_id']); ?>" data-fancybox-type="iframe" ><?php  echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
       <a id="mini_tripmedia_<?php echo $n['media_id']; ?>" class="hidden-md hidden-lg visible-sm visible-xs fancybox_picture" href="<?php echo $this->Html->url($mediadb[$n['media_id']]); ?>" data-pseudo="<?php echo $playerdb[$n['playerid']]['pseudo']; ?>" data-player-id="<?php echo $n['playerid']; ?>" data-trip="" data-badtrip="" data-mission-id="" data-mission="" ><?php  echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
        </div>
       <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
       <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> a <?php if ( $n['tripvalue']==0) { ?>bad <?php } ?><?php echo __("tripé sur");?> <?php if ( isset($mymedia[$n['media_id']]) ) { ?><?php echo __("votre");?> <? } else { ?>cette <?php } ?><?php echo __("photo");?></p></div>
          <?php if ($n['timestamp'] != NULL ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
       <?php } ?>




      <?php if ( $n['type'] == 'tripmission' )  { ?>
       <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
       <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php  echo $this->Media->image($mediadb[$n['img_id']],100,100); ?></a>
       </div>
       <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
       <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> a <?php if ( $n['tripvalue']==0) { ?>bad <?php } ?><?php echo __("tripé sur cette mission");?> : <br> > <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php echo $n['missiontitle']; ?></a></p></div>
          <?php if ($n['timestamp'] != NULL ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
       <?php } ?>


       <?php if ( $n['type'] == 'missiondone' ) { /* partie pour les missions tentés et réussi */?>
          <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
          <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php echo $this->Media->image($mediadb[$n['img_id']],100,100); ?></a>
          </div>
          <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
          <?php if ( $n['success'] == 1 ) { ?>
          <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a réussi la mission");?> <br> > <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php echo $n['title']; ?></a></p></div>
          <?php } else { ?>
          <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a tenté la mission");?> <br> > <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php echo $n['title']; ?></a></p></div>
          <?php } ?>
          <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
       <?php } ?>

      <?php if ( $n['type'] == 'mediacom' ) { /* partie pour les commentaires de photos ou de missions */?>
         <?php if ( $n['media_type'] == 'Media' ) { /* si c'est une photo */ ?>
              <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
              <a class="fancybox_galerie visible-lg visible-md hidden-xs hidden-sm" href="<?php echo $this->Html->url("/user/medias/details/".$n['media_id']); ?>" data-fancybox-type="iframe" ><?php echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
              <a id="mini_mediacom_<?php echo $n['media_id']; ?>" class="fancybox_picture hidden-lg hidden-md visible-xs visible-sm" href="<?php echo $this->Html->url($mediadb[$n['media_id']]); ?>" data-pseudo="<?php echo $playerdb[$n['playerid']]['pseudo']; ?>" data-player-id="<?php echo $n['playerid']; ?>"><?php echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
              </div>
              <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
              <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a aussi commenté");?> <?php if ( isset($mymedia[$n['media_id']]) ) { ?><?php echo __("votre");?><?php } else { ?><?php echo __("cette");?><?php } ?> <?php echo __("photo");?> :<br> > <?php echo $n['comment'];?>...</p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
         <?php }
               if ($n['media_type'] == 'Mission' && isset($mediadb[$mdb[$n['media_id']]]) ) { /* si c'est une mission */?>
                <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
                <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['media_id']); ?>"><?php echo $this->Media->image($mediadb[$mdb[$n['media_id']]],100,100); ?></a>
                </div>
              <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
               <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a aussi commenté cette mission");?> : <br> > <?php echo $n['comment'];?>...</p></div>
               <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
         <?php } ?>
      <?php } ?>

      <?php if ( $n['type'] == 'upload_photo' ) { /* partie pour les upload de photos */?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a class="fancybox_galerie visible-lg visible-md hidden-xs hidden-sm" href="<?php echo $this->Html->url("/user/medias/details/".$n['media_id']); ?>" data-fancybox-type="iframe" ><?php echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
            <a id="mini_upload_<?php echo $n['media_id']; ?>" class="fancybox_picture hidden-lg hidden-md visible-xs visible-sm" href="<?php echo $this->Html->url($mediadb[$n['media_id']]); ?>" data-pseudo="<?php echo $playerdb[$n['playerid']]['pseudo']; ?>" data-player-id="<?php echo $n['playerid']; ?>" ><?php echo $this->Media->image($mediadb[$n['media_id']],100,100); ?></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a ajouté une photo pour sa mission");?> <br> > <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$n['missionid']); ?>"><?php echo $n['missiontitle']; ?></a></p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>

     <?php if ( $n['type'] == 'levelup' ) { ?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><img src="<?php echo $this->Html->url($playerdb[$n['playerid']]['profil_pict']); ?>" height="100px" width="100px"></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("est maintenant");?> <br> > <?php echo $n['grade']; ?> (<?php echo __("niveau");?> <?php echo $n['level']; ?>)</p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>

     <?php if ( $n['type'] == 'palierup' ) { ?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><img src="<?php echo $this->Html->url($playerdb[$n['playerid']]['profil_pict']); ?>" height="100px" width="100px"></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a atteint le palier");?> <?php echo $n['palier']; ?></p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>

     <?php if ( $n['type'] == 'fan' ) { ?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><img src="<?php echo $this->Html->url($playerdb[$n['playerid']]['profil_pict']); ?>" height="100px" width="100px"></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("suit maintenant vos aventures");?></p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>

      <?php if ( $n['type'] == 'deviselike' ) { ?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><img src="<?php echo $this->Html->url($playerdb[$n['playerid']]['profil_pict']); ?>" height="100px" width="100px"></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> a <?php if ($n['value']==0) { ?>bad <?php } ?><?php echo __("tripé sur votre devise");?></p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>
         <?php if ( $n['type'] == 'profilcomm' ) { ?>
            <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2">
            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><img src="<?php echo $this->Html->url($playerdb[$n['playerid']]['profil_pict']); ?>" height="100px" width="100px"></a>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <p><a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$n['playerid']]['pseudo']))); ?>"><?php echo $playerdb[$n['playerid']]['pseudo']; ?></a> <?php echo __("a écrit sur votre livre d'or");?></p></div>
              <?php if ($n['timestamp'] != 0 ) { ?><div class="notification-time"><span data-livestamp="<?php echo $n['timestamp']; ?>" style="font-family:'Patua One';font-size:10px;"</span></div><?php } ?>
      <?php } ?>


     </div></div></div>
    <?php } ?>
   </div>
  <?php } else { ?>
     </br></br></br></br></br>
       <p style="text-align:center;color:#79757f;font-family:'Patua One';"><?php echo __("Pas de notifications");?></p>
 <?php } ?>
 </ul>
</li>
</ul> -->
           <a href="#"><button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#fans">Membres de l'équipe </button></a>

           <?php if ( $player['Player']['id']==129 ) { ?>
            <a href="javascript:;" onclick="questionnaire(<?php echo $player['Player']['id'];?>, 1);"><button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs"><?php echo __("Questionnaire");?></button></a>

        <?php } } else { ?>
        <a href="#"><button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#fans"><?php echo __("membres de l'équipe");?></button></a>
        <?php } ?>
     </div>
    </div>
    </nav>

  <hr>

<div class="container-fluid">
<div class="row">
<div class="col-md-4 col-lg-4" style="height:100%">







<div class="container-fluid" style="height:100%;">
  <div class="row" style="height:100%;">
    <ul class="nav nav-tabs nav-profil">
       <!-- <li class="pull-left" style="width:50px;heigth:40px"><img height="50px" src="<?php echo $this->Html->url($equipe['Team']['logo']); ?>"></img></li> -->
       <li class="pull-left"><h2><?php echo $equipe['Team']['name'] ?></h2></li>


	   <li class="pull-right">
	   <?php if ( $my_team['TeamsHasPlayers']['team_id'] == $equipe['Team']['id'] AND $my_team['TeamsHasPlayers']['captain']==1) { ?>
           <button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#parametres"><?php echo __("Photo profil");?></button>
       <?php }  ?> </li>

    </ul>
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 resume-avatar">
      <!-- Tab panes -->
        <div class="tab-content" align="center">
          <div class="tab-resume tab-pane active" id="encours">
            <?php if (!empty($equipe['Team']['logo'])) { ?>
            
            <img id ="picTeam" style="width:100%; height:100%;vertical-align:middle" src="<?php echo $this->Html->url('/img/uploads/team/'.$equipe['Team'][id].'/'.$equipe['Team']['logo']); ?>" class="img-responsive">
            
            <?php } else { ?>
            
            <img id ="picTeam" style="width:100%; height:100%;vertical-align:middle" src="<?php echo $this->Html->url('/img/uploads/team/no-image.jpg'); ?>" class="img-responsive">
            <?php  } ?>
          </div><!-- fin tab encours -->
		  <!-- <script>
      var height = document.images["picTeam"].height;
      var padding1 = 300 - height;
      var padding2 = padding1 / 2;
      if (padding2 >= 0){
            document.getElementById("encours").style.padding = padding2+"px 0 0 0";
      }
      
      
      </script> -->

        </div><!-- fin tab-content -->
        <!-- <ul class="nav nav-tabs" style="font-family: 'Patua One';">
              <li ><a href="#histoire" title="C'est l'histoire de laaaaaa viiieeeeeee !!" data-toggle="tab">Histoire</a></li>
              <li class="active"><a href="#encours" title="Votre avatar actuel !" data-toggle="tab"><?php echo __("Avatar actuel");?></a></li>
              <?php foreach ( $history_avatar as $hist) {
                  if ( $hist['key'] == 1 ) { ?>
                     <li><a href="#niveau0" title="Avatar niveau 0" data-toggle="tab"><?php echo __("Niveau 1");?></a></li>
                  <?php } else {
                     if ( $hist['key'] ==2 ) $i=4;
                     if ( $hist['key'] ==3 ) $i=8;
                     if ( $hist['key'] ==4 ) $i=12;
                     if ( $hist['key'] ==5 ) $i=16;
                     if ( $hist['key'] ==6 ) $i=20;
                     if ( $hist['key'] ==7 ) $i=21;
                     ?>

                     <li><a href="#niveau<?php echo $i; ?>" title="Avatar niveau <?php echo $i; ?>" data-toggle="tab"><?php echo $i; ?></a></li>
                  <?php } ?>
              <?php } ?>
        </ul> -->
    </div><!-- fin resume-avatar -->




    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color:#fff;">
      <h2 style="margin:0;padding-top:10px;padding-left:10px;padding-bottom:7px;color:#ea3341;height:15%">
        <?php if (  $my_team['TeamsHasPlayers']['team_id'] == $player['Player']['id']   ) { ?>
        <?php echo __("Notre devise");?><?php } else { ?><?php echo __("Sa devise");?><?php } ?>
        <?php if ( $my_team['TeamsHasPlayers']['team_id'] == $equipe['Team']['id'] AND $my_team['TeamsHasPlayers']['captain']==1) { ?>
        <button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#parametres"><?php echo __("Changer la devise");?></button>
        <?php } ?>
      </h2>

      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 devise">
        <?php if ( isset($equipe['Team']['devise']) ) echo stripslashes($equipe['Team']['devise']); else echo "< Définir une devise >"; ?>
      </div><!-- fin devise -->

      <!-- <?php if ( isset($owner['Devise']['id']) ) { ?>
      <ul class="nav nav-pills nav-trip-profil">
          <li><a class="like-galery" href="javascript:;" onclick="handle_devise_like(1,<?php echo $owner['Devise']['id']?>)"><img src="/img/missiondetails/trip-noir.png"><br><?php echo __("Trip");?> !</a></li>
          <li><span class="nbtrip" data-container="body" data-toggle="tooltip" data-placement="bottom" data-original-title="Vous avez déja tripé sur cette devise !" data-trigger="manual" id="display_devise_like"><?php echo $devise_trip; ?></span></li>
          <li><a class="like-galery" href="javascript:;" onclick="handle_devise_like(0,<?php echo $owner['Devise']['id']?>)"><img src="/img/missiondetails/badtrip-noir.png"><br><?php echo __("Bad trip");?></a></li>
          <li><span class="nbbadtrip" data-container="body" data-toggle="tooltip" data-placement="bottom" data-original-title="Vous avez déja bad tripé sur cette devise !" data-trigger="manual" id="display_devise_dislike"><?php echo $devise_badtrip; ?></span></li>

      </ul>
      <?php } ?> -->

    </div><!-- fin background-color:#fff; -->

    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 stats-joueur">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 span6-profil-droite" style="height:100%">
            <ul class="description-profil">
              <li>
			  <a href="http://www.intripid.fr/user/users/classement?page=1&team=team" target="_blank"><span class="gris-profil"><?php echo __("Classement saison en cours");?></span></a>
                <span class="rouge-profil pull-right"><?php if ( $exp_byTeamId[$equipe['Team']['id']] == NULL ) echo __("Non-classé"); else echo $classementByTeam[$equipe['Team']['id']]; ?></span>

              </li>

              <li>
			          <span class="gris-profil"><?php echo __("Expérience saison en cours");?></span>
                <span class="rouge-profil pull-right"><?php if ( $exp_byTeamId[$equipe['Team']['id']] == NULL ) echo '0'; else echo $exp_byTeamId[$equipe['Team']['id']]; ?> points</span>

              </li>

                <span class="gris-profil"><?php echo __("Missions réusssies saison en cours");?></span>
                <span class="rouge-profil pull-right"><?php echo $victoire_team ?></span>
              </li>
              <li>
                <span class="gris-profil"><?php echo __("Missions totales saison en cours");?></span>
                <span class="rouge-profil pull-right"><?php echo $missiondones ?></span>
              </li>

            </ul>
          </div>

        </div><!-- fin row -->
      </div><!-- fin container-fluid -->
    </div><!-- fin stats-joueur -->



  </div><!-- fin row -->
</div><!-- fin container -->
</div> <!-- fin colonne gauche -->


<!-- debut colonne MID -->
<div class="col-md-4 col-lg-4" style="padding-left:10px;height:100%">
  <div class="commentaires-joueurs" style="border:1px solid #FFD800;">
    <h2 style="margin:0;padding-top:7px;padding-left:10px;padding-bottom:7px;color:#ea3341;height:13%;background-color:#FFD800;"><?php echo __("Livre d'or");?>

      <button type="button" class="btn btn-default navbar-btn navbar-right devise-new" data-toggle="modal" data-target="#profil_comment"><?php echo __("Commenter");?></button>
    </h2>

      <div class="container-fluid">
        <div class="row">
         <?php if ( $profil_comments != NULL ) { foreach ( $profil_comments as $dc ) { ?>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 commentaires-joueurs-mur">
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">

            <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$dc['Profilcomm']['playerfrom_id']]['pseudo']))); ?>"><img height="50px" width="50px" src="<?php echo $this->Html->url($playerdb[$dc['Profilcomm']['playerfrom_id']]['profil_pict']); ?>"></a>
            </div>
            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 commentaires-joueurs-mur-nom">
              <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$playerdb[$dc['Profilcomm']['playerfrom_id']]['pseudo']))); ?>"><?php echo $playerdb[$dc['Profilcomm']['playerfrom_id']]['pseudo']; ?></a>
              <span class="timer-livreor" data-livestamp="<?php echo $dc['Profilcomm']['time']; ?>"</span>
            </div>
            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10" style="word-wrap: break-word;">
             <?php echo stripslashes($dc['Profilcomm']['commentaire']); ?>
            </div>
            <hr class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          </div>
         <?php } } ?>
        </div>
      </div>
   </div><!-- fin commentaires-joueurs -->




  <div class="col-md-12 col-lg-12 hidden-sm hidden-xs" style="background-color:#ea3341;">
    <h2 style="margin:0;padding-top:10px;padding-left:10px;padding-bottom:7px;color:#fff;height:15%"> <?php if (  $my_team['TeamsHasPlayers']['team_id'] == $equipe['Team']['id']  ) { ?><?php echo __("Nos");?><?php } else { ?><?php echo __("Leurs");?><?php } ?> <?php echo __("aventures");?></h2>
     <!-- Tab panes -->
        <div class="tab-content" style="background-color:#eeebf3;">
          <div class="tab-resume tab-pane active " id="plustripe" style="border: 7px solid #ea3341;">
            <div class="container-fluid">
              <div class="row">
                  <?php if ( count($dbownergallery)>0 ) {  $i=0; ?>
                      <?php foreach($dbownergallery as $pict):
                         if(isset($pict['file'])){  $i=$i+1;?>
                           <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                              <a class="visible-md visible-lg hidden-sm hidden-xs fancybox_galerie" rel="gallery" href="<?php echo $this->Html->url("/user/medias/details/".$pict['media_id']); ?>" data-fancybox-type="iframe"><?php  echo $this->Media->image($pict['file'],183,148); ?></a>
                              <a id="owner-gallery-<?php echo $pict['Media_id']; ?>" class="hidden-md hidden-lg visible-sm visible-xs fancybox_picture" rel="gallery" href="<?php echo $this->Html->url($pict['file']); ?>" data-pseudo="<?php echo $owner['Player']['pseudo']; ?>" data-player-id="<?php echo $owner['Player']['id']; ?>" data-trip="<?php echo $pict['like']; ?>" data-badtrip="<?php echo $pict['dislike']; ?>" data-mission-id="<?php echo $pict['ref_id']; ?>" data-mission="<?php echo $missionTitle[$pict['ref_id']]; ?>"><?php  echo $this->Media->image($pict['file'],183,148); ?></a>
                           </div>
                         <?php }
                       endforeach; ?>
                  <?php } else { ?>
                  </br></br>
                  <p style="text-align:center;color:#000;font-family:'Patua One';"><?php echo __("Pas de photos soumises");?></p>
                  <?php } ?>
              </div>
            </div> <!-- fermeture container-fluid -->

          </div><!-- fin tab plustripe -->

          <div class="tab-resume tab-pane" id="photosrecentes" style="border: 7px solid #ea3341;"8>
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <a href=""><img src="<?php echo $this->Html->url("/img/profil/resume-avatar/resume-avatar.jpg"); ?>" class="img-responsive"></a>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <a href=""><img src="<?php echo $this->Html->url("/img/profil/resume-avatar/resume-avatar.jpg"); ?>" class="img-responsive"></a>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <a href=""><img src="<?php echo $this->Html->url("/img/uploads/missiondone/2014/03/IMG_0701_110x82.jpg"); ?>" class="img-responsive">  </a>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                  <a href=""><img src="<?php echo $this->Html->url("/img/profil/resume-avatar/resume-avatar.jpg"); ?>" class="img-responsive">  </a>
                </div>
              </div><!-- fermeture row -->
            </div> <!-- fermeture container-fluid -->
          </div><!-- fin tab photosrecentes -->
        </div><!-- fin tab-content -->

        <!--<ul class="nav nav-tabs nav-photos" style="font-family: 'Patua One';background-color:#eeebf3;">
              <li class="active"><a href="#plustripe" title="Photos les plus Tripées" data-toggle="tab">Plus Tripées</a></li>
              <li><a href="#photosrecentes" title="Les photos récemment ajoutées" data-toggle="tab">Photos récentes</a></li>
              <li><a href="#" title="Voir toutes les photos">Voir plus de Photos</a></li>
        </ul>-->
  </div><!-- fin span12 photos -->




   </div><!-- fin colonne milieu -->





<div class="col-md-4 col-lg-4" style="height:100%;padding-left:10px">
    <div class="col-md-12 col-lg-12 ">
    <h2 style="padding-left:10px;margin:0;padding-top:10px;color:#ea3341;height:20%;"><?php echo __("Resumé de");?> <?php if ( $equipe['Team']['id']==$my_team['TeamsHasPlayers']['team_id'] ) { ?><?php echo __("nos");?><?php } else { ?><?php echo __("leurs");?><?php } ?> <?php echo __("talents");?> !</h2>
    <!-- Nav tabs -->
<ul class="nav nav-tabs" style="font-family: 'Patua One';">
  <li class="active"><a href="#missions" title=" ... que vous avez accepté !" data-toggle="tab"><?php echo __("Missions");?></a></li>
  <!--<li><a href="#hautfait" title="&#9834; &#9835; Ou sont les noooooobs .... &#9834; &#9835;  " data-toggle="tab">Hauts fait</a></li>
  <li ><a href="#histoire" title="C'est l'histoire de laaaaaa viiieeeeeee !!" data-toggle="tab">Histoire</a></li>
  <li><a href="#challenges" title="Tu es un écumeur de mondes !" data-toggle="tab">Challenges</a></li>-->
</ul>

<!-- Tab panes -->
<div class="tab-content">
   <?php if ( $equipe['Team']['id']==$my_team['TeamsHasPlayers']['team_id'] ) { ?>



<!-- tab missions -->

  <div class="tab-resume tab-resume-resume-profil tab-pane active " id="missions">
    <div class="container-fluid">
      <div class="row">
          <?php $i=1;
                foreach($missiondonelist as $m) : ?>

          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#mission-resume-<?php echo $i; ?>">
            <nav class="nav">
              <ul class="nav navbar-nav">
                 <?php if ( $m['succes'] == 1 ) {  ?>
                    <li><a href="#" style="padding-left:2px !important;" title="Mission réussie"><img height="35px;" src="<?php echo $this->Html->url("/img/badges/missionreussie.png"); ?>"></a></li>
                  <?php } else { ?>
                    <li><a href="#" style="padding-left:2px !important;" title="Mission échouée"><img height="35px;" src="<?php echo $this->Html->url("/img/badges/missionfail.png"); ?>"></a></li>
                  <?php } ?>
                <li>
                  <h4 class="<?php echo $missionNat[$m['missiondon_id']]; ?>-lien"><?php echo $m['mission_title'];?></h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <li class="pull-right"> <h5><?php echo date('d/m/Y',$m['missiondone_time']) ;?><span class="caret"></span></h5></li>
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="mission-resume-<?php echo $i; ?>" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo $missionsSDesc[$m['missiondon_id']]; ?>
            <br>
            <a class="<?php echo $missionNat[$m['missiondon_id']]; ?>-lien" href="<?php echo $this->Html->url("/user/missions/missionsingle/".$m['missiondon_id']); ?>"  target="_blank"><?php echo __("Aller voir la mission");?></a>
          </div>


          <?php $i=$i+1;
           endforeach; ?>



      </div><!-- fermeture row -->
    </div> <!-- fermeture container-fluid -->
  </div>

  <!-- fin tab missions -->


  <?php } else { ?>
  <div class="tab-resume tab-resume-resume-profil tab-pane active" id="missions"><!-- tab missions -->
<div class="container-fluid">
      <div class="row">
          <?php $i=1; foreach($missiondonelist as $m) : ?>

          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#mission-resume-<?php echo $i; ?>"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                  <?php if ( $m['succes'] == 1 ) {  ?>
                    <li><a href="#" style="padding-left:2px !important;" title="Mission réussie"><img height="35px;" src="<?php echo $this->Html->url("/img/badges/missionreussie.png"); ?>"></a></li>
                  <?php } else { ?>
                    <li><a href="#" style="padding-left:2px !important;" title="Mission échouée"><img height="35px;" src="<?php echo $this->Html->url("/img/badges/missionfail.png"); ?>"></a></li>
                  <?php } ?>

                <li>
                  <h4 class="<?php echo $missionNat[$m['missiondon_id']]; ?>-lien"><?php echo $m['mission_title'];?></h4> <!-- mettre la nature de la mission dans la classe (ripaille/festif/etc.) -->
                </li>
                <li class="pull-right"> <h5><?php echo date('d/m/Y',$m['missiondone_time']) ;?><span class="caret"></span></h5></li>
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="mission-resume-<?php echo $i; ?>" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo $missionsSDesc[$m['missiondon_id']]; ?>
            <br>
            <a href="<?php echo $this->Html->url("/user/missions/missionsingle/".$m['missiondon_id']); ?>" class="<?php echo $missionNat[$m['missiondon_id']]; ?>-lien" target="_blank"><?php echo __("Aller voir la mission");?></a><!-- mettre la nature de la mission dans la classe (ripaille/festif/etc.)-->
          </div>


          <?php $i=$i+1;  endforeach; ?>



      </div><!-- fermeture row -->
    </div> <!-- fermeture container-fluid -->
  </div><!-- fin tab missions -->
                <?php } ?>

  <div class="tab-resume tab-resume-resume-profil tab-pane" id="hautfait" style="font-family:'Patua One';">
    <div class="container-fluid">
      <div class="row">


          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#haut-fait-1"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                <li><a href="#" style="padding-left:2px !important;padding-bottom:5px !important;padding-top:5px !important;" title="un truc"><img height="35px;" src="<?php echo $this->Html->url("/img/badges/festif3.png"); ?>"></a></li>
                <li>
                  <h4 class="lien-pasfait"><?php echo __("Le Roi des Tavernes");?></h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <!--<li class="pull-right"> <h5>18/08/2014<?php //echo $m['Missiondone']['time'];?><span class="caret"></span></h5></li>-->
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="haut-fait-1" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo __("Avoir effectué 50 missions de type Festif !");?>
            <br>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="24" aria-valuemin="0" aria-valuemax="100" style="width: 24%;">
                12/50
              </div>
            </div><!-- fin progress -->
          </div><!-- fin haut-fait-1-->

          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#haut-fait-2"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                <li><a href="#" style="padding-left:2px !important;padding-bottom:5px !important;padding-top:5px !important;" title="un truc"><img src="<?php echo $this->Html->url("/img/badges/tampon-1.png"); ?>"></a></li>
                <li>
                  <h4 class="lien-pasfait">Paris Klingon</h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <!--<li class="pull-right"> <h5>18/08/2014<?php //echo $m['Missiondone']['time'];?><span class="caret"></span></h5></li>-->
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="haut-fait-2" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo __("Avoir effectué 50 missions de type Emplette !");?>
            <br>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="24" aria-valuemin="0" aria-valuemax="100" style="width: 24%;">
                05/50
              </div>
            </div><!-- fin progress -->
          </div><!-- fin haut-fait-2-->


          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#haut-fait-3"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                <li><a href="#" style="padding-left:2px !important;padding-bottom:5px !important;padding-top:5px !important;" title="un truc"><img src="<?php echo $this->Html->url("/img/badges/tampon-1.png"); ?>"></a></li>
                <li>
                  <h4 class="lien-pasfait">Don Juan</h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <!--<li class="pull-right"> <h5>18/08/2014<?php //echo $m['Missiondone']['time'];?><span class="caret"></span></h5></li>-->
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="haut-fait-3" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo __("Avoir effectué toutes les missions à thème romantique listées ci-dessous");?> :
            <br>
            <span class="glyphicon glyphicon-ok"></span><a href="#" class="lien-fait"> <?php echo __("La nuit de la Drague");?></a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-ok"></span><a href="#" class="lien-fait"> <?php echo __("Photo de Star");?></a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-remove"></span><a href="#" class="lien-pasfait"> <?php echo __("La Route du Paradis");?></a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-remove"></span><a href="#" class="lien-pasfait"> <?php echo __("Double Bonnet");?></a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-remove"></span><a href="#" class="lien-pasfait"> <?php echo __("Cérémonime de la Rose");?></a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-ok"></span><a href="#" class="lien-fait"> <?php echo __("On smack ou pas");?> ?</a><!-- mettre la nature de la mission dans la classe --><br>
            <span class="glyphicon glyphicon-ok"></span><a href="#" class="lien-fait"> <?php echo __("Reine de la Salsa");?></a><!-- mettre la nature de la mission dans la classe -->
          </div><!-- fin haut-fait-3-->

          <div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="<?php echo __("Voir les détails de la mission");?> !" data-target="#haut-fait-4"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                <li><a href="#" style="padding-left:2px !important;padding-bottom:5px !important;padding-top:5px !important;" title="un truc"><img src="<?php echo $this->Html->url("/img/badges/tampon-1.png"); ?>"></a></li>
                <li>
                  <h4 class="lien-fait">Obélix</h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <!--<li class="pull-right"> <h5>18/08/2014<?php //echo $m['Missiondone']['time'];?><span class="caret"></span></h5></li>-->
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="haut-fait-4" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo __("Avoir effectué 50 missions de type Ripaille");?> !
            <br>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                50/50
              </div>
            </div><!-- fin progress -->
          </div><!-- fin haut-fait-4-->


<div class="liste-mission col-xs-12 col-sm-12 col-md-12 col-lg-12" data-toggle="collapse" title="Voir les détails de la mission !" data-target="#haut-fait-5"><hr>
            <nav class="nav">
              <ul class="nav navbar-nav">
                <li><a href="#" style="padding-left:2px !important;padding-bottom:5px !important;padding-top:5px !important;" title="un truc"><img src="<?php echo $this->Html->url("/img/badges/tampon-1.png"); ?>"></a></li>
                <li>
                  <h4 class="lien-fait"><?php echo __("Amateur culinaire");?></h4> <!-- mettre la nature de la mission dans la classe -->
                </li>
                <!--<li class="pull-right"> <h5>18/08/2014<?php //echo $m['Missiondone']['time'];?><span class="caret"></span></h5></li>-->
              </ul>
            </nav>

          </div><!-- fin liste-mission -->
          <div id="haut-fait-5" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-mission">
            <hr>
            <?php echo __("Avoir effectué 10 missions de type Ripaille");?> !
            <br>
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                10/10
              </div>
            </div><!-- fin progress -->
          </div><!-- fin haut-fait-5-->

      </div><!-- fermeture row -->
    </div> <!-- fermeture container-fluid -->
  </div>
  <div class="tab-resume tab-resume-resume-profil tab-pane" id="challenges" style="font-family:'ShortStackRegular';">...</div>
  <div class="tab-resume tab-resume-resume-profil tab-pane " id="histoire" style="font-family:'ShortStackRegular';"><?php echo __("Le tout, tout tout tout ensemble ..HEY HEY ! Le tout, tout tout tout ensemble ..HEY HEY !Le tout, tout tout tout ensemble ..HEY HEY !Le tout, tout tout tout ensemble ..HEY HEY !Le tout, tout tout tout ensemble ..HEY HEY");?> !</div>
</div>
      </div><!-- fin span12 résumé-->


</div> <!-- fin colonne droite -->


  </div><!-- fin row -->

<!--   liste memebre de l'equipe -->
<div id="fans" class="modal fade bs-example-modal-lg" aria-labelledby="fans" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
       <div class="container-fluid">
        <div class="row">
        <div id="propfriends_content_div">
          <?php foreach($joueur_team as $membre) { ?>
             <div id="container_<?php echo $membre['id']; ?>" class="col-lg-5 col-md-5 col-sm-12 col-xs-12 conteneurrelationorange">
              <div class="container-fluid">
               <div class="row">
                <div class="classerelation">
                   <img class="relations_avatar_mini" src="<?php echo $this->Html->url($membre['avatar_mini']); ?>">
                </div>
               <div class="col-lg-12 pseudorelationorange">
         <p><?php echo $membre['pseudo']; ?> !</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 avatarrelation">
        <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$membre['pseudo']))); ?>" title="Voir le profil de ce héros <?php echo $membre['pseudo']; ?> !">
          <img height="100px"  src="<?php echo $this->Html->url($membre['profil_pict']); ?>">
        </a>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 infosrelationorange">
          <ul>
            <!--<li>Mon super titre de la mort</li>-->
            <!--<li>&nbsp;</li>-->
            <li><?php echo $membre['classe']; ?></li>
            <li><span class="grisrelations"><?php echo $membre['grade']; ?></span></li>
            <li><?php echo __("Niveau");?> : <span class="grisrelations"><?php echo $membre['level']; ?></span></li>
            <li><?php echo __("Points saison en cours ");?> : 
            <span class="grisrelations">
            <?php if ($ExpByPlayer[$membre['id']]['EXP_encours'] == NULL) 
            echo "0";                                     
            else echo $ExpByPlayer[$membre['id']]['EXP_encours']; ?></span></li>
            <li><?php echo __("Classement en cours");?> : <span class="grisrelations">
              <?php 
                  if ($ExpByPlayer[$membre['id']]['EXP_encours'] == NULL) {
                    echo __("non classé");
                  }else
                    echo $ExpByPlayer[$membre['id']]['classement_encours']; 
              ?>
            </span></li>
          </ul>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  missionsrelationorange">
          <?php echo __("Missions réussies saison en cours");?> : <span class="orangerelations"><?php echo $membre['victoires']; ?></span>
        </div>

            </div><!-- fin row -->
           </div><!-- fin container-fluid -->
          </div><!-- fin conteneurrelation -->
        <?php  if ($my_team['TeamsHasPlayers']['Player']['id'] == $membre['id'] AND $my_team['TeamsHasPlayers']['captain']==1) {?>
          <br/><br/>
        <?php }} ?>
      </div><!-- fin propfriends -->
      </div>
      </div>
     </div><!-- modal body -->

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    </div>

   </div><!-- fin modal content -->
 </div><!-- fin modal-dialog -->
</div><!-- fin div modal fans -->




<div id="new_devise" class="modal fade bs-example-modal-sm" aria-labelledby="new_devise" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
       <div class="container-fluid">
        <div class="row">
        <div id="formulaire_newdevise_failure" class="alert alert-danger" style="display:none;font-family:'ShortStackRegular';text-align:center;"></div>
        <form class="form-horizontal" id="formulaire_newdevise">
         <div class="form-group" style="margin-left:0;margin-right:0;font-family:'ShortStackRegular';">
            <div class="controls">
               <?php echo $this->Form->input('devise',array('label'=>"Ma devise : ", 'minlength'=>3, 'maxlength'=>100, 'placeholder'=>"Ma nouvelle devise", 'style' => 'width:100%')); ?>
            </div>
        </div>
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Annuler");?></button>
         <button type="submit" class="btn btn-primary" ><?php echo __("Je valide");?></button>
        </form>
       </div>
       </div>
      </div>
    </div>
   </div>
</div>


<div id="profil_comment" class="modal fade bs-example-modal-sm" aria-labelledby="profil_comment" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
       <div class="container-fluid">
        <div class="row">
        <div id="formulaire_profilcomment_failure" class="alert alert-danger" style="display:none;font-family:'ShortStackRegular';text-align:center;"></div>
        <form class="form-horizontal" id="formulaire_profilcomment">
         <div class="form-group" style="margin-left:0;margin-right:0;font-family:'ShortStackRegular';">
            <div class="controls">
               <?php echo $this->Form->input('profil_commentaire',array('label'=>"Mon commentaire : ", 'minlength'=>3, 'maxlength'=>255, 'placeholder'=>"Mon commentaire", 'style' => 'width:100%')); ?>
            </div>
        </div>
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Annuler");?></button>
         <button type="submit" class="btn btn-primary" ><?php echo __("Je valide");?></button>
        </form>
       </div>
       </div>
      </div>
    </div>
   </div>
</div>

</div><!-- fin container-fluid -->


<div id="parametres" class="modal fade bs-example-modal-lg" aria-labelledby="myParametres" aria-hidden="true">
 <div class="modal-dialog modal-lg">
  <div class="modal-content">
   <div class="modal-body">

   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h2><?php
                echo ($my_team['TeamsHasPlayers'])? 'Mon ' : 'Création de mon ' ?><?php echo __("équipe");?></h2>
    <form class="form_create_team"  method="post" enctype="multipart/form-data">
      <input type="hidden" name="teams[id]" value="<?php echo $my_team['Teams']['id'] ?>">
      <h4><?php echo __("Option générale de l'équipe");?> :</h4>
      <div class="form-group">
        <label><?php echo __("Nom de l'équipe");?></label>
        <input class="form-control" type="text" name="teams[name]" value="<?php echo $my_team['Teams']['name'] ?>">
      </div>
      <div class="form-group">
        <label><?php echo __("Devise de l'équipe");?></label>
        <input class="form-control" type="text" name="teams[devise]" value="<?php echo $my_team['Teams']['devise'] ?>">
      </div>


      <!-- t<div id="profil-img-container" style="background-color:#E0ECEC;padding-bottom:15px;">
                        <script>
                         var profil_picture_leader=<?php echo $my_team['Teams']['logo']; ?>;
                        </script>
                        <h2><?php echo __("Ajout d'images de profil");?></h2><br/>
                        <div id="img_list" style="display:inline">

                              <div id="upload-img-<?php echo $my_team['Team']['id']; ?>" style='height:115px;margin-left:20px;display:inline-block'>
                                <img src="<?php echo $this->Html->url($my_team['Team']['logo']); ?>" width="110px" height="110px">
                              <br/>
                              <p id="leader-<?php echo $img['Media']['id']; ?>" style="display:inline-block;margin-bottom:0;text-align:center;"><?php if ($img['Media']['id'] == $owner['Player']['profil_id']) { ?><font color="red"><?php echo __("Photo par défaut");?></font><?php } else { ?><a href="javascript:;" onclick="set_picture_leader(<?php echo $img['Media']['id']; ?>)">Select</a> - <a href="javascript:;" onclick="delete_picture(<?php echo $img['Media']['id']; ?>)">Delete</a><?php } ?></p></br></br>
                         </div>

                      </div> </div> -->

      <?php
      $display_zonePlayer='none';
      if($my_team['TeamsHasPlayers'])
        $display_zonePlayer='block';
       ?>

      <div class="zone_player " style="display:<?php echo $display_zonePlayer ?>">
                    <div class="form-group">
                        <label><?php echo __("Logo de l'équipe");?></label>
                        <input class="form-control" type="file" name="files"  />
                    </div>
      <h4><?php echo __("Les joueurs actuellement dans l'équipe");?> :</h4>
                                    <?php
                                    foreach($membreTeam as $membre){
                                        if($membre['Players']['id'] != $my_team['Players']['id'])
                                        $allMembre .= $membre['Players']['pseudo']."<br />";
                                    }
                                    if($allMembre != ""){
                                        echo $allMembre;
                                    }
                                    else{
                                        echo 'Il n\'y a pas encore de membre dans votre équipe';
                                    }
                                    ?>

                                    <h4><?php echo __("Les joueurs à inviter");?> :</h4>
                                    <?php echo __("Vous pouvez invitez autant de personnes que vous le souhaitez, mais <b>seulement 3 personnes</b> (en plus de vous) peuvent rejoindre votre groupe.<br /><b>Cliquez sur un nom pour le supprimer</b>"); ?>

      <div class="form-group">
                                        <div id="nameInv"></div>
                                        <input id="invite" type="hidden" name="friend[id]" style="width:100%">
                                        </input>
                                        <br />
        <label><?php echo __("Rechercher des joueurs à invité");?></label>
        <input class="form-control find_player" type="text">
        <div class="search_player zone_forme">
        <?php
          foreach($friends as $friend) {
            if(isset($playersDB[$friend]['id']) ) {
              echo '<a href="javascript:inviteFriend(\''.$playersDB[$friend]['id'].'\',\''.$playersDB[$friend]['pseudo'].'\');"><div class="col-lg-12 pseudorelation">
               <p>'.$playersDB[$friend]['pseudo'].' '.$playersDB[$friend]['id'].'</p>
              </div></a>';
            }
          }

          foreach($propfriends as $prop) {
            if ( isset($playersDB[$prop]['id']) && $playersDB[$prop]['id']!=$player['Player']['id'] ) {
              echo '<a href="javascript:inviteFriend(\''.$playersDB[$prop]['id'].'\',\''.$playersDB[$prop]['pseudo'].'\');"><div class="col-lg-12 pseudorelationorange">
               <p>'.$playersDB[$prop]['pseudo'].'</p>
              </div></a>';
            }
          }

          foreach($playersDB as $key=>$pl) {
            if ($key!=$player['Player']['id'] && !isset($friends[$key]) && !isset($propfriends[$key])) {
              echo '<a href="javascript:inviteFriend(\''.$pl['id'].'\',\''.$pl['pseudo'].'\');"><div class="col-lg-12 pseudorelationrose">
              <p>'.$pl['pseudo'].'</p>
              </div></a>';
            }
          }

         ?>
        </div>

      </div>
    </div>

    </form>
                            <div id="message" style="color:red" align="center"></div>
    <button id="verrouillerBtn" class="create_team_save btn btn-primary"><?php echo __("Valider");?> !</button>
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Fermer");?></button>
                            <?php echo ($my_team['TeamsHasPlayers'])? "<a href='javascript:quitTeam()'>Quitter le groupe</a>" : "" ?></h2>


    </div>

  </div>
  </div>

</div> <!-- fin parametres -->

<style type="text/css">
  .search_player{
    height: 200px;
    overflow: auto;
  }
</style>

<script type="text/javascript">
    function quitTeam(){
        if (window.confirm('Etes vous sur de vouloir quitter le groupe ?')){
            document.location.href = "<?php echo $this->Html->url('/user/users/quitTeam',true )."?id=".$my_team['Teams']['id'] ?>";
            document.getElementById("message").innerHTML = "<p style='border:1px red solid; padding:10px;'><b>Traitement en cours, veuillez patienter ...<br/> Ne pas fermer cette page, elle se fermera automatiquement.</b></p>"
        }
    }
  function reponseInvit(){
      if(document.getElementById('refuser').checked == true){
          var idTeam = document.getElementById('refuser').value;
          var lien = "<?php echo $this->Html->url('/user/users/refuserInvit',true ); ?>?id=" + idTeam;
          document.location.href = lien;
      }
      else if(document.getElementById('accepter').checked == true){
          var idTeam = document.getElementById('accepter').value;
          var lien = "<?php echo $this->Html->url('/user/users/accepterInvit',true ); ?>?id=" + idTeam;
          document.location.href = lien;
      }
      document.getElementById('messageReponse').innerHTML = "<b>Veuillez patienter ...  l'envoi de votre réponse est en cours.<br/> Ne pas fermer cette page, elle se fermera automatiquement.</b>"
  }
  //Ajoute l'id de l'invité dans le select
  function  inviteFriend(id,pseudo){
        document.getElementById("invite").value += id +"-";
        document.getElementById("nameInv").innerHTML  += "<a id='"+ id +"' href='javascript:delInvite("+ id +")'>" + pseudo + " ; </a>";
  }
  //Supprime du select l'invité sélectionné et resélectionne les autres membres
  function delInvite(id){
        var obj = document.getElementById("nameInv");
        var old = document.getElementById(id);
        obj.removeChild(old);

        var input = document.getElementById("invite").value;
        input = input.replace(id+"-", "");
        document.getElementById("invite").value = input;
  }
// little Hack for make :contains search case-insensitive
$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

jQuery(document).ready(function($) {

  $('.form_create_team .find_player').keyup(function(event) {
    var key = $(this).val();
    $('.form_create_team .search_player div').hide();
    $('.form_create_team .search_player div').find('p').filter(':contains("'+key+'")').parent().show();
  });


  $('.create_team_save').click(function(event) {

    if ($('input[name="teams[name]"]').val().length === 0) {
      alert('Merci de choisir un nom pour votre équipe');
      return false;
    };

                if (document.getElementById("invite").value != ""){
                    document.getElementById("verrouillerBtn").style.display='none';
                    document.getElementById('message').innerHTML = "<b>Veuillez patientez ... Ne fermez surtout pas cette page.<br />Vos demandes sont en train d'être traitées, la page se ferma toute seule une fois toutes les actions finies.</b><br/>";
                }

    var data = $('.form_create_team input').filter(function(index) {
        return $(this).val()!='';
    }).serialize();

        var data = new FormData(
            $('.form_create_team')[0]
            );
         console.log(data);

    $.ajax({
      url: '<?php echo $this->Html->url("/user/users/handle_modteam", true); ?>',
      type: 'POST',
            contentType: false,
            processData: false,
      dataType: 'JSON',
      data: data
    })
    .done(function(data) {
        if(data.statut=="OK"){
          $('.zone_player').show();
          $('input[name="teams[id]"]').val(data.team_id);
        }
        if ($('.zone_player:visible').length>0 && data.statut=="OK") {
            document.getElementById("verrouillerBtn").style.display='inline-block';
            document.getElementById('message').innerHTML = "";
            $('#parametres').modal('hide');
            var teamName= $('input[name="teams[name]"]').val();
            location.assign('<?php echo $this->Html->url("/equipe/", true); ?>'+teamName);
        };
        if(data.alert)
          alert(data.alert);
    });

  });

  $('.btn.create_team').click(function(event) {
    $('#create_team').modal('show');
    return false;
  });
        $('.btn.invitation').click(function(event) {
    $('#invitation').modal('show');
    return false;
  });

});




$(window).load(function() {
    $('#profil-slider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 115,
      itemMargin: 3,
      pausePlay: true,
            nextText: "",
            prevText: "",
            pauseText: "",
            playText: "",
            start: function(slider){
              $('body').removeClass('loading');
            }
    });
});


$('#formulaire_updatepassword').submit(function (e) {
     var from = $(this);
     e.preventDefault();
     document.getElementById('form_updatepassword_success').style.display = 'none';
     document.getElementById('form_updatepassword_failure').style.display = 'none';
     $.ajax({ url:'<?php echo $this->Html->url("/user/users/modifypassword",true); ?>',
             data: { lastpassword: $('#form_updatepassword_lastpassword').val(), newpassword : $('#form_updatepassword_newpassword').val(), confnewpassword: $('#form_updatepassword_confnewpassword').val() },
             success: function( data ) {
               var ret=JSON.parse(data);
               if ( ret['code'] == 1 ) document.getElementById('form_updatepassword_success').style.display = 'block';
               if ( ret['code'] == 0 ) document.getElementById('form_updatepassword_failure').style.display = 'block';
            }});
     return(false);
});




$(function () {
    'use strict';
    var url = '<?php echo $this->Html->url("/user/users/uploadJQ"); ?>',
    uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Arrêter')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
   $('#profil-pict').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        $.each(data.files, function (index, file) {
        if (!index) {
           data.context = $('<div style="height:125px;margin-left:20px;display:inline"/>').appendTo('#img_upload');
           var node = $('<p style="display:inline-block"/>');
           node.append(uploadButton.clone(true).data(data));
           node.appendTo(data.context);
         }
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Accepter !')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                $(data.context.children()[index]).parent().remove();
                update_profil_pictures();
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});


function handle_relation(mod,idUser) {
      $.ajax({
       url:'<?php echo $this->Html->url("/user/users/handle_modrelation",true); ?>',
       data: { type: mod, idu: idUser  },
       success: function(data) {
          document.getElementById("relation_mod").style.display="none";
          if ( mod == 0 ) $('#rm_relation_mod').modal('hide');
       }
      });
      return (false);
  }


$('.confirm_remove_rel').click(function(e) {
   e.preventDefault();
   var id=$(this).data('id');
   var pseudo=$(this).data('pseudo');
   $('#rm_rel_mod_pseudo').text(pseudo);
   $('#rm_rel_mod_ok').attr("onClick","handle_relation(0,"+id+")");
   $('#rm_relation_mod').modal('show');
});


$('.valid_add_rel').click(function(e) {
   e.preventDefault();
   var id=$(this).data('id');
   var pseudo=$(this).data('pseudo');
   handle_relation(1,id);
   $('#add_rel_mod_pseudo').text(pseudo);
   $('#add_relation_mod').modal('show');
});


function update_profil_pictures() {
  $.ajax({
     url:'<?php echo $this->Html->url("/user/users/update_profil_pictures",true); ?>',
     success: function(data) {
        var pictures=JSON.parse(data);
        if ( pictures['code']==1 ) {
           profil_picture_leader=pictures['leader'];
           var i = 0;
           $('#img_list').empty();
           while ( pictures['pictures'][i] != undefined ) {
              var div = '<div id="upload-img-'+pictures['pictures'][i]['id']+'" style="height:115px;margin-left:20px;display:inline-block">';
              div    += '<img src="'+pictures['pictures'][i]['file']+'" width="110px" height="110px"></br>';
              if (  profil_picture_leader == pictures['pictures'][i]['id'] ) {
                 div += '<p id="leader-'+pictures['pictures'][i]['id']+'" style="display:inline-block;margin-bottom:0;text-align:center;"><font color="red">Photo par défaut</font></p></br></br></div>';
              } else {
                 div += '<p id="leader-'+pictures['pictures'][i]['id']+'" style="display:inline-block;margin-bottom:0;text-align:center;"><a href="javascript:;" onclick="set_picture_leader('+pictures['pictures'][i]['id']+')">Select</a> - <a href="javascript:;" onclick="delete_picture('+pictures['pictures'][i]['id']+')">Delete</a></p></br></br></div>';
              }
              $('#img_list').append(div);
              i=i+1;
           }
        }
     }
  });
  return (false);
}



function set_picture_leader(idMedia) {
       if ( profil_picture_leader == idMedia ) return(false);
       $.ajax({
       url:'<?php echo $this->Html->url("/user/Medias/media_profil_leader",true); ?>',
       data: { id : idMedia },
       success: function(data) {
         var log=JSON.parse(data);
         if ( log['code'] == 1 ) {
            $('#leader-'+idMedia).empty();
            $('#leader-'+idMedia).append('<font color="red">Photo par défaut</font>');
            $('#leader-'+profil_picture_leader).empty();
            $('#leader-'+profil_picture_leader).append('<a href="javascript:;" onclick="set_picture_leader('+profil_picture_leader+')">Select</a> - <a href="javascript:;" onclick="delete_picture('+profil_picture_leader+')">Delete</a>');
            profil_picture_leader = idMedia;
         }
       }
      });
      return (false);
  }


function delete_picture(idMedia) {
       if ( profil_picture_leader == idMedia ) return(false);
       $.ajax({
       url:'<?php echo $this->Html->url("/user/Medias/media_profil_delete",true); ?>',
       data: { id : idMedia },
       success: function(data) {
         var log=JSON.parse(data);
         if ( log['code'] == 1 ) {
            $('#upload-img-'+idMedia).remove();
         }
       }
      });
      return (false);
}


$('#formulaire_newdevise').submit(function (e) {
      var from = $(this);
      e.preventDefault();
      var devise=$('#devise').val();
      $.ajax({ url:'<?php echo $this->Html->url("/user/users/set_deviseTeam",true); ?>',
               data: { valeur: devise},
               success: function(data) {
                   var ret=JSON.parse(data);
                   if ( ret["code"] == 1 ) {
                      $('#new_devise').modal('hide');
                      window.location.reload();
                   } else {
                      $('#formulaire_newdevise_failure').text(ret["message"]);
                      document.getElementById('formulaire_newdevise_failure').style.display = 'block';
                   }
               }
      });
      return(false);
});



$('#formulaire_profilcomment').submit(function (e) {
      var from = $(this);
      e.preventDefault();
      var _comment = $('#profil_commentaire').val();
      $.ajax({ url:'<?php echo $this->Html->url("/user/users/addTeam_profil_comment",true); ?>',
               data: { commentaire: _comment, id_target: <?php echo $equipe['Team']['id']; ?> },
               success: function(data) {
                   var ret=JSON.parse(data);
                   if ( ret["code"] == 1 ) {
                      $('#profil_comment').modal('hide');
                      window.location.reload();
                   } else {
                      $('#formulaire_profilcomment_failure').text(ret["message"]);
                      document.getElementById('formulaire_profilcomment_failure').style.display = 'block';
                   }
               }
      });
      return(false);
  });


function handle_devise_like(val_like,devise_id) {
   $.ajax({ url:'<?php echo $this->Html->url("/user/users/add_devise_like",true); ?>',
         data: { like: val_like, id_target: devise_id },
         success: function(data) {
         var ret=JSON.parse(data);
         if ( ret["code"] == 1 ) {
            if ( val_like == 1 ) document.getElementById("display_devise_like").innerHTML = ret["val"];
            if ( val_like == 0 ) document.getElementById("display_devise_dislike").innerHTML = ret["val"];
         } else {
            if ( val_like == 1 ) {
               $('#display_devise_like').tooltip();
               $('#display_devise_like').tooltip('show');
               setTimeout(function() {
                  $('#display_devise_like').tooltip('hide');
               }, 2000);
            }
            if ( val_like == 0 ) {
               $('#display_devise_dislike').tooltip();
               $('#display_devise_dislike').tooltip('show');
               setTimeout(function() {
                  $('#display_devise_dislike').tooltip('hide');
               }, 2000);
            }
         }
      }
   });
}


</script>
