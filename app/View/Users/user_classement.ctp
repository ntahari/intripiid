<div class="visible-lg"></br>
</br>
</br>
</div>
<div class="visible-xs"></br>
</div>
<div class="container master mabordure">
 <div class="mabordure-avatars visible-lg visible-md">
		<div align="right" class="mabordure-avatars-droite">
		<img src="<?php echo $this->Html->url($avatar_droite); ?>"></img>
		</div>
		<div align="left" class="mabordure-avatars-gauche">
		<img src="<?php echo $this->Html->url($avatar_gauche); ?>"></img>
		</div>
 </div>
	<nav class="navbar nav-selection" role="navigation">
	 <div class="container-fluid">

		 <!-- appear for medium and large sioscreens -->
		 <div class="visible-md visible-lg visible-sm visible-xs">
		 
				<h1 class="navbar-text navbar-left"> <?php echo __("La meilleur équipe au 29 aout gagnera un voyage pour 4 a Barcelone, faites vous plaisir entre amis ! ");?> ! </h1>
				
				<br>
				<form id="formsearch" class="navbar-form navbar-right hidden-xs" role="search">
				<div class="form-group" style="max-width:200px;">
					<input id="formsearch_input" type="text" class="form-control" style="font-family:'Patua One';" name="search" placeholder="Rechercher">
				</div>
				<button type="submit" class="btn btn-default"><?php echo __("Valider");?> !</button>
				</form>
				
				<!--<button type="button" class="btn btn-default navbar-btn navbar-right hidden-xs" data-toggle="modal" data-target="#top5"><?php echo __("Voir le Top 5");?> !</button>-->
				<button type="button" class="btn btn-default navbar-btn navbar-left hidden-xs" value="lien" onclick="location.href='http://www.intripid.fr/user/users/classement?page=1&saison=2';" data-toggle="modal" ><?php echo __("classement solo saison 2");?> </button>
				<button type="button" class="btn btn-default navbar-btn navbar-left hidden-xs" value="lien" onclick="location.href='http://www.intripid.fr/user/users/classement?page=1&team=team&teamsaison=teamsaison';" data-toggle="modal" ><?php echo __("classement équipe saison 2");?> </button>
				
				<button type="button" class="btn btn-default navbar-btn navbar-left hidden-xs" value="lien" onclick="location.href='http://www.intripid.fr/user/users/relations';" data-toggle="modal" ><?php echo __("RDV page ami pour créer son équipe");?> </button>
<button class="btn btn-default navbar-btn navbar-left hidden-xs" data-toggle="collapse" title="Voir tous les filtres !" data-target="#demofiltres" type="button" class=" filtrestop btn btn-default"><?php echo __("Chercher par");?> <span class="caret"></span></button>		      
				
		 	
					
			
		 	
		
		 
		 
		 
		 </div>
		</div>
		
		
		
	</nav>
	<!-- for telephone screen -->
		 <div class="visible-sm visible-xs">
		 	<ul class="nav navbar-nav nav-mission">
		 		<li class="dropdown">
					<a href="<?php echo $this->Html->url("/user/users/classement?page=1&saison=".$lastSaison['Saison']['id'])?>" class=" lien-valider-isotop col-xs-5 col-sm-5" >
						<?php echo __("Classement solo");?>
					</a>	
				</li>
		 		<li class="dropdown">
					<a href="<?php echo $this->Html->url("/user/users/classement?page=1&team=team&teamsaison=teamsaison")?>" class=" lien-valider-isotop col-xs-5 col-sm-5"><?php echo __("classement équipe");?>			
					</a>	
				</li>
		 	</ul>
		 	
		 </div>
	<!-- ********* -->

	<hr>

	<?php
		 $post="";
		 if ( $search!="" ) $post="search=".$search."&";
	?>
		<div id="demofiltres" class="collapse col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ul class="nav navbar-nav nav-mission">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle lien-valider-isotop" data-toggle="dropdown">
								<?php echo __("nature");?>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<form>
									<fieldset>
										<ul class="option-set clearfix">
										<?php foreach($missionsnature as $m) : ?>
											 <li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&nature=".$m['Missionnature']['id'])?>">
											 <?php echo $m['Missionnature']['name']?></a>
											</li>

										<?php endforeach; ?>
										</ul>
									</fieldset>
								</form>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle lien-valider-isotop" data-toggle="dropdown">
								<?php echo __("classe");?>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<form>
									<fieldset>
										<ul class="option-set clearfix">
											<li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&classe=moyenage")?>"><?php echo __("moyen age");?></a>
											</li>
											<li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&classe=aventurier")?>"><?php echo __("aventurier");?></a>
											</li>
											<li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&classe=antiquite")?>"><?php echo __("antiquité");?></a>
											</li>
											<li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&classe=alien")?>"><?php echo __("alien");?></a>
											</li>
											<li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&classe=superheros")?>"><?php echo __("super heros");?></a>
											</li>
										</ul>
									</fieldset>
								</form>
							</ul>
								</li>

 								<li class="dropdown">
							<a href="#" class="dropdown-toggle lien-valider-isotop" data-toggle="dropdown">
								periode
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<form>
									<fieldset>
										<ul class="option-set clearfix ">

											<li><a href="<?php echo $this->html->url("/user/users/classement?page=1&periode=31")?>"> <?php echo __("mois");?> </a></li>
											<?php foreach($saison as $m) : ?>
											 <li><a href="<?php echo $this->Html->url("/user/users/classement?page=1&saison=".$m['Saison']['id'])?>">
											 <?php echo $m['Saison']['intitule']?></a>
											</li>
											<?php endforeach; ?>
											<li><a href="<?php echo $this->html->url("/user/users/classement?page=1&historique=historique")?>"> <?php echo __("historique");?> </a></li>


										</ul>
									</fieldset>
								</form>
							</ul>
						</li>

						<li class="dropdown">
							<a href="<?php echo $this->html->url("/user/users/classement?page=1&amis=amis")?>" class=" lien-valider-isotop" >
								<?php echo __("Parmi les amis");?>
							</a>	
						</li>
						<!-- <li class="dropdown">
							<a href="<?php echo $this->Html->url("/user/users/classement?page=1&class_avatar=class_avatar")?>" class=" lien-valider-isotop"><?php echo __("par classe d'avatar");?>								
							</a>	
						</li> -->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle lien-valider-isotop" data-toggle="dropdown">
								Par équipe
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<form>
									<fieldset>
										<ul class="option-set clearfix ">
											<li>
											<a href="<?php echo $this->Html->url("/user/users/classement?page=1&team=team")?>"><?php echo __("En générale");?>			
											</a>	
											</li>

											<li>
											<a href="<?php echo $this->Html->url("/user/users/classement?page=1&team=team&teamsaison=teamsaison")?>"><?php echo __("saison en cours");?>			
											</a>	
											</li>
										</ul>
										</ul>
									</fieldset>
								</form>
							</ul>
						</li>

					</ul>
		</div>



	<div class="container-fluid">
	<div class="row">

		<div class="col-md-12 col-lg-12">

				<div class="container-fluid">
					<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12 headerclassement">
					<span class="pull-left headerclassement-classement"><?php echo __("Classement");?></span>
					<span class="pull-left headerclassement-membres"><?php echo __("Membre de la confrérie");?></span>

					<span class="pull-right headerclassement-points hidden-xs"> <?php echo __("Niveau");?></span>
					<span class="pull-right headerclassement-points"> <?php echo __("Points");?></span>
					<span class="pull-right headerclassement-victoires visible-lg visible-md visible-sm"> <?php echo __("Défis réussis");?></span>
					</div>
				 </div>
				</div>

	 <?php
	 $cc=0;
	 for($i =0 ; $i<50; $i++){ ?>

		<?php if(!empty($classement[$i])){
			$cc++;
			$p = 0;
			if ($page>1)
				$p=50*$page-50;
			?>
				<div class="container-fluid">
					<div class="row">
						
				<?php /*if(isset($_GET['class_avatar'])) { 
			?>
				 		<div class="divclassement col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-left" >
							<span class="scoreclassement pull-left"><?php echo ($p+$cc) ?></span>
							<i class="icon-minus arrow-classement pull-left"></i>
							<span class="mouvementclassement pull-left visible-lg visible-md visible-sm"></span>
							<span class-"visible-lg visible-md visible-sm">
								
								<img class="pull-left imgclassement visible-lg visible-md visible-sm" src="<?php  echo $this->Html->url($classement[$i]['avatar_type']); ?>" style="width:50px;height:50px">
							
							</span>
								
							<span class="pseudoclassement pull-left hidden-xs">
							  <?php echo $classement[$i]['slug_type']; ?><br/><?php echo $classement[$i]['Type']['slug']; ?>
							</span>
							<!-- uniquement pour le téléphone -->
							<span class="pseudoclassement pull-left hidden-lg hidden-md hidden-sm">
								<a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$classement[$i]['pseudo']))); ?>"><?php echo $classement[$i]['pseudo']; ?></a> <br> <?php echo $classement[$i]['classe']; ?>
							</span>
							<span class="pointsclassement point-classement hidden-xs pull-right"><?php echo $classement[$i]['level']; ?></span>
							<span class="pointsclassement pull-right"><?php echo $classement[$i]['experience']; ?></span>
							<span class="victoiresclassement pull-right visible-lg visible-md visible-sm"><?php echo $classement[$i]['victoires']; ?></span>
						</div>
				<?php } */
				if (isset($_GET['team'])) { ?>
						<div class="divclassement col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-left" >
							<span class="scoreclassement pull-left"><?php echo ($p+$cc) ?></span>
							<i class="icon-minus arrow-classement pull-left"></i>
							<span class="mouvementclassement pull-left visible-lg visible-md visible-sm"></span>
							<span class-"visible-lg visible-md visible-sm">
								<?php if (!empty($classement_team[$i]['logo'])) { ?>
									<img class="pull-left imgclassement visible-lg visible-md visible-sm" src="<?php  echo $this->Html->url('/img/uploads/team/thumb/'.$classement_team[$i][id].'/'.$classement_team[$i]['logo']);?>" style="width:50px;height:50px">
								<?php } else { ?>
									<img class="pull-left imgclassement visible-lg visible-md visible-sm" src="<?php  echo $this->Html->url('/img/uploads/team/thumb/no-image.jpg');?>" style="width:50px;height:50px">
								<?php } ?>
							
							</span>
								
							<span class="pseudoclassement pull-left hidden-xs">
							  <a href="<?php echo $this->Html->url("/equipe/".urlencode(str_replace(" ","-",$classement_team[$i]['name']))); ?>">  <?php echo $classement_team[$i]['name']; ?></a> <br/> <?php echo $classement_team[$i]['devise']; ?>
							</span>
							<!-- uniquement pour le téléphone -->
							<span class="pseudoclassement pull-left hidden-lg hidden-md hidden-sm">
								<a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$classement_team[$i]['name']))); ?>"><?php echo $classement_team[$i]['name']; ?></a> <br> <?php echo $classement_team[$i]['classe']; ?>
							</span>
							<span class="pointsclassement point-classement hidden-xs pull-right"> -<?php //echo $classement_team[$i]['devise']; ?></span>
							<span class="pointsclassement pull-right"><?php if ($classement_team[$i]['experience']==NULL) echo "-"; else echo $classement_team[$i]['experience']; ?></span>
							<span class="victoiresclassement pull-right visible-lg visible-md visible-sm"><?php echo $classement_team[$i]['victoires']; ?></span>
						</div>
				<?php }
				else { ?>

						<div class="divclassement col-md-12 col-lg-12 col-sm-12 col-xs-12 pull-left" <?php if ($classement[$i]['id']==$player['Player']['id']) { ?> style="background-color:#fea9ab;" <?php } ?>>
							<span class="scoreclassement pull-left"><?php echo ($p+$cc) ?></span>
							<!--<i class="icon-minus arrow-classement pull-left"></i>-->
							<span class="mouvementclassement pull-left visible-lg visible-md visible-sm"></span>
							<span class="visible-lg visible-md visible-sm">

								<a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$classement[$i]['pseudo']))); ?>" title="Voir le profil de <?php echo $classement[$i]['pseudo']; ?>">
									<img class="pull-left imgclassement visible-lg visible-md visible-sm" src="<?php if ( $classement[$i]['profil_pict'] != "" ) { echo $this->Html->url($classement[$i]['profil_pict']); } else { echo $this->Html->url($classement[$i]['avatar_mini']); } ?>" style="width:50px;height:50px">
								</a>
							</span>

							<span class="pseudoclassement pull-left hidden-xs">
							 <a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$classement[$i]['pseudo']))); ?>">  <?php echo $classement[$i]['pseudo']; ?></a> - <?php echo $classement[$i]['classe']; ?><br/><?php echo $classement[$i]['grade']; ?>
							</span>
							<!-- uniquement pour le téléphone -->
							<span class="pseudoclassement pull-left hidden-lg hidden-md hidden-sm">
								<a href="<?php echo $this->Html->url("/hero/".urlencode(str_replace(" ","-",$classement[$i]['pseudo']))); ?>"><?php echo $classement[$i]['pseudo']; ?></a> <br> <?php echo $classement[$i]['classe']; ?>
							</span>
							<span class="pointsclassement point-classement hidden-xs pull-right"><?php echo $classement[$i]['level']; ?></span>
							<span class="pointsclassement pull-right"><?php if ($classement[$i]['experience']==NULL) echo "-"; else echo $classement[$i]['experience']; ?></span>
							<span class="victoiresclassement pull-right visible-lg visible-md visible-sm"><?php echo $classement[$i]['victoires']; ?></span>
						</div>


						 
					</div><!-- fin row -->
				</div><!-- fin container fluid -->

		<?php }} }?>

		<div class="clearfix"></div>

<?php if ( $pages>=0 ) {
		if (!empty($_GET)) {
			$link_param='';

				foreach ($_GET as $parameter => $value) {
					if($parameter!='page')
							$link .= "&" . $parameter . "=" . urlencode($value);
				}
		}
	?>
	 <div class="text-center">
		<ul class="pagination">
				<?php if($page > 1  && $page <= $pages) { ?>
				<li style="font-family:'Patua One';"><a href="<?php $prev=$page-1; echo $this->Html->url("/user/users/classement?".$post."page=".$prev.$link); ?>">Précédent</a></li>
				<?php } else { ?>
				<li class="disabled" style="font-family:'Patua One';"><a nohref="nohref" >Retour</a></li>
				<?php }
				if ($page > 1 && $page <= $pages ) {
					 if  ( ($page - 4) < 1 ) $start = 1;
					 else $start = ($page - 4);
					 for ( $i=$start; $i<$page; $i++ ) { ?>
							 <li style="font-family:'Patua One';"><a href="<?php echo $this->Html->url("/user/users/classement?".$post."page=".$i.$link); ?>"><?php echo $i; ?></a></li>
				<?php }
				}
				if ($page >= 1 ) { ?>
				<li class="disabled" style="font-family:'Patua One';"><span><?php echo $page; ?></span></li>
				<?php }
				if ( ($page + 4) > $pages) $end = $pages;
				else $end = $page + 4;
				if ($page >= 1 && $page <= $pages ) {
					 for ( $i=($page+1); $i<($end+1); $i++ ) { ?>
							 <li style="font-family:'Patua One';"><a href="<?php echo $this->Html->url("/user/users/classement?".$post."page=".$i.$link); ?>"><?php echo $i; ?></a></li>
				<?php }
				}
				if($page >= 1  && $page < $pages) { ?>
				<li style="font-family:'Patua One';"><a href="<?php $next=$page+1; echo $this->Html->url("/user/users/classement?".$post."page=".$next.$link); ?>">Suivant</a></li>
				<?php } else { ?>
				<li class="disabled" style="font-family:'Patua One';"><a nohref="nohref">Suivant</a></li>
				<?php } ?>
		</ul>
	 </div>
<?php } ?>

		</div>


	</div> <!-- row -->
 </div> <!-- container-fluid-->


<div id="top5" class="modal fade bs-example-modal-lg" aria-labelledby="thetop5" aria-hidden="true">
			<div class="modal-dialog modal-lg">
			 <div class="modal-content">
			<div class="modal-body">
			 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		 <h2 class="top5"> <?php echo __("Le top 5");?> </h2>

		 <h2 class="top5phrase"> <?php echo __("Les joueurs les plus forts, les plus courageux, les plus fous de Paris");?> ! </h2>

			 <div class="container-fluid">
					<div class="row">
						<img class="img-responsive" src="<?php echo $this->Html->url($top5); ?>"></a>
					</div>
			 </div>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __("Bravo");?> !</button>
			</div>
		 </div>
		</div>
	</div>

<div class="navbar navbar-inverse navbar-fixed-bottom hidden-lg hidden-md hidden-sm" style="background-color: #3C3838 !important;">
		<form id="formsearch" class="navbar-right" role="search">
				<div class="form-group pull-left" style="max-width:200px;margin-left:5px;">
					<input id="formsearch_input" type="text" class="form-control pull-right" style="font-family:'Patua One';" name="search" placeholder="Rechercher">
				</div>
				<button type="submit" style="margin-right:5px;" class="btn btn-default pull-right"><?php echo __("Valider");?> !</button>
				</form>
</div>