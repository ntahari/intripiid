<?php
App::uses('AppController', 'Controller');
ini_set('memory_limit', '256M');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController
{
	var $components = array(
		'Captcha'
	);

	function save_newcompte($d = null)
	{
		$this->loadModel('User');
		$data = $this->User->save($d, array(
			true,
			array(
				'username',
				'password',
				'datenaissance',
				'email'
			) ,
			false,
			false
		));
		return $data;
	}

	function coupon_pdf($player, $cadeau, $partenaire, $mission_nature, $validite)
	{
		$this->loadModel('Cadeaux');
		$this->loadModel('Media');
		$this->set('player', $player);
		$this->set('cadeau', $cadeau);
		$this->set('partner', $partenaire);
		$this->set('mission_nature', $mission_nature);
		$image = null;
		if ($cadeau['Cadeaux']['id_media'] != NULL) {
			$image = $this->Media->find('first', array(
				'conditions' => array(
					'Media.id' => $cadeau['Cadeaux']['id_media']
				) ,
				'recursive' => - 1
			));
		}
		$this->set('image', $image);
		$this->set('validite', $validite);
		$this->layout = 'pdf';
		$coupon_num = time() . $player['Player']['id'] . $cadeau['Cadeaux']['id'];
		$file = $coupon_num . '.pdf';
		$this->set('coupon_num', $coupon_num);
		$this->set('file', $file);
		$output = $this->render('coupon_pdf');
		return $coupon_num;
	}

	function millisec()
	{
		return round(microtime(1) * 1000);
	}

	function generate_podium($top5)
	{
		$podiumx = 1200;
		$podiumy = 600;

		//640;
		$first_y = (70 * $podiumy) / 100;
		$second_y = (50 * $podiumy) / 100;
		$last_y = (40 * $podiumy) / 100;
		$last_x = (16 * $podiumx) / 100;
		$second_x = (21 * $podiumx) / 100;
		$first_x = (26 * $podiumx) / 100;
		$background = imagecreatetruecolor($podiumx, $podiumy);
		$white = imagecolorallocate($background, 255, 255, 255);
		$red = imagecolorallocate($background, 255, 0, 0);
		$black = imagecolorallocate($background, 0, 0, 0);
		imagefill($background, 0, 0, $white);
		$outputimage = $background;
		$font = WWW_ROOT . "css/fonts/angrybirds-regular.ttf";

		$avatar1_file = WWW_ROOT . $top5[0]['avatar'];
		$avatar1 = imagecreatefrompng($avatar1_file);
		$avatar1_size = getimagesize($avatar1_file);
		$avatar2_file = WWW_ROOT . $top5[1]['avatar'];
		$avatar2 = imagecreatefrompng($avatar2_file);
		$avatar2_size = getimagesize($avatar2_file);
		$avatar3_file = WWW_ROOT . $top5[2]['avatar'];
		$avatar3 = imagecreatefrompng($avatar3_file);
		$avatar3_size = getimagesize($avatar3_file);
		$avatar4_file = WWW_ROOT . $top5[3]['avatar'];
		$avatar4 = imagecreatefrompng($avatar4_file);
		$avatar4_size = getimagesize($avatar4_file);
		$avatar5_file = WWW_ROOT . $top5[4]['avatar'];
		$avatar5 = imagecreatefrompng($avatar5_file);
		$avatar5_size = getimagesize($avatar5_file);
		imagecopyresized($outputimage, $avatar4, 0, $first_y - $last_y, 0, 0, $last_x, $last_y, $avatar4_size[0], $avatar4_size[1]);
		$box = imagettfbbox(13, 0, $font, "4ème");
		imagettftext($outputimage, 13, 0, ($last_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 10, $black, $font, "4ème");
		$box = imagettfbbox(13, 0, $font, $top5[3]['pseudo']);
		imagettftext($outputimage, 13, 0, ($last_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 33, $red, $font, $top5[3]['pseudo']);
		$box = imagettfbbox(13, 0, $font, $top5[3]['experience']);
		imagettftext($outputimage, 13, 0, ($last_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 65, $red, $font, $top5[3]['experience']);

		imagecopyresized($outputimage, $avatar2, $last_x, $first_y - $second_y, 0, 0, $second_x, $second_y, $avatar2_size[0], $avatar2_size[1]);
		$box = imagettfbbox(15, 0, $font, "2ème");
		imagettftext($outputimage, 15, 0, $last_x + ($second_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 10, $black, $font, "2ème");
		$box = imagettfbbox(15, 0, $font, $top5[1]['pseudo']);
		imagettftext($outputimage, 15, 0, $last_x + ($second_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 35, $red, $font, $top5[1]['pseudo']);
		$box = imagettfbbox(15, 0, $font, $top5[1]['experience']);
		imagettftext($outputimage, 15, 0, $last_x + ($second_x / 2) - ($box[2] - $box[0]) / 2, $first_y + 65, $red, $font, $top5[1]['experience']);

		imagecopyresized($outputimage, $avatar1, $last_x + $second_x, 0, 0, 0, $first_x, $first_y, $avatar1_size[0], $avatar1_size[1]);
		$box = imagettfbbox(17, 0, $font, "1er");
		imagettftext($outputimage, 17, 0, $last_x + $second_x + $first_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 10, $black, $font, "1er");
		$box = imagettfbbox(17, 0, $font, $top5[0]['pseudo']);
		imagettftext($outputimage, 17, 0, $last_x + $second_x + $first_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 37, $red, $font, $top5[0]['pseudo']);
		$box = imagettfbbox(17, 0, $font, $top5[0]['experience']);
		imagettftext($outputimage, 17, 0, $last_x + $second_x + $first_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 65, $red, $font, $top5[0]['experience']);

		imagecopyresized($outputimage, $avatar3, $last_x + $second_x + $first_x, $first_y - $second_y, 0, 0, $second_x, $second_y, $avatar3_size[0], $avatar3_size[1]);
		$box = imagettfbbox(15, 0, $font, "3ème");
		imagettftext($outputimage, 15, 0, $last_x + $second_x + $first_x + $second_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 10, $black, $font, "3ème");
		$box = imagettfbbox(15, 0, $font, $top5[2]['pseudo']);
		imagettftext($outputimage, 15, 0, $last_x + $second_x + $first_x + $second_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 35, $red, $font, $top5[2]['pseudo']);
		$box = imagettfbbox(15, 0, $font, $top5[2]['experience']);
		imagettftext($outputimage, 15, 0, $last_x + $second_x + $first_x + $second_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 65, $red, $font, $top5[2]['experience']);

		imagecopyresized($outputimage, $avatar5, $last_x + $second_x + $first_x + $second_x, $first_y - $last_y, 0, 0, $last_x, $last_y, $avatar5_size[0], $avatar5_size[1]);
		$box = imagettfbbox(13, 0, $font, "5ème");
		imagettftext($outputimage, 13, 0, $last_x + $second_x + $first_x + $second_x + $last_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 10, $black, $font, "5ème");
		$box = imagettfbbox(13, 0, $font, $top5[4]['pseudo']);
		imagettftext($outputimage, 13, 0, $last_x + $second_x + $first_x + $second_x + $last_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 33, $red, $font, $top5[4]['pseudo']);
		$box = imagettfbbox(13, 0, $font, $top5[4]['experience']);
		imagettftext($outputimage, 13, 0, $last_x + $second_x + $first_x + $second_x + $last_x / 2 - ($box[2] - $box[0]) / 2, $first_y + 65, $red, $font, $top5[4]['experience']);

		imagepng($outputimage, WWW_ROOT . "img/podium/podium.png", 3);
		imagedestroy($outputimage);
		return "/img/podium/podium.png";
	}

	function compute_classement()
	{
		$this->loadModel('Player');
		$players = $this->Player->find('all', array(
			'fields' => 'Player.id,Player.pseudo,Player.experience,Player.classement',
			'order' => 'Player.experience ASC',
			'recursive' => - 1
		));
		$i = 1;
		foreach ($players as $pl) {
			if ($pl['Player']['experience'] != 0) {
				$this->Player->id = $pl['Player']['id'];
				$this->Player->saveField('classement', $i);
			}
			$i = $i + 1;
		}
	}

	public $paginate = array(
		'Player' => array(
			'fields' => array(
				'Player.profil_id,Player.id,Player.type_id,Level.level,Player.pseudo,Player.experience,Type.name,Type.image1,Type.image2,Type.image3,Type.image4,Type.image5,Type.image6,Media.thumb'
			) ,
			'order' => array(
				'Player.experience' => 'desc'
			) ,
			'limit' => '12'
		) ,
		'classement' => array(
			'order' => array(
				'classement' => 'asc'
			) ,
			'limit' => '50'
		) ,
		'notifications' => array(
			'order' => array(
				'timestamp' => 'desc'
			) ,
		)
	);

	function find_me($data, $id)
	{
		$i = 1;
		foreach ($data as $cl) {
			if ($cl['id'] == $id) $pos = $i;
			$i = $i + 1;
		}
		if ($pos > 1) $pos = $pos - 1;
		$page = (int)($pos / $this->paginate['classement']['limit']) + 1;
		return $page;
	}

	public function user_load_faq()
	{
		$this->loadModel('Faq');
		$faq = $this->Faq->find('first', array(
			'recursive' => 0
		));
		if (!isset($faq['Faq']['description'])) {
			$content = 'FAQ non disponible';
		}
		else $content = $faq['Faq']['description'];
		echo $content;
		exit();
	}

	public function user_load_cgu()
	{
		$this->loadModel('Contrat');
		$cgu = $this->Contrat->find('first', array(
			'recursive' => 0
		));
		if (!isset($cgu['Contrat']['description'])) {
			$content = 'CGU non disponible';
		}
		else $content = $cgu['Contrat']['description'];
		echo $content;
		exit();
	}

	function validate_string_input($string)
	{
		$string = trim($string);
		$string = stripslashes($string);
		$string = htmlspecialchars($string);
		$string = addslashes($string);
		return $string;
	}

	function make_classement($array)
	{
		usort($array, array(
			$this,
			'sort'
		));

		/*$nbrec=count($array);
		for ( $i=0; $i<$nbrec ; $i++) {
			  $array[$i]['pos']=$i+1;
		  }*/
		return $array;
	}

	/*  function make_notifications_sorting($array) {
	 usort($array,array($this,'sort_notif'));
	 return $array;
	}*/

	function sort_notif($a, $b)
	{
		$result = 0;
		foreach ($this->paginate['notifications']['order'] as $key => $order) {
			if ($a[$key] == $b[$key]) continue;
			$result = ($a[$key] < $b[$key] ? -1 : 1) * ($order == 'asc' ? 1 : -1);
		}
		return ($result);
	}

	function classement_paginate($array, $page)
	{
		$nbrec = count($array);
		if ($nbrec <= $this->paginate['classement']['limit']) $page_count = 0;
		else $page_count = (int)($nbrec / $this->paginate['classement']['limit']) + 1;
		$this->set('elements', $nbrec);
		$this->set('pages', $page_count);
		return array_slice($array, ($page - 1) * $this->paginate['classement']['limit'], $this->paginate['classement']['limit']);
	}

	function sort($a, $b)
	{
		$result = 0;
		$result = $b['experience'] - $a['experience'];
/*		foreach ($this->paginate['classement']['order'] as $key => $order) {
			if ($a[$key] == $b[$key]) continue;
			$result = ($a[$key] < $b[$key] ? -1 : 1) * ($order == 'asc' ? 1 : -1);
		}
*/		return ($result);
	}

	public function voirmail()
	{
	}

	public function user_classementbeta()
	{
		$this->loadModel('Player');
		$user = $this->Session->read();
		$owner = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
		$this->loadModel('Type');
		$this->SysNotification->computeCount($owner);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $owner['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $owner['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $owner['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $owner['Type'][$avatar_mini]);
		}

		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $owner['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $owner['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}

		if (isset($this->request->query['page']) && ctype_digit($this->request->query['page']) && $this->request->query['page'] >= 1) $page = $this->request->query['page'];
		else $page = 0;
		if (isset($this->request->query['search'])) {
			$this->set('search', $this->request->query['search']);
			$search = $this->validate_string_input($this->request->query['search']);
		}
		else {
			$search = "";
			$this->set('search', $search);
		}

		$this->loadModel('Missionnature');
		$mn = $this->Missionnature->find('list');
		$this->set('missionsnature', $mn);
		$filter = 'any';
		if (isset($this->request->query['nature'])) {
			foreach ($mn as $n):
				if ($n == $this->request->query['nature']) $filter = $n;
			endforeach;
		}
		$this->set('filter', $filter);

		$startmillisec = $this->millisec();

		$this->loadModel('Missiondone');
		$this->loadModel('Media');
		$this->loadModel('Type');
		$this->loadModel('Level');

		// On récupère toute les infos pour pouvoir faire le classement
		$records = array();
		$records_nocl = array();

		// ceux qui ne sont pas classé
		$players = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id,Level.name,Level.palier,Level.level,Player.pseudo,Player.experience,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini, Player.classement, Type.image4,Type.image5,Type.image6,Media.thumb',
			'order' => 'Player.experience DESC',
			'conditions' => array(
				'Player.user_id !=' => 1
			) ,
			'recursive' => 1
		));
		$i = 1;
		foreach ($players as $pl) {
			$victoires = count($pl['Missiondone']);
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = "";
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
			if ($pl['Player']['experience'] == 0 && $pl['Player']['classement'] != - 1) {
				$this->Player->id = $pl['Player']['id'];
				$this->Player->saveField('classement', -1);
				$pl['Player']['classement'] = - 1;
			}
			if ($pl['Player']['experience'] != 0 && $pl['Player']['classement'] != $i) {
				$this->Player->id = $pl['Player']['id'];
				$this->Player->saveField('classement', $i);
				$pl['Player']['classement'] = $i;
			}
			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name'],
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'experience' => $pl['Player']['experience'],
				'classement' => $pl['Player']['classement'],
				'level' => $pl['Level']['level']
			);
			if ($pl['Player']['experience'] == 0) {
				array_push($records_nocl, $record);
			}
			else {
				array_push($records, $record);
			}

			$i = $i + 1;
		}

		// on fait le classement globale ici
		$data = $this->make_classement($records);
		foreach ($records_nocl as $rec) {
			array_push($data, $rec);
		}

		$top5 = array_slice($data, 0, 5);

		// on sélectionne les données a afficher
		if ($search == "") {
			if ($page == 0) $page = $this->find_me($data, $owner['Player']['id']);
			$data = $this->classement_paginate($data, $page);
		}
		else {
			$records_search = array();
			foreach ($data as $cl) {
				if (stristr($cl['pseudo'], $search)) array_push($records_search, $cl);
			}
			$data = $this->classement_paginate($records_search, $page);
		}
		//$podium = $this->generate_podium($top5);

		//$this->set('top5',$top5);
		$this->set('top5', $podium);
		$this->set('page', $page);
		$this->set('classement', $data);
	}

	public function user_classement()
	{
		$this->set("title_for_layout", "Classement");
		$this->set("show_concept", 0);
		$this->loadModel('Player');
		$user = $this->Session->read();
		$owner = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
		$this->loadModel('Type');
		$this->SysNotification->computeCount($owner);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $owner['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $owner['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $owner['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $owner['Type'][$avatar_mini]);
		}

		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $owner['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $owner['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}

		if (isset($this->request->query['page']) && ctype_digit($this->request->query['page']) && $this->request->query['page'] >= 1) $page = $this->request->query['page'];
		else $page = 0;
		if (isset($this->request->query['search'])) {
			$this->set('search', $this->request->query['search']);
			$search = $this->validate_string_input($this->request->query['search']);
		}
		else {
			$search = "";
			$this->set('search', $search);
		}

		$this->loadModel('Missionnature');
		$mn = $this->Missionnature->find('all');
		$this->set('missionsnature', $mn);
		$filter = 'any';
		if (isset($this->request->query['nature'])) {
			foreach ($mn as $n):
				if ($n == $this->request->query['nature']) $filter = $n;
			endforeach;
		}

		$this->set('filter', $filter);

		$startmillisec = $this->millisec();

		$this->loadModel('Missiondone');
		$this->loadModel('Media');
                $this->loadModel('Level');
		$this->loadModel('Type');
                $type = $this->Type->find('all');
                $this->set('type',$type);

                $this->loadModel('Saison');
                $saison = $this->Saison->find('all');
                $this->set('saison',$saison);



		// On récupère toute les infos pour pouvoir faire le classement
		$records = array();
		$records_nocl = array();

		// ceux qui ne sont pas classé
		$players = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id,Level.name,Level.id,Level.palier,Level.level,Player.pseudo,Player.experience,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini, Player.classement, Type.image4,Type.image5,Type.image6,Media.thumb',
			'order' => 'Player.experience DESC',
			'conditions' => array(
				'Player.user_id !=' => 1
			) ,
			'recursive' => 1
		));
        $this->Mission->virtualFields['experienceCalc']=0;
        $this->Mission->virtualFields['expPlayerId']=0;

        if ($_GET['nature']) {
            $_GET['nature'] = (int)$_GET['nature'];
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
                      FROM      missions     AS m,
                                missiondones AS md,
                                missionnatures AS mn
                      WHERE     md.mission_id = m.id
                      AND       m.missionnature_id='{$_GET['nature']}'
                      GROUP BY  md.player_id
                      ORDER BY Mission__experienceCalc DESC";
        }
        if ($_GET['classe']) {
            //$_GET['classe'] = (int)$_GET['classe'];
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId, t.slug
             FROM      missions     AS m,
                       missiondones AS md,
                       players AS p,
                       types AS t
             WHERE     md.mission_id = m.id
             AND       md.player_id = p.id
             AND       p.type_id = t.id
			 AND	   t.slug = '{$_GET['classe']}'
             GROUP BY  md.player_id
             ORDER BY Mission__experienceCalc DESC";

        }
        if ($_GET['amis']) {
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
              FROM      missions     AS m,
                        missiondones AS md,
                        relations     AS r
              WHERE     md.mission_id = m.id
              AND       md.player_id = r.user_to
              AND       '{$owner['Player']['id']}' =r.user_from
              GROUP BY  md.player_id
              ORDER BY Mission__experienceCalc DESC";

        }
        $now= date(y-m-d);
        if ($_GET['periode']) {
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
              FROM      missions     AS m,
                        missiondones AS md,
                        players AS p
              WHERE     md.mission_id = m.id
              AND       md.player_id = p.id
              AND		DATEDIFF(NOW(), FROM_UNIXTIME(md.time)) < '{$_GET['periode']}'
              GROUP BY  md.player_id
              ORDER BY Mission__experienceCalc DESC";
        }
        if ($_GET['saison']) {
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
              FROM      missions     AS m,
                        missiondones AS md,
                        players AS p,
                        saison AS s
              WHERE     md.mission_id = m.id
              AND       md.player_id = p.id
              AND		s.id = '{$_GET['saison']}'
              AND		FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND s.date_fin
              GROUP BY  md.player_id
              ORDER BY Mission__experienceCalc DESC";
        }
        if ($_GET['historique']) {
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
              FROM      missions     AS m,
                        missiondones AS md,
                        players AS p,
                        saison AS s
              WHERE     md.mission_id = m.id
              AND       md.player_id = p.id
              AND       s.intitule = 'saison_beta'
              AND		FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND NOW()
              GROUP BY  md.player_id
              ORDER BY Mission__experienceCalc DESC";
        }
		if ($_GET['class_avatar']) {
            $sql = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, t.id AS Mission__expPlayerId
              FROM      missions     AS m,
                        missiondones AS md,
                        types     AS t,
                        players AS p
              WHERE     md.mission_id = m.id
              AND   	p.type_id  = t.id
              GROUP BY  t.slug
              ORDER BY Mission__experienceCalc DESC";

        }

		/*if ($_GET['team']) {
		$sql = "SELECT SUM(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, thp.team_id AS Mission__expPlayerId
		FROM teams_has_players AS thp,
			 missiondones AS md,
			 missions AS m,
			  players AS p
		WHERE thp.player_id = p .id
		GROUP BY thp.team_id
		ORDER BY Mission__experienceCalc DESC";
		}*/
        if ($_GET['teamsaison']) {
			$sql_team = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, thp.team_id AS Mission__expPlayerId
			FROM teams_has_players AS thp, missions AS m,saison AS s, missiondones AS md 
			WHERE thp.`player_id` = md.player_id
            AND   md.mission_id = m.id
			AND   thp.`accepted` = 1
            AND   DATE(NOW()) BETWEEN s.date_debut AND s.date_fin
		    AND   FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND s.date_fin
			GROUP BY thp.team_id 
			ORDER BY Mission__experienceCalc DESC";
		}
		
		if ($sql){
			$classement_filtre = $this->Mission->query($sql);

			if ($classement_filtre)
			foreach ($classement_filtre as $key => $value)
			{
				$ExpByPlayer[$value['Mission']['expPlayerId']] = array(
					'experience' => $value['Mission']['experienceCalc'],
					'classement' => $key+1,
					);
			}
        }

		if ($sql_team){
			$classement_team_filtre = $this->Mission->query($sql_team);

			if ($classement_team_filtre)
			foreach ($classement_team_filtre as $key => $value)
			{
				$ExpByEquipe[$value['Mission']['expPlayerId']] = array(
					'experience' => $value['Mission']['experienceCalc'],
					'classement' => $key+1,
					);
			}
        }

		$this->loadModel('TeamsHasPlayers');
		$this->loadModel('Team');

		/**** recupérer le debut et la fin de la derniere saison****/
		$lastSaison = $this->Saison->find('first',array(
        							'order' => array(
                        			'id' => 'desc'
                    					))
        							);
		$this->set('lastSaison', $lastSaison);
        $debut_lastSaison = strtotime($lastSaison['Saison']['date_debut']);
        $fin_lastSaison = strtotime($lastSaison['Saison']['date_fin']);
        /***********************************************************/

		$equipes = $this->Team->find('all');
		//var_dump($equipes);

		$i = 0;
		if($_GET['team']) {
			foreach ($equipes as $equipe) {
			$classement_team = 0;
			$exp_team = 0;
				$exp_team= $equipe['Team']['experience'];
				if($sql_team) {
					if ($ExpByEquipe)
					{
						$exp_team = $ExpByEquipe[$equipe['Team']['id']]['experience'];
						$classement_team = $ExpByEquipe[$equipe['Team']['id']]['classement'];
					}
				}

				$avatar = $equipe['Team']['logo'];

				if ($exp_team == 0 && $equipe['Team']['classement'] != - 1) {
					$this->Team->id = $equipe['Team']['id'];
					$this->Team->saveField('classement', -1);
					$classement_team = - 1;
					// $pl['Player']['classement'] = - 1;
				}
				if ($exp_team != 0 && $equipe['Team']['classement'] != $i) {
					$this->Team->id = $equipe['Team']['id'];
					$this->Team->saveField('classement', $i);
				}

				$classement_team = $i;
				$victoires = 0;
				/*if ($ExpByequipe)
					$classement_team = $ExpByequipe[$equipe['Team']['id']]['classement'];*/

				/**** calculer missions reussi des joueurs de l'equipe****/
				foreach ($equipe['TeamsHasPlayers'] as $thp) {
					if ($thp['accepted']==1) {
						$missiondoneBy_player = $this->Missiondone->find('count', array(
															'fields' => array(
																'Missiondone.id'
															) ,
															'conditions' => array(
																'Missiondone.player_id' => $thp['player_id'],
																'sucess'				=> 1,
																'Missiondone.time >='	=>$debut_lastSaison,
													            'Missiondone.time <='	=>$fin_lastSaison
															)
														));
			    		$victoires = $victoires + $missiondoneBy_player ;
					}
				
				}
				/********************************************************/

				$record = array(
				'id'          => $equipe['Team']['id'],
				'name'        => $equipe['Team']['name'],
				'logo'        => $equipe['Team']['logo'],
				'devise'      => $equipe['Team']['devise'],
				'victoires'   => $victoires,
				'experience'  => $exp_team,
				'classement'  => $classement_team
				);
				if ($equipe['Team']['experience'] == 0) {
				array_push($records_nocl, $record);
			}
			else {
				array_push($records, $record);
			}

			$i = $i + 1;

			$data = $this->make_classement($records);
				foreach ($records_nocl as $rec) {
				array_push($data, $rec);
				}
			$top5 = array_slice($data, 0, 5);

		// on sélectionne les données a afficher
		if ($search != "") {
			$records_search = array();
			foreach ($data as $cl) {
				if (stristr($cl['name'], $search)) array_push($records_search, $cl);
			}
			$data = $this->classement_paginate($records_search, $page);
		}


			}
			/*$this->set('top5',$podium);
			$this->set('page',$page);*/
			$this->set('classement_team',$data);
			//var_dump($classement_team);
		}



		foreach ($players as $pl) {

            $victoires = count($pl['Missiondone']);
			$exp=$pl['Player']['experience'];
			if ($ExpByPlayer)
			{
				$exp=$ExpByPlayer[$pl['Player']['id']]['experience'];
			}

			if (isset($pl['Media']['thumb']))
				$profil_pict = $pl['Media']['thumb'];
			else $profil_pict = "";
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';

			if ($exp == 0 && $pl['Player']['classement'] != - 1) {
				$this->Player->id = $pl['Player']['id'];
				$this->Player->saveField('classement', -1);
				$classement = - 1;
				// $pl['Player']['classement'] = - 1;
			}
			if ($exp != 0 && $pl['Player']['classement'] != $i) {
				$this->Player->id = $pl['Player']['id'];
				$this->Player->saveField('classement', $i);
			}

			$classement = $i;
			if ($ExpByPlayer)
				$classement = $ExpByPlayer[$pl['Player']['id']]['classement'];

                        /*echo "<pre>";
                        print_r($db);
                        echo "</pre>";
                        exit;*/

                        // Change les valeur en fonction de la langue
                        $lang = Configure::read('Config.language');

                        $fields = get_class_vars('DATABASE_CONFIG');
                        $host = $fields["default"]["host"];
                        $login = $fields["default"]["login"];
                        $password = $fields["default"]["password"];
                        $database = $fields["default"]["database"];

                        $db = mysql_connect($host, $login, $password);
                        mysql_select_db($database,$db);
                        $sql = 'SELECT i18n.content
                                FROM i18n
                                LEFT JOIN levels
                                ON i18n.foreign_key=levels.id
                                WHERE levels.id = "'.$pl['Level']['id'].'"
                                AND i18n.model = "Level"
                                AND i18n.locale = "'.$lang.'"';
                        $req = mysql_query($sql);
                        $data = mysql_fetch_assoc($req);

                        $pl['Level']['name'] = $data["content"];

			$record = array(
				'id'          => $pl['Player']['id'],
				'pseudo'      => $pl['Player']['pseudo'],
				'classe'      => $pl['Type']['name'],
				'grade'       => $pl['Level']['name'],
				'avatar'      => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires'   => $victoires,
				'experience'  => $exp,
				'classement'  => $classement,
				'level'       => $pl['Level']['level'],
				'avatar_type' => $t['Type']['image1_mini'],
				'slug_type'	  => $t['Type']['slug'],
				'id_type'	  => $t['Type']['id']

			);

			if ($pl['Player']['experience'] == 0) {
				array_push($records_nocl, $record);
			}
			else {
				array_push($records, $record);
			}

			$i = $i + 1;

		}
		// on fait le classement globale ici
		$data = $this->make_classement($records);
		foreach ($records_nocl as $rec) {
			array_push($data, $rec);
		}

		$top5 = array_slice($data, 0, 5);

		// on sélectionne les données a afficher
		if ($search != "") {
			/*if ($page == 0) $page = $this->find_me($data, $owner['Player']['id']);
			$data = $this->classement_paginate($data, $page);
		}
		else {*/
			$records_search = array();
			foreach ($data as $cl) {
				if (stristr($cl['pseudo'], $search)) array_push($records_search, $cl);
			}
			$data = $this->classement_paginate($records_search, $page);
		}

		//$podium=$this->generate_podium($top5);
		//$this->set('top5',$top5);
		$this->set('top5',$podium);
		$this->set('page',$page);

		$this->set('classement',$data);


	}

	public function user_handle_questionnaire()
	{
		$user = $this->Session->read();
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => - 1
		));
		$playerid = $player['Player']['id'];

		if ($this->request->is('ajax')) {
			if (isset($this->request->query['valeurs'])) {
				$values = array();
				$reponses = $this->request->query['valeurs'];

				// validate input
				if ($reponses['@celebrite'] == "" || strlen($reponses['@celebrite']) < 3 || strlen($reponses['@celebrite']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Celebrité détestée'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@celebrite'] =

				/*$this->validate_string_input(*/
				$reponses['@celebrite'];

				//);
				if ($reponses['@destination'] == "" || strlen($reponses['@destination']) < 3 || strlen($reponses['@destination']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Destination de rêve'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@destination'] = $this->validate_string_input($reponses['@destination']);

				if ($reponses['@animal_prefere'] == "" || strlen($reponses['@animal_prefere']) < 3 || strlen($reponses['@animal_prefere']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Animal préféré'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@animal_prefere'] = $this->validate_string_input($reponses['@animal_prefere']);

				if ($reponses['@animal_deteste'] == "" || strlen($reponses['@animal_deteste']) < 3 || strlen($reponses['@animal_deteste']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Animal détesté'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@animal_deteste'] = $this->validate_string_input($reponses['@animal_deteste']);

				if ($reponses['@plat_deteste'] == "" || strlen($reponses['@plat_deteste']) < 3 || strlen($reponses['@plat_deteste']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Plat détesté'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@plat_deteste'] = $this->validate_string_input($reponses['@plat_deteste']);

				if ($reponses['@ami'] == "" || strlen($reponses['@ami']) < 3 || strlen($reponses['@ami']) > 50) {
					$message = array(
						'code' => - 1,
						'message' => 'Réponse manquante ou malformé pour Nom/Surnom de votre meilleur ami'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else $values['@ami'] = $this->validate_string_input($reponses['@ami']);
				$encoded = json_encode($values);

				if ($player['Player']['questionnaire'] == NULL) {
					$this->Player->id = $playerid;
					$this->Player->saveField('questionnaire', $encoded);
					$message = array(
						'code' => 1,
						'message' => 'Les informations ont bien été pris en compte et vous avez été crédité'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else {
					$this->Player->id = $playerid;
					$this->Player->saveField('questionnaire', $encoded);
					$message = array(
						'code' => 1,
						'message' => 'Les informations ont bien été pris en compte'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
			}
			$message = array(
				'code' => - 1,
				'message' => 'mauvais format de données'
			);
			echo json_encode($message);
			exit();
		}
	}

	function user_update_profil_pictures()
	{
		$this->loadModel('Player');
		$user = $this->Session->read();
		$owner = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => - 1
		));
		$playerid = $player['Player']['id'];
		$this->loadModel('Media');
		$pictures = $this->Media->find('all', array(
			'fields' => array(
				'Media.id, Media.file'
			) ,
			'conditions' => array(
				'Media.ref' => 'User',
				'Media.userid' => $owner['Player']['id'],
				'Media.visible' => 1
			) ,
			'recursive' => - 1
		));
		$pics = array();
		foreach ($pictures as $p) {
			$a = array(
				'id' => $p['Media']['id'],
				'file' => $p['Media']['file']
			);
			array_push($pics, $a);
		}
		$message = array(
			'code' => 1,
			'leader' => $owner['Player']['profil_id'],
			'pictures' => $pics
		);
		echo json_encode($message);
		exit();
	}
	function user_update_team_pictures()
	{
		$this->loadModel('Player');
		$user = $this->Session->read();
		$pl = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => - 1
		));
		$this->loadmodel('TeamsHasPlayers');
		$owner = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $owner['Player']['id'],
                 'accepted' => 1,
                 'captain'  => 1
			) ,
			'recursive' => 1
		));
		$this->set('my_team', $my_team);
		$this->loadModel('Teams');

		$playerid = $player['Player']['id'];
		$this->loadModel('Media');
		$pictures = $this->Media->find('all', array(
			'fields' => array(
				'Media.id, Media.file'
			) ,
			'conditions' => array(
				'Media.ref' => 'User',
				'Media.userid' => $owner['Player']['id'],
				'Media.visible' => 1
			) ,
			'recursive' => - 1
		));
		$pics = array();
		foreach ($pictures as $p) {
			$a = array(
				'id' => $p['Media']['id'],
				'file' => $p['Media']['file']
			);
			array_push($pics, $a);
		}
		$message = array(
			'code' => 1,
			'leader' => $owner['Player']['profil_id'],
			'pictures' => $pics
		);
		echo json_encode($message);
		exit();
	}

	public function user_load_messagerie()
	{

		/*  Récupération de la liste de contacts :
		   les contacts avec qui on est en discussion sinon,
		   la liste des héros du joueur */
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));

		/* C'est normalement une requête Ajax venant du Layout default.ctp */
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Relation');
			$friendslist = $this->Relation->find('list', array(
				'fields' => array(
					'Relation.user_to',
					'Relation.user_to'
				) ,
				'conditions' => array(
					'Relation.user_from' => $player['Player']['id']
				) ,
				'recursive' => - 1
			));
			$friends = array();
			$friends_not = array();
			foreach ($friendslist as $f) {
				array_push($friends, array(
					'Relation.user_from' => $f
				));
				array_push($friends_not, array(
					'Relation.user_to' => $f
				));
			}
			$propfriends = $this->Relation->find('list', array(
				'fields' => array(
					'Relation.user_to',
					'Relation.user_to'
				) ,
				'conditions' => array(
					'AND' => array(
						'OR' => $friends,
						'NOT' => $friends_not
					)
				) ,
				'recursive' => 1
			));

			$this->loadModel('Relation');
			$this->loadModel('Messagerie');

			/* on récupère la liste des players dont on a reçu des messages ou vers lequel on a émis des messages */

			/* Pour l'instant on fait 2 requêtes pour ne récupérer qu'une liste */
			$messagefriends_from = $this->Messagerie->find('list', array(
				'fields' => array(
					'Messagerie.playerid_from',
					'Messagerie.playerid_from'
				) ,
				'conditions' => array(
					'Messagerie.playerid_to' => $player['Player']['id']
				) ,
				'recursive' => - 1
			));
			$messagefriends_to = $this->Messagerie->find('list', array(
				'fields' => array(
					'Messagerie.playerid_to',
					'Messagerie.playerid_to'
				) ,
				'conditions' => array(
					'Messagerie.playerid_from' => $player['Player']['id']
				) ,
				'recursive' => - 1
			));

			/* on merge les deux listes */
			$messagefriends = array_unique(array_merge($messagefriends_from, $messagefriends_to));

			/* si vide alors récuperer la liste des héros */
			if (!isset($messagefriends[0])) {
				$friendslist = $this->Relation->find('list', array(
					'fields' => array(
						'Relation.user_to',
						'Relation.user_to'
					) ,
					'conditions' => array(
						'Relation.user_from' => $player['Player']['id']
					) ,
					'recursive' => - 1
				));
				$friends = $friendslist;
			}
			else $friends = $messagefriends;

			$player_pseudoDB = $this->Player->find('list', array(
				'fields' => array(
					'Player.id',
					'Player.pseudo'
				) ,
				'recursive' => - 1
			));
			$contacts = array();
			foreach ($friends as $key => $val) {
				if ($val != 1 && $val != $player['Player']['id']) {
					$nb_nonlu = $this->Messagerie->find('count', array(
						'conditions' => array(
							'Messagerie.lu' => 0,
							'Messagerie.playerid_to' => $player['Player']['id'],
							'Messagerie.playerid_from' => $val
						) ,
						'recursive' => - 1
					));
					$status = "monde";
					if (isset($friendslist[$val])) $status = "heros";
					if (isset($propfriends[$val])) $status = "prop";
					$record = array(
						'player_id' => $val,
						'player_pseudo' => $player_pseudoDB[$val],
						'nb_nonlu' => $nb_nonlu,
						'status' => $status
					);
					array_push($contacts, $record);
				}
			}
			echo json_encode(array(
				'code' => 1,
				'data' => $contacts
			));
			exit();
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_get_messagerie_nonlu()
	{

		/* Récupération du nombre de messages non lu */
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Messagerie');
			$messages_nonlu = $this->Messagerie->find('count', array(
				'conditions' => array(
					'Messagerie.lu' => 0,
					'Messagerie.playerid_to' => $player['Player']['id']
				) ,
				'recursive' => - 1
			));
			echo json_encode(array(
				'code' => 1,
				'nonlu' => $messages_nonlu
			));
			exit();
		}
		else {
			echo json_encode(array(
				'code' => - 1
			));
			exit();
		}
	}

	public function user_load_messagerie_search()
	{

		/* Récupération de la liste des joueurs dont le pseudo ou l'adresse mail
		 ressemble a la valeur recherché par le joueur (champ de recherche de contacts) */
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$search = NULL;
			if ($this->request->query['search'] != '') {
				$search = $this->validate_string_input($this->request->query['search']);
			}
			if ($search == NULL) {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'pas d\'indice de recherche'
				));
				exit();
			}
			else {
				$contacts_search = $this->Player->find("all", array(
					'fields' => array(
						'Player.id,Player.pseudo'
					) ,
					'conditions' => array(
						'OR' => array(
							array(
								"User.username LIKE" => '%' . $search . '%'
							) ,
							array(
								"User.email LIKE" => '%' . $search . '%'
							)
						)
					) ,
					'recursive' => 1
				));
			}
			$this->loadModel('Relation');
			$friendslist = $this->Relation->find('list', array(
				'fields' => array(
					'Relation.user_to',
					'Relation.user_to'
				) ,
				'conditions' => array(
					'Relation.user_from' => $player['Player']['id']
				) ,
				'recursive' => - 1
			));
			$friends = array();
			$friends_not = array();
			foreach ($friendslist as $f) {
				array_push($friends, array(
					'Relation.user_from' => $f
				));
				array_push($friends_not, array(
					'Relation.user_to' => $f
				));
			}
			$propfriends = $this->Relation->find('list', array(
				'fields' => array(
					'Relation.user_to',
					'Relation.user_to'
				) ,
				'conditions' => array(
					'AND' => array(
						'OR' => $friends,
						'NOT' => $friends_not
					)
				) ,
				'recursive' => 1
			));

			$this->loadModel("Messagerie");
			$contacts = array();
			foreach ($contacts_search as $f) {
				if ($f['Player']['id'] != 1 && $f['Player']['id'] != $player['Player']['id']) {
					$count = $this->Messagerie->find('count', array(
						'conditions' => array(
							'Messagerie.playerid_from' => $f['Player']['id'],
							'Messagerie.playerid_to' => $player['Player']['id'],
							'Messagerie.lu' => 0
						) ,
						'recursive' => - 1
					));
					$status = "monde";
					if (isset($friendslist[$f['Player']['id']])) $status = "heros";
					if (isset($propfriends[$f['Player']['id']])) $status = "prop";
					$record = array(
						'player_id' => $f['Player']['id'],
						'player_pseudo' => $f['Player']['pseudo'],
						'nonlu' => $count,
						'status' => $status
					);
					array_push($contacts, $record);
				}
			}
			echo json_encode(array(
				'code' => 1,
				'data' => $contacts
			));
			exit();
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_update_contact_messages()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			if ($this->request->query['id'] != '') {
				$contact = $this->Player->find('first', array(
					'conditions' => array(
						'Player.id' => $this->request->query['id']
					) ,
					'recursive' => 1
				));
				if (!isset($contact['Player']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Contact inexistant'
					));
					exit();
				}
				$this->loadModel('Relation');
				$friendslist = $this->Relation->find('list', array(
					'fields' => array(
						'Relation.user_to',
						'Relation.user_to'
					) ,
					'conditions' => array(
						'Relation.user_from' => $player['Player']['id']
					) ,
					'recursive' => - 1
				));
				$friends = array();
				$friends_not = array();
				foreach ($friendslist as $f) {
					array_push($friends, array(
						'Relation.user_from' => $f
					));
					array_push($friends_not, array(
						'Relation.user_to' => $f
					));
				}
				$propfriends = $this->Relation->find('list', array(
					'fields' => array(
						'Relation.user_to',
						'Relation.user_to'
					) ,
					'conditions' => array(
						'AND' => array(
							'OR' => $friends,
							'NOT' => $friends_not
						)
					) ,
					'recursive' => 1
				));

				$this->loadModel('Messagerie');
				$messages_contact = $this->Messagerie->find('all', array(
					'conditions' => array(
						'Messagerie.playerid_to' => $player['Player']['id'],
						'Messagerie.playerid_from' => $contact['Player']['id'],
						'Messagerie.lu' => 0
					) ,
					'order' => 'Messagerie.time ASC',
					'recursive' => - 1
				));
				$messages_nonlu = $this->Messagerie->find('list', array(
					'fields' => array(
						'Messagerie.playerid_from',
						'Messagerie.playerid_from'
					) ,
					'conditions' => array(
						'Messagerie.playerid_to' => $player['Player']['id'],
						'Messagerie.playerid_from !=' => $contact['Player']['id'],
						'Messagerie.lu' => 0
					) ,
					'order' => 'Messagerie.time ASC',
					'recursive' => - 1
				));
				if (isset($contact['Media']['thumb'])) $profil_pict = $contact['Media']['thumb'];
				else {
					$avatar_mini = 'image' . $contact['Level']['palier'] . '_mini';
					$profil_pict = $contact['Type'][$avatar_mini];
				}

				$messages = array();
				foreach ($messages_contact as $m) {
					$record = array(
						'mine' => 0,
						'player_id' => $m['Messagerie']['playerid_from'],
						'player_pseudo' => $contact['Player']['pseudo'],
						'player_picture' => $profil_pict,
						'time' => $m['Messagerie']['time'],
						'message' => stripslashes($m['Messagerie']['message'])
					);
					$this->Messagerie->id = $m['Messagerie']['id'];
					$this->Messagerie->saveField('lu', 1);
					array_push($messages, $record);
				}

				$nonlu = array();
				foreach ($messages_nonlu as $key => $id_contact) {
					$count = $this->Messagerie->find('count', array(
						'conditions' => array(
							'Messagerie.playerid_to' => $player['Player']['id'],
							'Messagerie.playerid_from' => $id_contact,
							'Messagerie.lu' => 0
						) ,
						'recursive' => - 1
					));
					$pseudo = $this->Player->find('first', array(
						'conditions' => array(
							'Player.id' => $id_contact
						) ,
						'recursive' => - 1
					));
					$status = "monde";
					if (isset($friendslist[$id_contact])) $status = "heros";
					if (isset($propfriends[$id_contact])) $status = "prop";
					$record = array(
						'id_contact' => $id_contact,
						'nonlu' => $count,
						'pseudo_contact' => $pseudo['Player']['pseudo'],
						'status' => $status
					);
					array_push($nonlu, $record);
				}
				echo json_encode(array(
					'code' => 1,
					'data' => $messages,
					'nonlu' => $nonlu
				));
				exit();
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Mauvaise requête'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_load_contact_messages()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			if ($this->request->query['id'] != '') {
				$contact = $this->Player->find('first', array(
					'conditions' => array(
						'Player.id' => $this->request->query['id']
					) ,
					'recursive' => 1
				));
				if (!isset($contact['Player']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Contact inexistant'
					));
					exit();
				}
				$this->loadModel('Messagerie');
				$messagesDB = $this->Messagerie->find('all', array(
					'conditions' => array(
						'OR' => array(
							array(
								'Messagerie.playerid_from' => $player['Player']['id'],
								'Messagerie.playerid_to' => $contact['Player']['id']
							) ,
							array(
								'Messagerie.playerid_to' => $player['Player']['id'],
								'Messagerie.playerid_from' => $contact['Player']['id']
							)
						)
					) ,
					'order' => 'Messagerie.time ASC',
					'recursive' => - 1
				));

				if (isset($contact['Media']['thumb'])) $profil_pict = $contact['Media']['thumb'];
				else {
					$avatar_mini = 'image' . $contact['Level']['palier'] . '_mini';
					$profil_pict = $contact['Type'][$avatar_mini];
				}

				$messages = array();
				foreach ($messagesDB as $m) {
					if ($m['Messagerie']['playerid_from'] == $player['Player']['id']) $record = array(
						'mine' => 1,
						'player_id' => $m['Messagerie']['playerid_from'],
						'player_pseudo' => 'Moi',
						'time' => $m['Messagerie']['time'],
						'message' => stripslashes($m['Messagerie']['message'])
					);
					else {
						$record = array(
							'mine' => 0,
							'player_id' => $m['Messagerie']['playerid_from'],
							'player_pseudo' => $contact['Player']['pseudo'],
							'player_picture' => $profil_pict,
							'time' => $m['Messagerie']['time'],
							'message' => stripslashes($m['Messagerie']['message'])
						);
						$this->Messagerie->id = $m['Messagerie']['id'];
						$this->Messagerie->saveField('lu', 1);
					}
					array_push($messages, $record);
				}
				echo json_encode(array(
					'code' => 1,
					'data' => $messages
				));
				exit();
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Identifiant du contact manquant'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_send_messagerie()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			if ($this->request->query['id'] != '' && $this->request->query['message'] != '') {
				$contact = $this->Player->find('first', array(
					'conditions' => array(
						'Player.id' => $this->request->query['id']
					) ,
					'recursive' => - 1
				));
				if (!isset($contact['Player']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Contact inexistant'
					));
					exit();
				}
				$message = $this->validate_string_input($this->request->query['message']);
				if (strlen($message) >= 10000) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Message trop long'
					));
					exit();
				}
				$this->loadModel('Messagerie');
				$message_info = array(
					'Messagerie' => array(
						'playerid_from' => $player['Player']['id'],
						'playerid_to' => $contact['Player']['id'],
						'time' => time() ,
						'lu' => 0,
						'message' => $message
					)
				);
				$this->Messagerie->create();
				if ($this->Messagerie->save($message_info)) {
					echo json_encode(array(
						'code' => 1
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Echec sauvegarde'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Identifiant du contact manquant'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_set_devise()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Devise');
			if ($this->request->query['valeur'] != '') {
				$devise = $this->validate_string_input($this->request->query['valeur']);
				$devise_info = array(
					'Devise' => array(
						'player_id' => $player['Player']['id'],
						'devise' => $devise,
						'time' => time()
					)
				);
				$this->Devise->create();
				if ($this->Devise->save($devise_info)) {
					$this->Player->id = $player['Player']['id'];
					$this->Player->saveField('devise_id', $this->Devise->id);
					echo json_encode(array(
						'code' => 1
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur enregistrement'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Devise vide'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_set_deviseTeam()
	{
		$this->loadModel('Player');
		$user = $this->Session->read();
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));

		$this->loadModel('TeamsHasPlayers');

		$my_team = $this->TeamsHasPlayers->find('first',array(
												'conditions' => array(
													'TeamsHasPlayers.player_id' => $player['Player']['id']
													),
												));
		$captain = $this->Player->find('first', array(
			'conditions' => array(
				'Player.id' => $my_team['Teams']['player_id'],
				 $my_team['Teams']['captain']=> 1
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Devise');
			if ($this->request->query['valeur'] != '') {
				$devise = $this->validate_string_input($this->request->query['valeur']);
				$devise_info = array(
					'Devise' => array(
						'player_id' => $player['Player']['id'],
						'devise' => $devise,
						'time' => time()
					)
				);
				$this->Devise->create();
				if ($this->Devise->save($devise_info)) {
					$this->Player->id = $player['Player']['id'];
					$this->Player->saveField('devise_id', $this->Devise->id);
					echo json_encode(array(
						'code' => 1
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur enregistrement'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Devise vide'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_add_profil_comment()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Profilcomm');
			if ($this->request->query['commentaire'] != '' && $this->request->query['id_target'] != '') {
				$comment = $this->validate_string_input($this->request->query['commentaire']);
				$id_target = $this->request->query['id_target'];
				$target = $this->Player->find('first', array(
					'conditions' => array(
						'id' => $id_target
					) ,
					'recursive' => - 1
				));
				if (!isset($target['Player']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur identification joueur'
					));
					exit();
				}
				$profilcomm_info = array(
					'Profilcomm' => array(
						'playerfrom_id' => $player['Player']['id'],
						'commentaire' => $comment,
						'time' => time() ,
						'playerto_id' => $target['Player']['id']
					)
				);
				$this->Profilcomm->create();
				if ($this->Profilcomm->save($profilcomm_info)) {
					echo json_encode(array(
						'code' => 1
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur enregistrement'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Commentaire vide'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_addTeam_profil_comment()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		$this->loadModel('Team');
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Profilcomm');
			if ($this->request->query['commentaire'] != '' && $this->request->query['id_target'] != '') {
				$comment = $this->validate_string_input($this->request->query['commentaire']);
				$id_target = $this->request->query['id_target'];
				$target = $this->Team->find('first', array(
					'conditions' => array(
						'id' => $id_target
					) ,
					'recursive' => - 1
				));
				if (!isset($target['Team']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur identification joueur'
					));
					exit();
				}
				$profilcomm_info = array(
					'Profilcomm' => array(
						'playerfrom_id' => $player['Player']['id'],
						'commentaire' => $comment,
						'time' => time() ,
						'teamTo_id' => $target['Team']['id']
					)
				);
				$this->Profilcomm->create();
				if ($this->Profilcomm->save($profilcomm_info)) {
					echo json_encode(array(
						'code' => 1
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur enregistrement'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Commentaire vide'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	public function user_add_devise_like()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		if ($this->request->is('ajax') && $player['Player']['id'] != 1) {
			$this->loadModel('Deviselike');
			if ($this->request->query['like'] != '' && $this->request->query['id_target'] != '') {
				$like = $this->request->query['like'];
				$id_target = $this->request->query['id_target'];
				$target = $this->Player->find('first', array(
					'conditions' => array(
						'devise_id' => $id_target
					) ,
					'recursive' => - 1
				));
				if (!isset($target['Player']['id']) || $target['Player']['devise_id'] == NULL || $target['Player']['id'] == $player['Player']['id']) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur identification joueur'
					));
					exit();
				}
				$firsttime = $this->Deviselike->find('first', array(
					'conditions' => array(
						'devise_id' => $id_target,
						'player_id' => $player['Player']['id']
					) ,
					'recursive' => - 1
				));
				if (isset($firsttime['Deviselike']['id'])) {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Vous avez déjà trippé/bad trippé sur cette devise'
					));
					exit();
				}
				if ($like == 1) $deviselike_info = array(
					'Deviselike' => array(
						'player_id' => $player['Player']['id'],
						'trip' => 1,
						'time' => time() ,
						'devise_id' => $target['Player']['devise_id'],
						'badtrip' => 0
					)
				);
				else $deviselike_info = array(
					'Deviselike' => array(
						'player_id' => $player['Player']['id'],
						'trip' => 0,
						'time' => time() ,
						'devise_id' => $target['Player']['devise_id'],
						'badtrip' => 1
					)
				);
				$this->Deviselike->create();
				if ($this->Deviselike->save($deviselike_info)) {
					if ($like == 1) $devise_like_nb = $this->Deviselike->find('count', array(
						'conditions' => array(
							'Deviselike.devise_id' => $id_target,
							'Deviselike.trip' => 1
						) ,
						'recursive' => - 1
					));
					else $devise_like_nb = $this->Deviselike->find('count', array(
						'conditions' => array(
							'Deviselike.devise_id' => $id_target,
							'Deviselike.trip' => 0
						) ,
						'recursive' => - 1
					));
					echo json_encode(array(
						'code' => 1,
						'val' => $devise_like_nb
					));
					exit();
				}
				else {
					echo json_encode(array(
						'code' => - 1,
						'message' => 'Erreur enregistrement'
					));
					exit();
				}
			}
			else {
				echo json_encode(array(
					'code' => - 1,
					'message' => 'Pas de valeurs'
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1,
				'message' => 'Mauvaise requête'
			));
			exit();
		}
	}

	function handle_code($codeenv)
	{
		$this->loadModel('Code');
		$code = $this->Code->find('first', array(
			'conditions' => array(
				'code' => $codeenv['codemission']['code'],
				'use' => 0
			) ,
			'recursive' => 0
		));

		if (isset($code['Code']['code'])) {
			if (strcmp($code['Code']['code'], $codeenv['codemission']['code']) == 0) {
				$user = $this->Session->read();
				$this->loadModel('Mission');
				$mission = $this->Mission->find('first', array(
					'fields' => array(
						'id',
						'pointsucces',
						'phrase_succes',
						'phrase_visite',
						'pointfail',
						'numupload',
						'title'
					) ,
					'conditions' => array(
						'Mission.id' => $code['Code']['mission_id']
					) ,
					'recursive' => 0
				));
				$this->loadModel('Player');
				$player = $this->Player->find('first', array(
					'conditions' => array(
						'user_id' => $user['Auth']['User']['id']
					) ,
					'recursive' => 1
				));
				$this->loadModel('Missiondone');

				$missiondone = array(
					'Missiondone' => array(
						'time' => time() ,
						'sucess' => $code['Code']['succes'],
						'player_id' => $player['Player']['id'],
						'mission_id' => $mission['Mission']['id'],
						'numupload' => $mission['Mission']['numupload']
					)
				);

				$this->Missiondone->create();
				if ($this->Missiondone->save($missiondone)) {
					$this->Session->setFlash(__('The mission has been saved'));
				}
				else {
					$this->Session->setFlash(__('The mission could not be saved. Please, try again.'));
					return;
				}
				$id_missiondone = $this->Missiondone->id;
				$this->Code->updateAll(array(
					'use' => 1
				) , array(
					'use =' => 0,
					'id =' => $code['Code']['id']
				));
				if ($code['Code']['succes'] == 0) {
					$new_experience = $mission['Mission']['pointfail'] + $player['Player']['experience'];
					$this->Player->updateAll(array(
						'experience' => $new_experience
					) , array(
						'Player.id' => $player['Player']['id']
					));
					$this->loadModel('Level');
					$this->loadModel('Notification');
					$dblevels = $this->Level->find('all', array(
						'conditions' => array(
							'Level.type_id' => $player['Type']['id'],
							'Level.experience >' => $new_experience
						) ,
						'order' => 'Level.experience ASC',
						'recursive' => 0
					));
					if ($player['Player']['level_id'] != $dblevels[0]['Level']['id'] && $player['Level']['level'] < $dblevels[0]['Level']['level']) {
						$this->Notification->create();
						$option = array(
							'player_id' => $player['Player']['id'],
							'timestamp' => time() ,
							'classe' => 'levelup',
							'ref_id' => $id_missiondone,
							'classe_id' => $dblevels[0]['Level']['id']
						);
						$this->Notification->save($option);
						if ($player['Level']['palier'] != $dblevels[0]['Level']['palier']) {
							$this->Notification->create();
							$option = array(
								'player_id' => $player['Player']['id'],
								'timestamp' => time() ,
								'classe' => 'palierup',
								'ref_id' => $id_missiondone,
								'classe_id' => $dblevels[0]['Level']['id']
							);
							$this->Notification->save($option);
						}
						$this->Player->id = $player['Player']['id'];
						$this->Player->saveField('level_id', $dblevels[0]['Level']['id']);
					}
					$this->Session->setFlash(__('Vous venez de valider votre tentative pour la mission "' . $mission['Mission']['title'] . '" et vous avez gagné  ' . $mission['Mission']['pointfail'] . ' points. ' . $mission['Mission']['phrase_visite']) , 'codesuccess');
				}
				else {
					$new_experience = $mission['Mission']['pointsucces'] + $player['Player']['experience'];
					$this->Player->updateAll(array(
						'experience' => $new_experience
					) , array(
						'Player.id' => $player['Player']['id']
					));
					$this->loadModel('Level');
					$this->loadModel('Notification');
					$dblevels = $this->Level->find('all', array(
						'conditions' => array(
							'Level.type_id' => $player['Type']['id'],
							'Level.experience >' => $new_experience
						) ,
						'order' => 'Level.experience ASC',
						'recursive' => 0
					));
					if ($player['Player']['level_id'] != $dblevels[0]['Level']['id'] && $player['Level']['level'] < $dblevels[0]['Level']['level']) {
						$this->Notification->create();
						$option = array(
							'player_id' => $player['Player']['id'],
							'timestamp' => time() ,
							'classe' => 'levelup',
							'ref_id' => $id_missiondone,
							'classe_id' => $dblevels[0]['Level']['id']
						);
						$this->Notification->save($option);
						if ($player['Level']['palier'] != $dblevels[0]['Level']['palier']) {
							$this->Notification->create();
							$option = array(
								'player_id' => $player['Player']['id'],
								'timestamp' => time() ,
								'classe' => 'palierup',
								'ref_id' => $id_missiondone,
								'classe_id' => $dblevels[0]['Level']['id']
							);
							$this->Notification->save($option);
						}
						$this->Player->id = $player['Player']['id'];
						$this->Player->saveField('level_id', $dblevels[0]['Level']['id']);
					}
					$this->Session->setFlash(__('Vous venez de valider la mission "' . $mission['Mission']['title'] . '" et vous avez gagné  ' . $mission['Mission']['pointsucces'] . ' points. ' . $mission['Mission']['phrase_succes']) , 'codesuccess');
				}
			}
			else {
				$this->Session->setFlash('Mauvais code ou code déjà utilisé.', 'codefail');
			}
		}
		else {
			$this->Session->setFlash('Mauvais code ou code déjà utilisé.', 'codefail');
		}
		$this->compute_classement();
	}

	public function user_relationsbeta()
	{
		$this->loadModel('Player');
		$user = $this->Session->read();
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
		$this->set('player', $player);
		$this->SysNotification->computeCount($player);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}
		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}

		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}

		$this->loadModel('Missiondone');
		$this->loadModel('Media');
		$this->loadModel('Type');
		$this->loadModel('Level');

		if (isset($this->request->query['search'])) {
			$this->set('search', $this->request->query['search']);
			$search = $this->validate_string_input($this->request->query['search']);
		}
		else {
			$search = "";
			$this->set('search', $search);
		}

		$records = array();
		if ($search == "") {
			$playerDB = $this->Player->find('all', array(
				'fields' => 'Player.profil_id,Player.id,Player.type_id,Level.name,Level.palier,Level.level,Player.pseudo,Player.experience,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini, Player.classement,Type.image4,Type.image5,Type.image6,Media.thumb',
				'order' => 'Player.experience DESC',
				'recursive' => 1
			));
		}
		else {
			$playerDB = $this->Player->find('all', array(
				'fields' => 'User.email, Player.profil_id,Player.id,Player.type_id,Level.name,Level.palier,Level.level,Player.pseudo,Player.experience,Type.name,Type.image1,Type.image1_mini,Type.image2, Type.image2_mini,Type.image3,Type.image3_mini, Player.classement,Type.image4,Type.image5,Type.image6,Media.thumb',
				'conditions' => array(
					'OR' => array(
						array(
							"Player.pseudo LIKE" => '%' . $search . '%'
						) ,
						array(
							"User.email LIKE" => '%' . $search . '%'
						)
					)
				) ,
				'order' => 'Player.experience DESC',
				'recursive' => 1
			));
		}

		foreach ($playerDB as $pl) {
			$victoires = count($pl['Missiondone']);
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name'],
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'experience' => $pl['Player']['experience'],
				'level' => $pl['Level']['level']
			);
			array_push($records, $record);
		}
		$playerDB = Hash::combine($records, '{n}.id', '{n}');
		$this->loadModel('Relation');
		$friendslist = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'Relation.user_from' => $player['Player']['id']
			) ,
			'recursive' => - 1
		));
		$friends = array();
		$friends_not = array();
		foreach ($friendslist as $f) {
			array_push($friends, array(
				'Relation.user_from' => $f
			));
			array_push($friends_not, array(
				'Relation.user_to' => $f
			));
		}
		$propfriends = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'AND' => array(
					'OR' => $friends,
					'NOT' => $friends_not
				)
			) ,
			'recursive' => 1
		));
		$this->set('playersDB', $playerDB);
		$this->set('friends', $friendslist);
		$this->set('propfriends', $propfriends);
	}

        public function user_accepterInvit()
	{
            $user = $this->Session->read();
            $team_id = $this->params['url']['id'];


            //Recupère le nombre de membre actuel du groupe
            $this->loadModel('TeamsHasPlayers');
            $isFullTeam = $this->TeamsHasPlayers->find('all', array(
            'conditions' => array(
                    'accepted' => "1",
                    'team_id' => $team_id
            )
            ));
            $nbrMembre = count($isFullTeam);
            //Vérifie que le groupe n'est pas plein (4membre max)
            if ($nbrMembre < "4"){
                //Recupère l'id du player
                $this->loadModel('Player');
                $arrPlayer = $this->Player->find('first', array(
                	'conditions' => array(
                		'user_id' => $user['Auth']['User']['id']
                	) ,
                	'recursive' => 1
                ));

                //Recupère si le player a déjà accepté une équipe
                $this->loadModel('TeamsHasPlayers');
                $existTeam = $this->TeamsHasPlayers->find('first', array(
                'conditions' => array(
                        'accepted' => "1",
                        'player_id' => $arrPlayer['Player']['id']
                )
                ));
                //Si le player n'as pas déja accepté une autre équipe
                if ($existTeam['TeamsHasPlayers']['player_id'] == ""){
                    //On accepte je joueur dans le groupe
                    $this->loadModel('TeamsHasPlayers');
                    $this->TeamsHasPlayers->updateAll(
                        array('accepted' => "1"),
                        array('player_id' => $arrPlayer['Player']['id'], 'team_id' => $team_id)
                    );
                    //Recupère si le player a d'autres demandes en attentes
                    $this->loadModel('TeamsHasPlayers');
                    $invitAtt = $this->TeamsHasPlayers->find('all', array(
                    'conditions' => array(
                            'accepted' => "0",
                            'player_id' => $arrPlayer['Player']['id']
                    )
                    ));
                    //Si le players a d'autres demandes en attentes
                    if($invitAtt["0"]['TeamsHasPlayers']['player_id'] != ""){

                        //On supprime les autres demandes en attentes
                        $this->loadModel('TeamsHasPlayers');
                        $conditions = array(
                            'accepted' => "0",
                            'player_id' => $arrPlayer['Player']['id']
                        );
                        $this->TeamsHasPlayers->deleteAll($conditions, false);
                    }
                }
                else{
                    echo "Erreur vous avez déjà une équipe";
                    $this->Session->setFlash("Vous avez déjà une équipe.", 'codefail');
                    //On redirige
                    $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));
                    exit;
                }
            }
            else{
                echo "le groupe est plein";
                $this->Session->setFlash("Vous ne pouvez pas rejoindre ce groupe car il est plein.", 'codefail');
                //On redirige
                $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));
                exit;
            }

            $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));
        }

        public function user_refuserInvit()
	{
            $user = $this->Session->read();
            $team_id = $this->params['url']['id'];
            //Refuse l'invitation (renseigner $team_id)

            //Recupère l'id du player
            $this->loadModel('Player');
           $arrPlayer = $this->Player->find('first', array(
           	'conditions' => array(
           		'user_id' => $user['Auth']['User']['id']
           	) ,
           	'recursive' => 1
           ));
            //Supprime la ligne de la table TeamsHasPlayers
            $this->loadModel('TeamsHasPlayers');
            $conditions = array(
                'accepted' => "0",
                'team_id' => $team_id,
                'player_id' => $arrPlayer['Player']['id']
            );

            $this->TeamsHasPlayers->deleteAll($conditions, false);

            $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));
        }

        public function user_quitTeam()
        {

            $user = $this->Session->read();
            $team_id = $this->params['url']['id'];
            //Supprime un player d'une équipe (renseigner $team_id)

            //Recupère l'id du player
            $this->loadModel('Player');
            $arrPlayer = $this->Player->find('first', array(
            	'conditions' => array(
            		'user_id' => $user['Auth']['User']['id']
            	) ,
            	'recursive' => 1
            ));
            //Recupère si le player est le captain
            $this->loadModel('TeamsHasPlayers');
            $isCaptain = $this->TeamsHasPlayers->find('first', array(
            'conditions' => array(
                    'captain' => "1",
                    'player_id' => $arrPlayer['Player']['id'],
                    'team_id' => $team_id
            )
            ));
            //Si le player n'est pas le captain
            if ($isCaptain['TeamsHasPlayers']['player_id'] == ""){
                //Supprime la ligne de la table TeamsHasPlayers
                $this->loadModel('TeamsHasPlayers');
                $conditions = array(
                    'team_id' => $team_id,
                    'captain' => "0",
                    'accepted' => "1",
                    'player_id' => $arrPlayer['Player']['id']
                );
                $this->TeamsHasPlayers->deleteAll($conditions, false);
            }
            //Si c'est le captain
            else{
                //Recupère la personne la plus ancienne du groupe
                $this->loadModel('TeamsHasPlayers');
                $older = $this->TeamsHasPlayers->find('first', array(
                    'conditions' => array(
                        'captain' => "0",
                        'accepted' => "1",
                        'team_id' => $team_id
                    ),
                    'order' => array(
                        'id' => 'asc'
                    )
                ));

                //Passe le lead au plus ancien
                $this->TeamsHasPlayers->updateAll(
                    array('captain' => "1", 'team_id' => $team_id ),
                    array('player_id' => $older['Players']['id'])
                );

                //Supprime la ligne de la table TeamsHasPlayers de l'ancien leader
                $this->loadModel('TeamsHasPlayers');
                $conditions = array(
                    'accepted' => "1",
                    'captain' => "1",
                    'team_id' => $team_id,
                    'player_id' => $arrPlayer['Player']['id']
                );

                $this->TeamsHasPlayers->deleteAll($conditions, false);
            }

            //On récupères les membres de l'équipe
            $this->loadModel('TeamsHasPlayers');
            $etatTeam = $this->TeamsHasPlayers->find('all', array(
                'conditions' => array(
                    'team_id' => $team_id
                )
            ));
            $nbrMembre = count($etatTeam);
            //Si l'équipe n'as pas aucun membre, on la supprime
            if($nbrMembre == "0"){
                $this->loadModel('Teams');
                $conditions = array(
                    'id' => $team_id
                );

                $this->Teams->deleteAll($conditions, false);
            }
            //On redirige
            $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));

        }
        public function user_askInvit()
	{
            $ids = $this->params['url']['id'];
            $id = explode("-", $ids);
            $idTeam = $id["0"];
            $idPlayer = $id["1"];
            
            //Récupère le pseudo du joueur
            $this->loadModel('Players');
            $joueurInfos = $this->Players->find('first', array(
                'conditions' => array(
                        'id' => $idPlayer
                )
            ));
            //$joueurInfos["Players"]["pseudo"]
            
            //Récupère l'id du captain du groupe
            $this->loadModel('TeamsHasPlayers');
            $captain = $this->TeamsHasPlayers->find('first', array(
                'conditions' => array(
                        'team_id' => $idTeam,
                        'captain' => "1"
                )
            ));
            $idCaptain = $captain["TeamsHasPlayers"]["player_id"];
            
            //Récupère user_id et pseudo du captain
            $this->loadModel('Players');
            $captainInfos = $this->Players->find('first', array(
                'conditions' => array(
                        'id' => $idCaptain
                )
            ));
            
            $this->loadModel('Users');
            $captainInfosUser = $this->Users->find('first', array(
                'conditions' => array(
                        'id' => $captainInfos["Players"]["user_id"]
                )
            ));
            
            //Création dans la table (2 équivaut a demande d'invit)
            $addInvit["TeamsHasPlayers"] = array("team_id" => $idTeam, "player_id" => $idPlayer, "accepted" => "2", "captain" => "0") ;
            
            $this->TeamsHasPlayers->create();
            $this->TeamsHasPlayers->save($addInvit);
            
            
            $pseudoDestinataire = $captainInfos["Players"]["pseudo"];
            $mailDestinataire = $captainInfosUser["Users"]["email"];     

            //Envoi un mail aux invités
            App::uses('CakeEmail', 'Network/Email');
            $mail = new CakeEmail();
            $mail->from('info@intripid.fr')->to($mailDestinataire)->subject($joueurInfos["Players"]["pseudo"].' souhaite rejoindre votre équipe')->emailFormat('html')->template('askinvit')->viewVars(array(
                    'username' => $captainInfos["Players"]["pseudo"],
                    'usernameFriend' => $joueurInfos["Players"]["pseudo"],
                    'link' => "http://www.intripid.fr/hero/".$captainInfos["Players"]["pseudo"]
                ))->send();
            
            $this->Session->setFlash("Demande envoyée", 'codesuccess');
            //On redirige
            $this->redirect(array('controller' => 'users', 'action' => 'user_relations'));
            
        }
	public function user_relations()
	{


		$user = $this->Session->read();
                $this->loadModel('User');
                if ($this->request->is('post')) {
                    //Mettre à jour amis facebook
                   if ($this->request->data['majFb']['fbid'] != ""){
                        $fbidamis = $this->request->data['majFb']['fbidamis'];
                        $fbid = $this->request->data['majFb']['fbid'];
                        $this->User->updateAll(
                            array('fbidamis' => "'$fbidamis'", 'fbid' => "'$fbid'"),
                            array('id' => $user['Auth']['User']['id'])
                        );
                    }
                    if ($this->request->data['majFb']['fbid'] == ""){
                        $erreur = "Erreur : Vous n'étiez pas connecté à facebook. Avant de cliquez sur <b>mettre à jour mes informations</b>, veuillez cliquez sur le boutton de connexion facebook.";
                        $this->set('erreur', $erreur);
                    }

                }


		$this->set("title_for_layout", "Relations");
		$this->set("show_concept", 0);
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));                
		$this->set('player', $player);
                $this->loadModel('TeamsHasPlayers');
		$playerHasTeam = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $player["Player"]["id"],
                                'accepted' => "1"
			) ,
			'recursive' => 1
		));
                $this->set('playerHasTeam', $playerHasTeam);
		$this->SysNotification->computeCount($player);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		$this->loadModel('TeamsHasPlayers');
		$my_team = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $player['Player']['id'],
                                'accepted' => '1'
			) ,
			'recursive' => 1
		));
		$this->set('my_team', $my_team);

                //Recupère tous les membres du groupe
                $this->loadModel('TeamsHasPlayers');
		$membreTeam = $this->TeamsHasPlayers->find('all', array(
			'conditions' => array(
				'team_id' => $my_team['TeamsHasPlayers']['team_id'],
                                'accepted' => '1'
			) ,
			'recursive' => 1
		));
		$this->set('membreTeam', $membreTeam);

                //Players en attente d'acceptation d'invitation
                $this->loadModel('TeamsHasPlayers');
		$my_teamAtt = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $player['Player']['id'],
                                'accepted' => '0'
			) ,
			'recursive' => 1
		));
		$this->set('my_teamAtt', $my_teamAtt);


                //Amis facebook
                $this->loadModel('User');

                $fbidamis = $this->User->find('all', array(
			'conditions' => array(
				'id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
                $arrIdAmis = $fbidamis[0]['User']['fbidamis'];
                $idAmis = explode("-", $arrIdAmis);
                $i ="0";

                $fields = get_class_vars('DATABASE_CONFIG');
                $host = $fields["default"]["host"];
                $login = $fields["default"]["login"];
                $password = $fields["default"]["password"];
                $database = $fields["default"]["database"];

                $db = mysql_connect($host, $login, $password);
                mysql_select_db($database,$db);

                foreach ($idAmis as $valIdAmis){
                    if ($i != "0"){ $sql = 'SELECT *
                        FROM users
                        LEFT JOIN players
                        ON players.user_id=users.id
                        WHERE users.fbid = '.$valIdAmis;
                        $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
                        $amisfb[$i] = mysql_fetch_assoc($req);
                    }
                    $i++;
                }
                //echo "<pre>";print_r($amisfb);echo "</pre>";

		$this->set('amisfb', $amisfb);


		$this->loadModel('Team');
		$this->Team->virtualFields['teamId'] = 0;
		$this->Team->virtualFields['ExperienceEquipe'] = 0;

		$rows_team_exp = $this->Team->query("
			SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Team__ExperienceEquipe, thp.team_id AS Team__teamId 
			FROM teams_has_players AS thp, missions AS m,saison AS s, missiondones AS md 
			WHERE thp.`player_id` = md.player_id
            AND   md.mission_id = m.id
			AND   thp.`accepted` = 1
            AND   DATE(NOW()) BETWEEN s.date_debut AND s.date_fin
		    AND   FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND s.date_fin
			GROUP BY thp.team_id 
			ORDER BY Team__ExperienceEquipe DESC;"
			);

		foreach ($rows_team_exp as $key => $infoExp)
		{
			$ids_exp='"'.$infoExp['Team']['teamId'].'",';
			$exp_byTeamId[$infoExp['Team']['teamId']]=$infoExp['Team']['ExperienceEquipe'];

			$this->Team->id = $infoExp['Team']['teamId'];
			$this->Team->saveField('experience', $infoExp['Team']['ExperienceEquipe']);
		}
		
		error_log(print_r($exp_byTeamId,true));
		$this->set('exp_byTeamId', $exp_byTeamId);
		$ids_exp = rtrim($ids_exp,',');


		if($ids_exp)
			$sql=array(
				'order'=>array('FIELD(Team.id, '.$ids_exp.') ASC')
			);
		$rows_teams = $this->Team->find('all',$sql);
                $nbrTeams = count($rows_teams);
                for( $i= 0 ; $i < $nbrTeams ; $i++ ){
                    $this->loadModel('TeamsHasPlayers');
                    $membreTeam = $this->TeamsHasPlayers->find('all', array(
			'conditions' => array(
				'team_id' => $rows_teams[$i]["Team"]["id"],
                                'accepted' => '1'
			) ,
			'recursive' => 1
		));
                    $nrbPlayer = count($membreTeam)."<br/>";
                    $rows_teams[$i]["Team"]["nbrPlayer"] = $nrbPlayer;
                }
                
		$this->set('rows_teams', $rows_teams);


		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}
		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}

		$this->loadModel('Missiondone');
		$this->loadModel('Media');
		$this->loadModel('Type');
		$this->loadModel('Level');
		$this->loadModel('Saison');

		if (isset($this->request->query['search'])) {
			$this->set('search', $this->request->query['search']);
			$search = $this->validate_string_input($this->request->query['search']);
		}
		else {
			$search = "";
			$this->set('search', $search);
		}

		$records = array();
		if ($search == "") {
			$playerDB = $this->Player->find('all', array(
				'fields' => 'Player.profil_id,Player.id,Player.type_id,Level.name,Level.palier,Level.level,Player.pseudo,Player.experience,User.lastlogin,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini, Player.classement,Type.image4,Type.image5,Type.image6,Media.thumb',
				'order' => 'User.lastlogin DESC',
				'conditions' => 'User.id <> "1"',
				'recursive' => 1
			));
		}
		else {
			$playerDB = $this->Player->find('all', array(
				'fields' => 'User.email, Player.profil_id,Player.id,Player.type_id,Level.name,Level.palier,Level.level,Player.pseudo,Player.experience,User.lastlogin,Type.name,Type.image1,Type.image1_mini,Type.image2, Type.image2_mini,Type.image3,Type.image3_mini, Player.classement,Type.image4,Type.image5,Type.image6,Media.thumb',
				'conditions' => array(
					'OR' => array(
						array(
							"Player.pseudo LIKE" => '%' . $search . '%'
						) ,
						array(
							"User.email LIKE" => '%' . $search . '%'
						)
					)
				) ,
				'order' => 'User.lastlogin DESC',
				'recursive' => 1
			));
		}

		/**** recupérer le debut et la fin de la derniere saison****/
		$lastSaison = $this->Saison->find('first',array(
        							'order' => array(
                        			'id' => 'desc'
                    					))
        							);
        $debut_lastSaison = strtotime($lastSaison['Saison']['date_debut']);
        $fin_lastSaison = strtotime($lastSaison['Saison']['date_fin']);
        /***********************************************************/

		foreach ($playerDB as $pl) {

			/**** calculer missions reussi des joueurs de l'equipe****/
			$MD_ByPlayerTeam = $this->Missiondone->find('count', array(
												'fields' => array(
													'Missiondone.id'
												) ,
												'conditions' => array(
													'Missiondone.player_id' => $pl['Player']['id'],
													'sucess'				=> 1,
													'Missiondone.time >='	=>$debut_lastSaison,
										            'Missiondone.time <='	=>$fin_lastSaison
												)
											));
			/********************************************************/
                        $this->loadModel('TeamsHasPlayers');
                        $team = $this->TeamsHasPlayers->find('first', array('conditions' => array('player_id' => $pl['Player']['id'], 'accepted' => "1"),'recursive' => -1));
                        if($team['TeamsHasPlayers']['id'] == "") {$team['TeamsHasPlayers']['id'] = '0';}
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
			$victoires = count($pl['Missiondone']);
			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name'],
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'experience' => $pl['Player']['experience'],
				'level' => $pl['Level']['level'],
				'lastlogin' => $pl['User']['lastlogin'],
                'id_team' => $team['TeamsHasPlayers']['id'],
				'victoire_PlayerTeam'=> $MD_ByPlayerTeam
			);
			array_push($records, $record);
		}
		$playerDB = Hash::combine($records, '{n}.id', '{n}');
		$this->loadModel('Relation');
		$friendslist = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'Relation.user_from' => $player['Player']['id']
			) ,
			'recursive' => - 1
		));
		$friends = array();
		$friends_not = array();
		foreach ($friendslist as $f) {
			array_push($friends, array(
				'Relation.user_from' => $f
			));
			array_push($friends_not, array(
				'Relation.user_to' => $f
			));
		}
		$propfriends = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'AND' => array(
					'OR' => $friends,
					'NOT' => $friends_not
				)
			) ,
			'recursive' => 1
		));
		$this->set('playersDB', $playerDB);
		$this->set('friends', $friendslist);
		$this->set('propfriends', $propfriends);

		//$this->Player->recursive=2;
		//$test=$this->Player->findById(129);
		//$test=$this->Relation->find('all',array('conditions'=>array('Relation.user_from'=>$player['Player']['id']),'recursive'=>2));
		//$test=$this->Player->find('all',array('conditions' => array('Player.id'=>33), 'recursive'=>-1));
		/*foreach($records as $rec) {
		   $test=$this->Relation->find('first',array('conditions'=>array('user_from'=> $rec['id'], 'user_to'=>90),'recursive'=>0));
		   if ( !isset($test['Relation']['id']) && $rec['id']!=90) {
			  $this->Relation->create();
			  $option = array('user_from' => $rec['id'], 'user_to' => 90, 'timestamp' => time());
			  $this->Relation->save($option);
		   }
		}*/
                
                $this->loadModel('Rules');
                $rules = $this->Rules->find('first');
                $rulesTexte = $rules["Rules"]["texte"];
                $this->set('rulesTexte', $rulesTexte);
                        
	}

	public function user_handle_modteam()
	{
		$user = $this->Session->read();
		$this->loadModel('Player');

		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => - 1
		));
		$playerid = $player['Player']['id'];

		if ($this->request->is('ajax')) {
			$this->loadModel('Team');
			$this->loadModel('TeamsHasPlayers');

			App::import('Vendor', 'UploadHandler', array(
				'file' => 'UploadHandler/UploadHandler.php'
			));
			ob_start();
			$upload_handler = new UploadHandler();
			$path = "team/" . $this->request->data['teams']['id'] . "/";
			$path_thumb = "team/thumb/" . $this->request->data['teams']['id'] . "/";
			$upload_handler->config($path, $path_thumb);
			$result = $upload_handler->post($this->request->data['teams']['id']);
			unset($this->request->data['teams']['logo']);
			if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
				$this->request->data['teams']['logo']=$result['files']['0']->name;
			}
			ob_end_clean();
                        $this->request->data['teams']['classement'] = "-1";
			$this->Team->save($this->request->data['teams']);
                        
			$data = array(
				'team_id'   => $this->Team->id,
				'player_id' => $playerid,
				'captain'   => 1,
				'accepted'  => 1
				);
                        if ($this->TeamsHasPlayers->isUnique(array('TeamsHasPlayers.player_id'=>$playerid, 'TeamsHasPlayers.accepted'=>1), false)) {
                            $this->TeamsHasPlayers->create();
                            $this->TeamsHasPlayers->save($data);
			}

                        //Invitation (création dans la table TeamsHasPlayers avec accepeted et captain à 0 + envoi de mail)
                        if($this->request->data['friend']["id"] != ""){

                            $id = explode("-", $this->request->data['friend']["id"]);
                            $nb_res = count($id);
                            $nb_res = $nb_res - "1";
                            $i = "0";
                            $data = array();
                            while($i < $nb_res){
                                    $data[$i]['team_id'] = $this->Team->id;
                                    $data[$i]['player_id'] = $id[$i];
                                    $data[$i]['captain'] = "0";
                                    $data[$i]['accepted'] = "0";
                                    $i++;
                            }
                            //Recupère si le player a déjà accepté une équipe
                            $this->loadModel('TeamsHasPlayers');
                            foreach ($data as $player){
                                $existTeam = $this->TeamsHasPlayers->find('first', array(
                                'conditions' => array(
                                        'accepted' => "1",
                                        'player_id' => $player["player_id"]
                                )
                                ));
                                //Si le player n'as pas déja accepté une autre équipe
                                if ($existTeam['TeamsHasPlayers']['player_id'] == ""){
                                    //Ajoute le player dans la table TeamsHasPlayers mais avec accepeted et captain à 0
                                    $this->TeamsHasPlayers->create();
                                    $this->TeamsHasPlayers->save($player);

                                    $this->loadModel('Player');
                                    $pseudo = $this->Player->find('first', array(
                                        'conditions' => array(
                                                'Player.id' => $player["player_id"]
                                        )
                                    ));
                                    $pseudoDestinataire = $pseudo["Player"]["pseudo"];

                                    $mailDestinataire =$pseudo["User"]["email"];

                                    //Envoi un mail aux invités
                                    App::uses('CakeEmail', 'Network/Email');
                                    $mail = new CakeEmail();
                                    $mail->from('info@intripid.fr')->to($mailDestinataire)->subject('Invitation à rejoindre mon équipe')->emailFormat('html')->template('equipe')->viewVars(array(
                                            'username' => $pseudoDestinataire,
                                            'usernameFriend' => $user['Auth']['User']['username'],
                                            'link' => "http://intripid.fr/user/users/relations"
                                        ))->send();
                                }
                            }
                        }



			if ($this->TeamsHasPlayers->isUnique(array('TeamsHasPlayers.player_id'=>$playerid, 'TeamsHasPlayers.accepted'=>1), false)) {
				$this->TeamsHasPlayers->save($data);
			}
                            $info['alert']='Enregistrement réussi';
                            $info['team_id']=$this->Team->id;
                            $this->sendJson('OK', 'user_handle_modteam', $info);

/*            $ref_id = $this->request->query['ref_id'];
			if ($this->validate_numeric($ref_id)) {
				$comment = $this->validate_string_input($this->request->query['comment']);
				$this->loadModel('Commentaire');
				$record = array(
					'Commentaire' => array(
						'ref' => 'Mission',
						'ref_id' => $ref_id,
						'player_id' => $player['Player']['id'],
						'time' => time() ,
						'comment' => $comment
					)
				);
				$this->Commentaire->create();
				$this->Commentaire->save($record);
			}*/
		}

		$this->sendJson('KO', 'user_handle_modteam', $info);
	}

	public function user_handle_modrelation($type, $id)
	{
		$user = $this->Session->read();
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => - 1
		));
		$playerid = $player['Player']['id'];

		if ($this->request->is('ajax')) {
			$this->loadModel('Relation');
			if ($this->request->query['type'] != '' && ($this->request->query['type'] == 0 || $this->request->query['type'] == 1) && $this->request->query['idu'] != '') {
				$type = $this->request->query['type'];
				$user_to = $this->request->query['idu'];
				if ($playerid == $user_to) exit();
				$test = $this->Relation->find('first', array(
					'conditions' => array(
						'user_from' => $playerid,
						'user_to' => $user_to
					) ,
					'recursive' => 0
				));
				if ($type == 0) {

					// delete user
					if (isset($test['Relation']['id'])) {

						// delete
						$this->Relation->id = $test['Relation']['id'];
						$this->Relation->delete();
						echo 1;
						exit();
					}
				}
				if ($type == 1) {

					// add user
					if (!isset($test['Relation']['id'])) {
						$this->Relation->create();
						$option = array(
							'user_from' => $playerid,
							'user_to' => $user_to,
							'timestamp' => time()
						);
						$this->Relation->save($option);
						echo 1;
						exit();
					}
				}
			}
			echo 0;
			exit();
		}
	}

	public function user_get_cadeau_coupon($id = null)
	{
		if ($this->request->is('ajax')) {
			if (isset($this->request->query['id']) && is_numeric($this->request->query['id']) == TRUE) {
				$idCadeau = $this->request->query['id'];
				$this->loadModel('Player');
				$user = $this->Session->read();
				$player = $this->Player->find('first', array(
					'conditions' => array(
						'user_id' => $user['Auth']['User']['id']
					) ,
					'recursive' => 1
				));
				$this->loadModel('Cadeaux');
				$this->loadModel('Mission');
				$cadeau = $this->Cadeaux->find('first', array(
					'conditions' => array(
						'Cadeaux.id' => $idCadeau
					) ,
					'recursive' => - 1
				));
				if (!isset($cadeau['Cadeaux']['id'])) {
					echo json_encode(array(
						'code' => - 1
					));
					exit();
				}
				if ($cadeau['Cadeaux']['type'] != 1) {
					echo json_encode(array(
						'code' => - 1
					));
					exit();
				}

				// cadeau de type palier = 1

				if ($player['Player']['id'] != 129 && $player['Player']['id'] != 31) {
					if ($cadeau['Cadeaux']['niveau'] > $player['Level']['level']) {
						echo json_encode(array(
							'code' => - 1,
							'message' => 'Vous devez atteindre le palier ' . $cadeau['Cadeaux']['niveau']
						));
						exit();
					}
				}

				$this->loadModel('Coupon');
				$coupon = $this->Coupon->find('first', array(
					'conditions' => array(
						'Coupon.player_id' => $player['Player']['id'],
						'Coupon.cadeau_id' => $cadeau['Cadeaux']['id']
					) ,
					'recursive' => - 1
				));
				if (isset($coupon['Coupon']['id'])) {
					$current_time = time();
					if ($current_time <= ($coupon['Coupon']['time'] + ($cadeau['Cadeaux']['delai'] * 86400))) {
						echo json_encode(array(
							'code' => - 1,
							'message' => 'Vous avez encore un coupon cadeau en cours de validité'
						));
						exit();
					}
					if ($coupon['Coupon']['utilisation'] >= $cadeau['Cadeaux']['utilisable']) {
						echo json_encode(array(
							'code' => - 1,
							'message' => 'Vous avez déjà profité de ce cadeau'
						));
						exit();
					}
				}

				$this->loadModel('Partner');
				$partenaire = $this->Partner->find('first', array(
					'conditions' => array(
						'Partner.id' => $cadeau['Cadeaux']['id_partenaire']
					) ,
					'recursive' => - 1
				));
				if (!isset($partenaire['Partner']['id'])) {
					echo json_encode(array(
						'code' => - 1
					));
					exit();
				}
				$palier = $cadeau['Cadeaux']['niveau'];
				$delai = $cadeau['Cadeaux']['delai'];
				$cadeau_name = $cadeau['Cadeaux']['short_description'];
				if (isset($cadeau['Cadeaux']['id_mission'])) {
					$mission = $this->Mission->find('first', array(
						'conditions' => array(
							'Mission.id' => $cadeau['Cadeaux']['id_mission']
						) ,
						'recursive' => 1
					));
					$mission_nature = $mission['Missionnature']['slug'];
				}
				else $mission_nature = null;
				$time = time();
				$valide = $time + ($cadeau['Cadeaux']['delai'] * 86400);
				$valide = date('d/m/Y', $valide);

				if ($player['Player']['id'] != 129 && $player['Player']['id'] != 33 && $player['Player']['id'] != 31) {
					if (isset($coupon['Coupon']['id'])) {
						$this->Coupon->id = $coupon['Coupon']['id'];
						$utilisation = $coupon['Coupon']['utilisation'] + 1;
						$this->Coupon->saveField('coupon', $coupon_pdf);
						$this->Coupon->saveField('utilisation', $utilisation);
						$this->Coupon->saveField('time', $time);
					}
					else {
						$utilisation = 1;
						$this->Coupon->create();
						$option = array(
							'player_id' => $player['Player']['id'],
							'cadeau_id' => $cadeau['Cadeaux']['id'],
							'coupon' => $coupon_pdf,
							'utilisation' => $utilisation,
							'time' => $time
						);
						$this->Coupon->save($option);
					}
				}
				$coupon_pdf = $this->coupon_pdf($player, $cadeau, $partenaire, $mission_nature, $valide);
				if ($player['Player']['id'] == 129 || $player['Player']['id'] == 33 || $player['Player']['id'] == 31) {
					$destinataire = $player['User']['email'];
				}
				else $destinataire = $partenaire['Partner']['email'];
				if ($player['Player']['id'] != 129) {
					App::uses('CakeEmail', 'Network/Email');
					$mail = new CakeEmail();
					$mail->from('contact@intripid.fr')->to($destinataire)->subject('Intripid coupon cadeau')->emailFormat('html')->template('info_coupon_partenaire')->viewVars(array(
						'username' => $player['User']['username'],
						'palier' => $palier,
						'delai' => $delai,
						'cadeauname' => $cadeau_name
					))->send();
				}
				echo json_encode(array(
					'code' => 1,
					'valide' => $valide,
					'coupon' => $coupon_pdf
				));
				exit();
			}
			else {
				echo json_encode(array(
					'code' => - 1
				));
				exit();
			}
		}
		else {
			echo json_encode(array(
				'code' => - 1
			));
			exit();
		}
	}

	public function user_cadeauxbeta()
	{
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		$this->set('player', $player);
		$this->SysNotification->computeCount($player);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}

		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}
		$leader = $this->Player->find('first', array(
			'conditions' => array(
				'Player.classement' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('leader', $leader);
		$this->loadModel('Cadeaux');
		$cadeauxDB = $this->Cadeaux->find('all', array(
			'recursive' => - 1
		));
		$this->set('cadeauxs', $cadeauxDB);
		$this->loadModel('Partner');
		$partners_name = $this->Partner->find('list', array(
			'fields' => array(
				'Partner.id',
				'Partner.partnername'
			) ,
			'recursive' => - 1
		));
		$this->set('partners_name', $partners_name);
		$partners_address = $this->Partner->find('list', array(
			'fields' => array(
				'Partner.id',
				'Partner.adress'
			) ,
			'recursive' => - 1
		));
		$this->set('partners_address', $partners_address);
		$imgDB = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.thumb'
			) ,
			'conditions' => array(
				'ref' => 'Cadeaux'
			) ,
			'recursive' => - 1
		));
		$this->set('imgdb', $imgDB);
		$missions_name = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.title'
			) ,
			'recursive' => - 1
		));
		$this->set('missions_name', $missions_name);
		$missionatureDB = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Missionnature.slug'
			) ,
			'recursive' => 1
		));
		$this->set('missionature', $missionatureDB);
		if ($player['Player']['id'] == 129) {
			$this->set('player_palier', 7);
		}
		else $this->set('player_palier', $player['Level']['palier']);
	}

	public function user_cadeaux()
	{
		$this->set("title_for_layout", "Cadeaux");
		$this->loadModel('User');
		$this->set("show_concept", 0);
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => 1
		));
		$this->set('player', $player);
		$this->SysNotification->computeCount($player);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}

		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		$this->loadModel('Mission');
		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}
		$leader = $this->Player->find('first', array(
			'conditions' => array(
				'Player.classement' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('leader', $leader);
		$this->loadModel('Cadeaux');
		$cadeauxDB = $this->Cadeaux->find('all', array(
			'recursive' => - 1
		));
		$this->set('cadeauxs', $cadeauxDB);
		$this->loadModel('Partner');
		$partners_name = $this->Partner->find('list', array(
			'fields' => array(
				'Partner.id',
				'Partner.partnername'
			) ,
			'recursive' => - 1
		));
		$this->set('partners_name', $partners_name);
		$partners_address = $this->Partner->find('list', array(
			'fields' => array(
				'Partner.id',
				'Partner.adress'
			) ,
			'recursive' => - 1
		));
		$this->set('partners_address', $partners_address);
		$imgDB = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.thumb'
			) ,
			'conditions' => array(
				'ref' => 'Cadeaux'
			) ,
			'recursive' => - 1
		));
		$this->set('imgdb', $imgDB);
		$missions_name = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.title'
			) ,
			'recursive' => - 1
		));
		$this->set('missions_name', $missions_name);
		$missionatureDB = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Missionnature.slug'
			) ,
			'recursive' => 1
		));
		$this->set('missionature', $missionatureDB);
		$this->loadModel('Missiontype');
		$missiontypeDB = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Missiontype.name'
			) ,
			'recursive' => 1
		));
		$this->set('missiontype', $missiontypeDB);
		$this->loadModel('Missionnature');
		$mnature_filtre = $this->Missionnature->find('list', array(
			'fields' => array(
				'Missionnature.id',
				'Missionnature.slug'
			) ,
			'recursive' => - 1
		));
		$this->set('mnature_filtre', $mnature_filtre);
		$this->loadModel('Missiontheme');
		$mtheme_filtre = $this->Missiontheme->find('list', array(
			'fields' => array(
				'Missiontheme.id',
				'Missiontheme.name'
			) ,
			'recursive' => - 1
		));
		$this->set('mtheme_filtre', $mtheme_filtre);

		//CakeLog::write("debug",print_r($mnature_filtre,true));
		if ($player['Player']['id'] == 31 || $player['Player']['id'] == 129) {
			$this->set('player_palier', 7);
		}
		else $this->set('player_palier', $player['Level']['level']);
	}

	public function user_profilbeta($id = null)
	{
		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}
		$this->loadModel('Player');
		$user = $this->Session->read();
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
		$this->set('player', $player);
		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		$owner = $this->Player->find('first', array(
			'conditions' => array(
				'Player.id' => $id
			) ,
			'recursive' => 1
		));
		if (!isset($owner['Player']['id'])) {
			$this->redirect(array(
				'controller' => 'missions',
				'action' => 'carte',
				'user' => true
			));
		}

		$this->set('owner', $owner);

		$this->loadModel('Relation');
		$isfriend = $this->Relation->find('first', array(
			'conditions' => array(
				'user_from' => $player['Player']['id'],
				'user_to' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		if (isset($isfriend['Relation']['id'])) $this->set('isfriend', 1);
		else $this->set('isfriend', 0);

		$session = $this->Session->read();
		$this->loadModel('Missiondone');
		$this->loadModel('Mission');

		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $owner['Player']['profil_id']
			) ,
			'recursive' => 0
		));
		if (isset($profil['Media'])) {
			$this->set('owner_profil_picture', $profil['Media']['file']);
			$this->set('owner_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $owner['Level']['palier'] . '_mini';
			$this->set('owner_profil_picture', $owner['Type'][$avatar_mini]);
			$this->set('owner_profil_thumb_picture', $owner['Type'][$avatar_mini]);
		}
		$avatar = 'image' . $owner['Level']['palier'];
		$this->set('avatar', $owner['Type'][$avatar]);

		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}

		$profil_pictures = $this->Media->find('all', array(
			'conditions' => array(
				'Media.userid' => $owner['Player']['id'],
				'Media.ref' => 'User',
				'Media.visible' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('profil_pictures', $profil_pictures);

		$idplayer = $owner['Player']['id'];
		$missiondones = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer
			)
		));
		$missiondonesucess = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer,
				'Missiondone.sucess' => 1
			)
		));

		/*$players = $this->Player->find('first',array(
									 'conditions'=> array('Player.user_id'=>$id)
									 ));*/
		$missiondonelists = $this->Missiondone->find('all', array(
			'conditions' => array(
				'Missiondone.player_id' => $idplayer
			)
		));
                
		$this->set(compact('missiondones', 'missiondonesucess', 'missiondonelists'));

		$dbownergallery = $this->Media->find('all', array(
			'conditions' => array(
				'ref' => 'Missiondone',
				'userid' => $owner['Player']['id']
			) ,
			'order' => 'like DESC',
			'recursive' => - 1
		));

		//$subsetgallery=array_slice($dbownergallery,0,6);
		$this->set('dbownergallery', $dbownergallery);
		$this->loadModel('Relation');
		$ownerherosDB = $this->Relation->find('all', array(
			'conditions' => array(
				'Relation.user_from' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$ownerfansDB = $this->Relation->find('all', array(
			'conditions' => array(
				'Relation.user_to' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$ownerheros = count($ownerherosDB);
		$ownerfans = count($ownerfansDB);
		$this->set('followersdb', $ownerfansDB);
		$this->set('followingdb', $ownerherosDB);
		$this->set('followers', $ownerfans);
		$this->set('following', $ownerheros);

		// Notifications
		$records = array();
		$lastview = $owner['Player']['notif_time'];
		$time = time();
		$this->loadModel('Relation');
		$this->loadModel('Level');

		$mediaDB = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.file'
			) ,
			'recursive' => - 1
		));
		$this->set('mediadb', $mediaDB);
		$mDB = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.media_id'
			) ,
			'recursive' => - 1
		));
		$missionTitle = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.title'
			) ,
			'recursive' => - 1
		));
		$this->set('missionTitle', $missionTitle);
		$this->set('mdb', $mDB);
		$precords = array();
		$playerDB = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id, Player.experience,Level.name,Level.palier,Level.level,Player.pseudo,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini,Type.image4,Type.image5,Type.image6,Media.thumb',
			'recursive' => 1
		));
		foreach ($playerDB as $pl) {
			$victoires = count($pl['Missiondone']);
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name'],
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'level' => $pl['Level']['level'],
				'experience' => $pl['Player']['experience']
			);
			array_push($precords, $record);
		}
		$playerDB = Hash::combine($precords, '{n}.id', '{n}');
		$this->set('playerdb', $playerDB);
		$this->loadModel('Relation');
		$this->loadModel('Like');
		$this->loadModel('Commentaire');
		$friendslist = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.timestamp'
			) ,
			'conditions' => array(
				'Relation.user_from' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$mediaComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Media',
				'Commentaire.player_id' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$missionComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Mission',
				'Commentaire.player_id' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));

		//$mediaTrip=$this->Like->find('list',array('fields'=>array('Like.ref_id','Like.timestamp'),'conditions'=>array('ref'=>'Missiondone', 'player_id'=>$owner['Player']['id']),'recursive'=>-1));
		$mymedia = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.ref_id'
			) ,
			'conditions' => array(
				'Media.ref' => 'Missiondone',
				'userid' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$this->set('mymedia', $mymedia);
		$levelvalue = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.level'
			) ,
			'recursive' => - 1
		));
		$levelgrade = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.name'
			) ,
			'recursive' => - 1
		));
		$levelpalier = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.palier'
			) ,
			'recursive' => - 1
		));

		// $friends=array();
		$friendsmedia = array();
		$friendsmdone = array();
		$friendscomm = array();
		$friendstrip = array();
		$friendslevel = array();

		array_push($friendsmdone, array(
			'player_id' => $owner['Player']['id']
		));
		foreach ($friendslist as $f_id => $f) {
			array_push($friendstrip, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'timestamp >=' => $f
					)
				)
			));
			array_push($friendsmedia, array(
				'AND' => array(
					array(
						'Media.userid' => $f_id
					) ,
					array(
						'time >=' => $f
					)
				)
			));
			array_push($friendsmdone, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'time >=' => $f
					)
				)
			));
			array_push($friendscomm, array(
				'AND' => array(
					array(
						'Commentaire.player_id' => $f_id
					) ,
					array(
						'Commentaire.time >=' => $f
					)
				)
			));
			array_push($friendslevel, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'timestamp >=' => $f
					)
				)
			));
		}

		/* Notifications for missiondone */
		$this->loadModel('Missiondone');
		$mdoneDB = $this->Missiondone->find('all', array(
			'fields' => 'Missiondone.time,Missiondone.sucess,Mission.id,Mission.title,Mission.media_id,Player.id',
			'conditions' => array(
				'Mission.active' => 1,
				'OR' => $friendsmdone
			) ,
			'recursive' => 0
		));
		foreach ($mdoneDB as $mdone) {
			if (isset($mediaDB[$mdone['Mission']['media_id']])) {
				$record = array(
					'timestamp' => $mdone['Missiondone']['time'],
					'type' => 'missiondone',
					'success' => $mdone['Missiondone']['sucess'],
					'title' => $mdone['Mission']['title'],
					'playerid' => $mdone['Player']['id'],
					'img_id' => $mdone['Mission']['media_id'],
					'missionid' => $mdone['Mission']['id'],
					'vue' => 1
				);
				array_push($records, $record);
			}
		}

		/*********************************/
		$comDB = $this->Commentaire->find('all', array(
			'conditions' => array(
				'OR' => $friendscomm
			) ,
			'recursive' => - 1
		));
		foreach ($comDB as $com) {
			if (($com['Commentaire']['ref'] == 'Mission' && isset($mDB[$com['Commentaire']['ref_id']]) && isset($missionComm[$com['Commentaire']['ref_id']])) || ($com['Commentaire']['ref'] == 'Media' && isset($mediaDB[$com['Commentaire']['ref_id']]) && (isset($mymedia[$com['Commentaire']['ref_id']]) || isset($mediaComm[$com['Commentaire']['ref_id']])))) {
				$record = array(
					'timestamp' => $com['Commentaire']['time'],
					'vue' => 1,
					'type' => 'mediacom',
					'comment' => substr($com['Commentaire']['comment'], 0, 30) ,
					'playerid' => $com['Commentaire']['player_id'],
					'media_type' => $com['Commentaire']['ref'],
					'media_id' => $com['Commentaire']['ref_id']
				);
				array_push($records, $record);
			}
		}

		$tripDB = $this->Like->find('all', array(
			'conditions' => array(
				'OR' => $friendstrip
			) ,
			'recursive' => - 1
		));
		foreach ($tripDB as $trip) {
			if (($trip['Like']['ref'] == 'Missiondone' && isset($trip['Like']['timestamp']) && isset($mediaDB[$trip['Like']['ref_id']])) || ($trip['Like']['ref'] == 'Mission' && isset($trip['Like']['timestamp']) && isset($mDB[$trip['Like']['ref_id']]) && isset($mediaDB[$mDB[$trip['Like']['ref_id']]]))) {
				if ($trip['Like']['ref'] == 'Missiondone') $record = array(
					'timestamp' => $trip['Like']['timestamp'],
					'vue' => 1,
					'type' => 'tripmedia',
					'tripvalue' => $trip['Like']['like'],
					'playerid' => $trip['Like']['player_id'],
					'media_id' => $trip['Like']['ref_id']
				);
				if ($trip['Like']['ref'] == 'Mission' && isset($missionTitle[$trip['Like']['ref_id']])) $record = array(
					'timestamp' => $trip['Like']['timestamp'],
					'vue' => 1,
					'type' => 'tripmission',
					'tripvalue' => $trip['Like']['like'],
					'playerid' => $trip['Like']['player_id'],
					'img_id' => $mDB[$trip['Like']['ref_id']],
					'missionid' => $trip['Like']['ref_id'],
					'missiontitle' => $missionTitle[$trip['Like']['ref_id']]
				);
				array_push($records, $record);
			}
		}

		$uploadPhotos = $this->Media->find('all', array(
			'conditions' => array(
				'Media.ref' => 'Missiondone',
				'OR' => $friendsmedia
			) ,
			'recursive' => 0
		));
		foreach ($uploadPhotos as $p) {
			$record = array(
				'timestamp' => $p['Media']['time'],
				'vue' => 1,
				'type' => 'upload_photo',
				'playerid' => $p['Media']['userid'],
				'media_id' => $p['Media']['id'],
				'missionid' => $p['Media']['ref_id'],
				'missiontitle' => $missionTitle[$p['Media']['ref_id']]
			);
			array_push($records, $record);
		}

		$this->loadModel('Notification');
		$levelups = $this->Notification->find('all', array(
			'conditions' => array(
				'OR' => $friendslevel
			) ,
			'recursive' => 0
		));
		foreach ($levelups as $l) {
			$record = array(
				'timestamp' => $l['Notification']['timestamp'],
				'vue' => 1,
				'type' => $l['Notification']['classe'],
				'playerid' => $l['Notification']['player_id'],
				'grade' => $levelgrade[$l['Notification']['classe_id']],
				'level' => $levelvalue[$l['Notification']['classe_id']],
				'palier' => $levelpalier[$l['Notification']['classe_id']]
			);
			array_push($records, $record);
		}

		foreach ($ownerfansDB as $fan) {
			if (isset($playerDB[$fan['Relation']['user_from']])) {
				$record = array(
					'timestamp' => $fan['Relation']['timestamp'],
					'vue' => 1,
					'type' => 'fan',
					'playerid' => $fan['Relation']['user_from']
				);
				array_push($records, $record);
			}
		}

		// mettre les notifs en ordre temporel
		$records = $this->SysNotification->make_notifications_sorting($records);

		// définir ceux qui n'ont pas encore été vue par le owner
		$records = $this->SysNotification->setnotview($records, $lastview);

		// Récupérer le nombre de notifications non-vue pour afficher dans la barre du haut si le owner est le visiteur
		// mettre a jour la date de dernière vue
		if ($owner['Player']['id'] == $player['Player']['id']) {
			$nbnotview = $this->SysNotification->countnotview($records);
			$this->set('notifications', $nbnotview);
			$this->SysNotification->updatetime($owner['Player']['id'], $time);
		}
		$this->set('notifs', $records);
		if ($owner['Player']['id'] != $player['Player']['id']) $this->SysNotification->computeCount($player);

		$this->loadModel('Profilcomm');
		$this->loadModel('Deviselike');
		$profil_comments = NULL;
		$profil_comments = $this->Profilcomm->find("all", array(
			'conditions' => array(
				"Profilcomm.playerto_id" => $owner['Player']['id']
			) ,
			'order' => 'time DESC',
			'recursive' => - 1
		));
		$this->set('profil_comments', $profil_comments);
		$devise_trip = $this->Deviselike->find('count', array(
			'conditions' => array(
				'Deviselike.devise_id' => $owner['Player']['devise_id'],
				'Deviselike.trip' => 1
			) ,
			'recursive' => - 1
		));
		$devise_badtrip = $this->Deviselike->find('count', array(
			'conditions' => array(
				'Deviselike.devise_id' => $owner['Player']['devise_id'],
				'Deviselike.badtrip' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('devise_trip', $devise_trip);
		$this->set('devise_badtrip', $devise_badtrip);
	}

	public function user_profil($id = null)
	{
		$this->set("show_concept", 0);
		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}
		$this->loadModel('Player');
		$user = $this->Session->read();
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));
		$this->set('player', $player);
		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		if ($id == NULL) {
			if (isset($this->request->params['pseudo'])) {
				$pseudo = $this->validate_string_input(str_replace("-", "%", $this->request->params['pseudo']));
				$hero = $this->Player->find('first', array(
					'conditions' => array(
						'pseudo LIKE' => $pseudo
					) ,
					'recursive' => - 1
				));
				if (isset($hero['Player']['id']))
					$id = $hero['Player']['id'];
			}
		}

		$owner = $this->Player->find('first', array(
			'conditions' => array(
				'Player.id' => $id
			) ,
			'recursive' => 1
		));
                /*
                echo "<pre>";
                print_r($owner);
                echo "</pre>";
                */

                // Change les valeur en fonction de la langue
                $lang = Configure::read('Config.language');
                if ($lang == "eng"){
                    $fields = get_class_vars('DATABASE_CONFIG');
                    $host = $fields["default"]["host"];
                    $login = $fields["default"]["login"];
                    $password = $fields["default"]["password"];
                    $database = $fields["default"]["database"];

                    $db = mysql_connect($host, $login, $password);
                    mysql_select_db($database,$db);
                    $sql = 'SELECT i18n.content
                    FROM i18n
                    LEFT JOIN levels
                    ON i18n.foreign_key=levels.id
                    WHERE levels.id = "'.$owner['Level']['id'].'"
                    AND i18n.model = "Level"
                    AND i18n.locale = "'.$lang.'"';
                    $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
                    $data = mysql_fetch_assoc($req);

                    $owner['Level']['name'] = $data["content"];
                }

		if (!isset($owner['Player']['id'])) {
			$this->redirect(array(
				'controller' => 'missions',
				'action' => 'carte',
				'user' => true
			));
		}
		$this->set("title_for_layout", "Profile de " . $owner["Player"]["pseudo"]);
		$this->set('owner', $owner);
		$this->loadModel('Relation');
		$isfriend = $this->Relation->find('first', array(
			'conditions' => array(
				'user_from' => $player['Player']['id'],
				'user_to' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		if (isset($isfriend['Relation']['id'])) $this->set('isfriend', 1);
		else $this->set('isfriend', 0);

		$session = $this->Session->read();
		$this->loadModel('Missiondone');
		$this->loadModel('Mission');

		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));


		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini_profil = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini_profil]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini_profil]);
		}
		$profil_owner = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $owner['Player']['profil_id']
			) ,
			'recursive' => 0
		));


		if (isset($profil_owner['Media'])) {
			$this->set('owner_profil_picture', $profil_owner['Media']['file']);
			$this->set('owner_profil_thumb_picture', $profil_owner['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $owner['Level']['palier'] . '_mini';
			$this->set('owner_profil_picture', $owner['Type'][$avatar_mini]);
			$this->set('owner_profil_thumb_picture', $owner['Type'][$avatar_mini]);
		}

		$avatar = 'image' . $owner['Level']['palier'];
		$this->set('avatar', $owner['Type'][$avatar]);

		$history_avatar=array();
        $i=0;
        $record=array();
        for ($i=1; $i <= $owner['Level']['palier']; $i++ ) {
           $record=array('key'=>$i, 'avatar'=>$owner['Type']['image'.$i] );
           array_push($history_avatar,$record);
        }
        $this->set('history_avatar',$history_avatar);

		$profil_pictures = $this->Media->find('all', array(
			'conditions' => array(
				'Media.userid' => $owner['Player']['id'],
				'Media.ref' => 'User',
				'Media.visible' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('profil_pictures', $profil_pictures);

		$idplayer = $owner['Player']['id'];
		$missiondones = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer
			)
		));
		$missiondonesucess = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer,
				'Missiondone.sucess' => 1
			)
		));

		/*$players = $this->Player->find('first',array(
			 'conditions'=> array('Player.user_id'=>$id)
			 ));*/
		$missiondonelists = $this->Missiondone->find('all', array(
			'conditions' => array(
				'Missiondone.player_id' => $idplayer
			) ,
			'order' => 'time DESC'
		));
                $lang = Configure::read('Config.language');
                
                $this->loadModel('Trad');
		$tradMissDonelists = $this->Trad->find('all', array(
			'conditions' => array(
				'model' => "Mission",
                                'field' => "title",
                                'locale' => $lang
			) ,
			'recursive' => - 1
		));
                $i = "0";
                foreach ($missiondonelists as $valmissiondonelists){
                    foreach ($tradMissDonelists as $valtradMissDonelists){
                        if($valmissiondonelists["Mission"]["id"] == $valtradMissDonelists["Trad"]["foreign_key"]){
                            $missiondonelists[$i]["Mission"]["title"] = $valtradMissDonelists["Trad"]["content"];
                        }
                    }
                    $i++;
                }
                
                //echo "<pre>";print_r($missiondonelists);exit;
		$this->set(compact('missiondones', 'missiondonesucess', 'missiondonelists'));

		$dbownergallery = $this->Media->find('all', array(
			'conditions' => array(
				'ref' => 'Missiondone',
				'userid' => $owner['Player']['id']
			) ,
			'order' => 'like DESC',
			'recursive' => - 1
		));

		//$subsetgallery=array_slice($dbownergallery,0,6);
		$this->set('dbownergallery', $dbownergallery);
		CakeLog::write("debug", print_r($dbownergallery, true));
		$this->loadModel('Relation');
		$ownerherosDB = $this->Relation->find('all', array(
			'conditions' => array(
				'Relation.user_from' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$ownerfansDB = $this->Relation->find('all', array(
			'conditions' => array(
				'Relation.user_to' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$ownerheros = count($ownerherosDB);
		$ownerfans = count($ownerfansDB);
		$this->set('followersdb', $ownerfansDB);
		$this->set('followingdb', $ownerherosDB);
		$this->set('followers', $ownerfans);
		$this->set('following', $ownerheros);

		// Notifications
		$records = array();
		$lastview = $owner['Player']['notif_time'];
		$time = time();
		$this->loadModel('Relation');
		$this->loadModel('Level');
		$this->loadModel('Deviselike');
		$this->loadModel('Profilcomm');

		$mediaDB = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.file'
			) ,
			'recursive' => - 1
		));
		$this->set('mediadb', $mediaDB);
		$mDB = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.media_id'
			) ,
			'recursive' => - 1
		));
		$missionTitle = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.title'
			) ,
			'recursive' => - 1
		));
		$missionSDesc = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.short_description'
			) ,
			'recursive' => - 1
		));
		$missionNat = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Missionnature.slug'
			) ,
			'recursive' => 1
		));
		$this->set('missionTitle', $missionTitle);
		$this->set('missionsSDesc', $missionSDesc);
		$this->set('missionNat', $missionNat);
		$this->set('mdb', $mDB);
		$precords = array();
		$playerDB = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id, Player.experience,Level.name,Level.id,Level.palier,Level.level,Player.pseudo,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini,Type.image4,Type.image5,Type.image6,Media.thumb',
			'recursive' => 1
		));

		foreach ($playerDB as $pl) {
			$victoires = count($pl['Missiondone']);
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
                        /*

                        $db = mysql_connect('localhost', 'root', '');
                        mysql_select_db('intripid',$db);
                        $sql = 'SELECT i18n.content
                        FROM i18n
                        LEFT JOIN levels
                        ON i18n.foreign_key=levels.id
                        WHERE levels.id = "'.$pl['Player']['pseudo'].'";';
                        echo $sql;
                        $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
                        while($data = mysql_fetch_assoc($req))
                        {
                        // on affiche les informations de l'enregistrement en cours
                        //echo '<b>'.$data['content'];
                        }
                        */

			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name']."a",
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'level' => $pl['Level']['level'],
				'experience' => $pl['Player']['experience']
			);
			array_push($precords, $record);
		}
		$playerDB = Hash::combine($precords, '{n}.id', '{n}');
		$this->set('playerdb', $playerDB);

		$this->loadModel('Relation');
		$this->loadModel('Like');
		$this->loadModel('Commentaire');
		$friendslist = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.timestamp'
			) ,
			'conditions' => array(
				'Relation.user_from' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$mediaComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Media',
				'Commentaire.player_id' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$missionComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Mission',
				'Commentaire.player_id' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));

		//$mediaTrip=$this->Like->find('list',array('fields'=>array('Like.ref_id','Like.timestamp'),'conditions'=>array('ref'=>'Missiondone', 'player_id'=>$owner['Player']['id']),'recursive'=>-1));
		$mymedia = $this->Media->find('list', array(
			'fields' => array(
				'Media.id',
				'Media.ref_id'
			) ,
			'conditions' => array(
				'Media.ref' => 'Missiondone',
				'userid' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		$this->set('mymedia', $mymedia);
		$levelvalue = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.level'
			) ,
			'recursive' => - 1
		));
		$levelgrade = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.name'
			) ,
			'recursive' => - 1
		));
		$levelpalier = $this->Level->find('list', array(
			'fields' => array(
				'Level.id',
				'Level.palier'
			) ,
			'recursive' => - 1
		));

		// $friends=array();
		$friendsmedia = array();
		$friendsmdone = array();
		$friendscomm = array();
		$friendstrip = array();
		$friendslevel = array();

		array_push($friendsmdone, array(
			'player_id' => $owner['Player']['id']
		));
		foreach ($friendslist as $f_id => $f) {
			array_push($friendstrip, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'timestamp >=' => $f
					)
				)
			));
			array_push($friendsmedia, array(
				'AND' => array(
					array(
						'Media.userid' => $f_id
					) ,
					array(
						'time >=' => $f
					)
				)
			));
			array_push($friendsmdone, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'time >=' => $f
					)
				)
			));
			array_push($friendscomm, array(
				'AND' => array(
					array(
						'Commentaire.player_id' => $f_id
					) ,
					array(
						'Commentaire.time >=' => $f
					)
				)
			));
			array_push($friendslevel, array(
				'AND' => array(
					array(
						'player_id' => $f_id
					) ,
					array(
						'timestamp >=' => $f
					)
				)
			));
		}

		/* Notifications for missiondone */
		$this->loadModel('Missiondone');
		$mdoneDB = $this->Missiondone->find('all', array(
			'fields' => 'Missiondone.time,Missiondone.sucess,Mission.id,Mission.title,Mission.media_id,Player.id',
			'conditions' => array(
				'Mission.active' => 1,
				'OR' => $friendsmdone
			) ,
			'recursive' => 0
		));
                
                $lang = Configure::read('Config.language');
                $this->loadModel('Trad');
                
		foreach ($mdoneDB as $mdone) {
			if (isset($mediaDB[$mdone['Mission']['media_id']])) {
                            
                            
                            $mdoneDBtrad = $this->Trad->find('first', array(
                                    'conditions' => array(
                                            "foreign_key" => $mdone['Mission']['id'],
                                            "field" => "title",
                                            "model" => "Mission",
                                            "locale" => $lang
                                    ) ,
                                    'recursive' => 0
                            ));
                            //echo "<pre>";print_r($mdoneDBtrad);

				$record = array(
					'timestamp' => $mdone['Missiondone']['time'],
					'type' => 'missiondone',
					'success' => $mdone['Missiondone']['sucess'],
					'title' => $mdoneDBtrad['Trad']['content'],
					'playerid' => $mdone['Player']['id'],
					'img_id' => $mdone['Mission']['media_id'],
					'missionid' => $mdone['Mission']['id'],
					'vue' => 1
				);
				array_push($records, $record);
			}
		}

		/*********************************/

		$comDB = $this->Commentaire->find('all', array(
			'conditions' => array(
				'OR' => $friendscomm
			) ,
			'recursive' => - 1
		));
		foreach ($comDB as $com) {
			if (($com['Commentaire']['ref'] == 'Mission' && isset($mDB[$com['Commentaire']['ref_id']]) && isset($missionComm[$com['Commentaire']['ref_id']]))
				|| ($com['Commentaire']['ref'] == 'Media' && isset($mediaDB[$com['Commentaire']['ref_id']]) && (isset($mymedia[$com['Commentaire']['ref_id']])
					|| isset($mediaComm[$com['Commentaire']['ref_id']])))) {
				$record = array(
					'timestamp' => $com['Commentaire']['time'],
					'vue' => 1,
					'type' => 'mediacom',
					'comment' => substr($com['Commentaire']['comment'], 0, 30) ,
					'playerid' => $com['Commentaire']['player_id'],
					'media_type' => $com['Commentaire']['ref'],
					'media_id' => $com['Commentaire']['ref_id']
				);
				array_push($records, $record);
			}
		}

		if (isset($owner['Player']['devise_id'])) {
			$deviselikeDB = $this->Deviselike->find('all', array(
				'conditions' => array(
					'devise_id' => $owner['Player']['devise_id']
				) ,
				'recursive' => - 1
			));
			foreach ($deviselikeDB as $dlike) {
				$record = array(
					'timestamp' => $dlike['Deviselike']['time'],
					'value' => $dlike['Deviselike']['trip'],
					'vue' => 1,
					'playerid' => $dlike['Deviselike']['player_id'],
					'type' => 'deviselike'
				);
				array_push($records, $record);
			}
		}

		$profilcommDB = $this->Profilcomm->find('all', array(
			'conditions' => array(
				'playerto_id' => $owner['Player']['id']
			) ,
			'recursive' => - 1
		));
		foreach ($profilcommDB as $pcomm) {
			$record = array(
				'timestamp' => $pcomm['Profilcomm']['time'],
				'comment' => $pcomm['Profilcomm']['commentaire'],
				'vue' => 1,
				'playerid' => $pcomm['Profilcomm']['playerfrom_id'],
				'type' => 'profilcomm'
			);
			array_push($records, $record);
		}

		$tripDB = $this->Like->find('all', array(
			'conditions' => array(
				'OR' => $friendstrip
			) ,
			'recursive' => - 1
		));
		foreach ($tripDB as $trip) {
			if (($trip['Like']['ref'] == 'Missiondone' && isset($trip['Like']['timestamp']) && isset($mediaDB[$trip['Like']['ref_id']])) || ($trip['Like']['ref'] == 'Mission' && isset($trip['Like']['timestamp']) && isset($mDB[$trip['Like']['ref_id']]) && isset($mediaDB[$mDB[$trip['Like']['ref_id']]]))) {
				if ($trip['Like']['ref'] == 'Missiondone') $record = array(
					'timestamp' => $trip['Like']['timestamp'],
					'vue' => 1,
					'type' => 'tripmedia',
					'tripvalue' => $trip['Like']['like'],
					'playerid' => $trip['Like']['player_id'],
					'media_id' => $trip['Like']['ref_id']
				);
				if ($trip['Like']['ref'] == 'Mission' && isset($missionTitle[$trip['Like']['ref_id']])) $record = array(
					'timestamp' => $trip['Like']['timestamp'],
					'vue' => 1,
					'type' => 'tripmission',
					'tripvalue' => $trip['Like']['like'],
					'playerid' => $trip['Like']['player_id'],
					'img_id' => $mDB[$trip['Like']['ref_id']],
					'missionid' => $trip['Like']['ref_id'],
					'missiontitle' => $missionTitle[$trip['Like']['ref_id']]
				);
				array_push($records, $record);
			}
		}

		$uploadPhotos = $this->Media->find('all', array(
			'conditions' => array(
				'Media.ref' => 'Missiondone',
				'OR' => $friendsmedia
			) ,
			'recursive' => 0
		));
		foreach ($uploadPhotos as $p) {
			$record = array(
				'timestamp' => $p['Media']['time'],
				'vue' => 1,
				'type' => 'upload_photo',
				'playerid' => $p['Media']['userid'],
				'media_id' => $p['Media']['id'],
				'missionid' => $p['Media']['ref_id'],
				'missiontitle' => $missionTitle[$p['Media']['ref_id']]
			);
			array_push($records, $record);
		}

		$this->loadModel('Notification');
		$levelups = $this->Notification->find('all', array(
			'conditions' => array(
				'OR' => $friendslevel
			) ,
			'recursive' => 0
		));
		foreach ($levelups as $l) {
			$record = array(
				'timestamp' => $l['Notification']['timestamp'],
				'vue' => 1,
				'type' => $l['Notification']['classe'],
				'playerid' => $l['Notification']['player_id'],
				'grade' => $levelgrade[$l['Notification']['classe_id']],
				'level' => $levelvalue[$l['Notification']['classe_id']],
				'palier' => $levelpalier[$l['Notification']['classe_id']]
			);
			array_push($records, $record);
		}

		foreach ($ownerfansDB as $fan) {
			if (isset($playerDB[$fan['Relation']['user_from']])) {
				$record = array(
					'timestamp' => $fan['Relation']['timestamp'],
					'vue' => 1,
					'type' => 'fan',
					'playerid' => $fan['Relation']['user_from']
				);
				array_push($records, $record);
			}
		}

		// mettre les notifs en ordre temporel
		$records = $this->SysNotification->make_notifications_sorting($records);

		// définir ceux qui n'ont pas encore été vue par le owner
		$records = $this->SysNotification->setnotview($records, $lastview);

		// Récupérer le nombre de notifications non-vue pour afficher dans la barre du haut si le owner est le visiteur
		// mettre a jour la date de dernière vue
		if ($owner['Player']['id'] == $player['Player']['id']) {
			$nbnotview = $this->SysNotification->countnotview($records);
			$this->set('notifications', $nbnotview);
			$this->SysNotification->updatetime($owner['Player']['id'], $time);
		}
                //echo "<pre>";print_r($records);exit;
		$this->set('notifs', $records);
		if ($owner['Player']['id'] != $player['Player']['id']) $this->SysNotification->computeCount($player);
		$this->loadModel('Profilcomm');
		$this->loadModel('Deviselike');
		$profil_comments = NULL;
		$profil_comments = $this->Profilcomm->find("all", array(
			'conditions' => array(
				"Profilcomm.playerto_id" => $owner['Player']['id']
			) ,
			'order' => 'time DESC',
			'recursive' => - 1
		));
		$this->set('profil_comments', $profil_comments);
		$devise_trip = $this->Deviselike->find('count', array(
			'conditions' => array(
				'Deviselike.devise_id' => $owner['Player']['devise_id'],
				'Deviselike.trip' => 1
			) ,
			'recursive' => - 1
		));
		$devise_badtrip = $this->Deviselike->find('count', array(
			'conditions' => array(
				'Deviselike.devise_id' => $owner['Player']['devise_id'],
				'Deviselike.badtrip' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('devise_trip', $devise_trip);
		$this->set('devise_badtrip', $devise_badtrip);

                //Recupère les notificatiions d'invitation d'équipe
                $this->loadModel('TeamsHasPlayers');
		$notifEquipe = $this->TeamsHasPlayers->find("all", array(
			'conditions' => array(
				"player_id" => $player["Player"]["id"],
				"accepted" => "0",
			)
		));
		$this->set('notifEquipe', $notifEquipe);

		$my_team = $this->TeamsHasPlayers->find("first", array(
			'conditions' => array(
				"player_id" => $player["Player"]["id"],
				"accepted" => "1",
			)
		));
		$this->set('my_team', $my_team);
		$owner_team = $this->TeamsHasPlayers->find("first", array(
			'conditions' => array(
				"player_id" => $owner["Player"]["id"],
				"accepted" => "1",
			)
		));
		$this->set('owner_team', $owner_team);
                
                //Si le player est captain de son équipe
                if($my_team["TeamsHasPlayers"]["captain"] == "1"){
                    $this->loadModel('TeamsHasPlayers');
                    $dmd = $this->TeamsHasPlayers->find("all", array(
                            'conditions' => array(
                                    "team_id" => $my_team["Teams"]["id"],
                                    "accepted" => "2",
                            )
                    ));
                    if ($dmd["0"]["TeamsHasPlayers"]["id"] != ""){
                        $this->set('dmd', $dmd);
                    }
                    else{
                        $dmd = "0";
                        $this->set('dmd', $dmd);
                    }
                }
                else{
                    $dmd = "0";
                    $this->set('dmd', $dmd);
                }
                
                
	}
        function user_accAskInvit()
	{
            $ids = explode("-",$this->params['url']['id']);
            $idPlayer = $ids["0"];
            $idTeam = $ids["1"];
            
            //Verifie que le groupe n'est pas déjà plein
            $this->loadModel('TeamsHasPlayers');
            $teamInfo = $this->TeamsHasPlayers->find("all", array(
                    'conditions' => array(
                            "team_id" => $idTeam,
                            "accepted" => "1",
                    )
            ));
            //Si le groupe à moins de 4joueur
            if (count($teamInfo) < "4"){
                //Verifie que le joueur n'as pas déjà une équipe
                $this->loadModel('TeamsHasPlayers');
                $playerInfo = $this->TeamsHasPlayers->find("all", array(
                        'conditions' => array(
                                "player_id" => $idPlayer,
                                "accepted" => "1",
                        )
                ));
               
                //Si le player a déjà une équipe on refuse
                if ($playerInfo["0"]["TeamsHasPlayers"]["id"] != ""){
                    //Supprime la ligne de la table TeamsHasPlayers
                    $this->loadModel('TeamsHasPlayers');
                    $conditions = array(
                        'accepted' => "2",
                        'team_id' => $idTeam,
                        'player_id' => $idPlayer
                    );

                    $this->TeamsHasPlayers->deleteAll($conditions, false);

                    $this->Session->setFlash("Le joueur à déjà une équipe. La demande à été refusé", 'codefail');
                    $this->redirect(array('controller' => 'users', 'action' => 'user_relations')); 
                }
                //Si le joueur n'as pas déquipe on ajoute
                else{
                    //On accepte je joueur dans le groupe
                    $this->loadModel('TeamsHasPlayers');
                    $this->TeamsHasPlayers->updateAll(
                        array('accepted' => "1"),
                        array('player_id' => $idPlayer, 'team_id' => $idTeam)
                    );
                    $this->Session->setFlash("Le joueur à rejoins votre équipe", 'codesuccess');
                    $this->redirect(array('controller' => 'users', 'action' => 'user_relations')); 
                }
            }
            //Si le groupe a au moins 4 joueur  on refuse
            else{
                //Supprime la ligne de la table TeamsHasPlayers
                $this->loadModel('TeamsHasPlayers');
                $conditions = array(
                    'accepted' => "2",
                    'team_id' => $idTeam,
                    'player_id' => $idPlayer
                );

                $this->TeamsHasPlayers->deleteAll($conditions, false);
                
                $this->Session->setFlash("Le groupe est déjà plein. La demande à été refusé", 'codefail');
                $this->redirect(array('controller' => 'users', 'action' => 'user_relations')); 
            }            
        }
        
        function user_refAskInvit()
	{
            $ids = explode("-",$this->params['url']['id']);
            $idPlayer = $ids["0"];
            $idTeam = $ids["1"];
            
            //Supprime la ligne de la table TeamsHasPlayers
            $this->loadModel('TeamsHasPlayers');
            $conditions = array(
                'accepted' => "2",
                'team_id' => $idTeam,
                'player_id' => $idPlayer
            );

            $this->TeamsHasPlayers->deleteAll($conditions, false);
            $this->Session->setFlash("Vous avez refusé la demande", 'codesuccess');
            $this->redirect(array('controller' => 'users', 'action' => 'user_relations')); 
            
        }
	function securimage($random_number)
	{
		$this->autoLayout = false;

		//a blank layout

		//override variables set in the component - look in component for full list
		$this->captcha->image_height = 75;
		$this->captcha->image_width = 350;
		$this->captcha->image_bg_color = '#ffffff';
		$this->captcha->line_color = '#cccccc';
		$this->captcha->arc_line_colors = '#999999,#cccccc';
		$this->captcha->code_length = 5;
		$this->captcha->font_size = 45;
		$this->captcha->text_color = '#000000';
		ob_clean();
		$this->set('captcha_data', $this->captcha->show());

		//dynamically creates an image


	}

	function verifypassword()
	{
		if (!empty($this->request->params['pass'][0])) {
			$token = $this->request->params['pass'][0];
			$token = explode('-', $token);
			$user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $token[0],
					'MD5(User.password)' => $token[1],
					'active' => 1
				)
			));
			if (!empty($user)) {
				$password = substr(md5(uniqid(rand() , true)) , 0, 8);
				$this->User->id = $user['User']['id'];
				$this->User->saveField('password', Security::hash($password, null, true));
				$this->Session->setFlash("Votre mot de passse a bien ete reinitialisé ,
					voici votre nouveau mot de passe : $password");
			}
			else {
				$this->Session->setFlash("Ce lien n'est pas valide.");
			}
		}
	}

	function password()
	{
		if ($this->request->is('ajax')) {
			if (isset($this->request->query['email'])) {
				$email = $this->request->query['email'];
				$exist = $this->User->find('first', array(
					'conditions' => array(
						'email' => $email,
					)
				));
				if (isset($exist['User']['id'])) {
					$link = Router::url("/users/verifypassword/" . $exist['User']['id'] . '-' . md5($exist['User']['password']) , true);
					App::uses('CakeEmail', 'Network/Email');
					$mail = new CakeEmail();
					$mail->from('info@intripid.fr')->to($exist['User']['email'])->subject('Nouveau mot de passe Intripid')->emailFormat('html')->template('passwordforget')->viewVars(array(
						'username' => $exist['User']['username'],
						'link' => $link
					))->send();

					$message = array(
						'code' => 1,
						'message' => 'Vous avez reçu un email de confirmation. Celui-ci se trouve peut-être dans vos courriers indésirables.'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
				else {
					$message = array(
						'code' => 0,
						'message' => 'Cette adresse mail n\'existe pas.'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
			}
			$message = array(
				'code' => 0,
				'message' => 'Demande mal formée.'
			);
			$ret = json_encode($message);
			echo $ret;
			exit();
		}
		$message = '';
		$this->set('ajax_resp', $message);
	}

	public function user_modifypassword()
	{
		if ($this->request->is('ajax')) {
			if (isset($this->request->query['lastpassword']) && isset($this->request->query['newpassword']) && isset($this->request->query['confnewpassword'])) {
				$lastpassword = $this->request->query['lastpassword'];
				$newpassword = $this->request->query['newpassword'];
				$confnewpassword = $this->request->query['confnewpassword'];
				if ((strlen($newpassword) >= 8) && ($newpassword == $confnewpassword)) {
					$this->loadModel('User');
					$user = $this->Session->read();
					$exist = $this->User->find('first', array(
						'conditions' => array(
							'id' => $user['Auth']['User']['id']
						) ,
						'recursive' => - 1
					));
					if (isset($exist['User']['password']) && $exist['User']['password'] == Security::hash($lastpassword, null, true)) {
						$this->User->id = $user['Auth']['User']['id'];
						$this->User->saveField('password', Security::hash($newpassword, null, true));
						$ret = array(
							'code' => 1
						);
						echo json_encode($ret);
						exit();
					}
				}
			}
		}
		$ret = array(
			'code' => 0
		);
		echo json_encode($ret);
		exit();
	}

	function signup()
	{
		$this->set('captcha_form_url', $this->webroot . 'users/signup');

		//url for the form
		$this->set('captcha_image_url', $this->webroot . 'users/securimage/0');

		//url for the captcha image

		$captcha_success_msg = 'The code you entered matched the captcha';
		$captcha_error_msg = 'The code you entered does not match';
		if (empty($this->data)) {

			//form has not been submitted yet
			$this->set('error_captcha', '');

			//error message displayed to user
			$this->set('success_captcha', '');

			//success message displayed to user
			//$this->render(); //reload page


		}
		else {

			//form was submitted
			if ($this->captcha->check($this->data['Contact']['captcha_code']) == false) {

				//the code was incorrect - display an error message to user
				$this->set('error_captcha', $captcha_error_msg);

				//set error msg
				$this->set('success_captcha', '');

				//set success msg
				//$this->render(); //reload page


			}
			else {

				//the code was correct - display a success message to user
				$this->set('error_captcha', '');

				//set error msg
				$this->set('success_captcha', $captcha_success_msg);

				//set success msg
				//$this->render(); //reload page

				if ($this->request->is('post')) {
					$this->loadModel('Forbidden');
					$d = $this->request->data;
					$d['User']['id'] = null;
					if (isset($d['User']['cgu'])) {
						if (isset($d['User']['username'])) {
							$prohibited = $this->Forbidden->find('first', array(
								'conditions' => array(
									'pseudo' => $d['User']['username']
								) ,
								'recursive' => - 1
							));
							if (isset($prohibited['Forbidden']['pseudo'])) {
								$this->Session->setFlash("Votre pseudo n'est pas correct.");
								return;
							}
						}
						else {
							$this->Session->setFlash("N'oubliez pas votre pseudo.");
							return;
						}
						if ($d['User']['password'] == $d['User']['confpassword'] && $d['User']['cgu'] == 1 && $d['User']['email'] == $d['User']['emailconf']) {
							if (!empty($d['User']['password'])) {
								$psw = $d['User']['password'];
								$d['User']['password'] = Security::hash($d['User']['password'], null, true);
							}
							$d['User']['datenaissance'] = $d['User']['datenaissance']['year'] . "-" . $d['User']['datenaissance']['month'] . "-" . $d['User']['datenaissance']['day'];

							//if($this->User->save($d, true,array('username','password','email'))){

							//if($this->User->save($d, true, array('username','firstname','datenaissance','name','sexe','nationnalite','codepostale','adress','adresscompl','email','password'))){
							if ($this->User->save($d, true, array(
								'username',
								'password',
								'datenaissance',
								'email'
							))) {

								//if ($this->User->validates($d)){

								//$link = array('controller'=>'users','action'=>'activate',$this->User->id.'-'.md5($d['User']['password']));
								$link = Router::url('/users/activate/' . $this->User->id . '-' . md5($d['User']['password']) , true);
                                                                echo "################";
                                                                var_dump($link);

								App::uses('CakeEmail', 'Network/Email');
								$mail = new CakeEmail();
								$mail->from('info@intripid.fr')->to($d['User']['email'])->subject('Inscription Intripid')->emailFormat('html')->template('signup')->viewVars(array(
									'username' => $d['User']['username'],
									'psw' => $psw,
									'link' => $link
								))->send();
								$this->redirect(array(
									'controller' => 'users',
									'action' => 'voirmail',
									'user' => false
								));
								$this->Session->setFlash("Inscription terminée. Vous avez reçu un email de confirmation. Celui-ci se trouve peut-être dans vos courriers indésirables. On vous pardonne ! Veuillez cliquer sur le lien de l'email pour poursuivre l'inscription et devenir un héros");
							}
							else {

								//$errors = $this->validateErrors($this->User);
								//$errors = $this->User->validates();
								//var_dump($errors);
								//debug($this->User->validationErrors);

								$this->Session->setFlash("Merci de corriger vos erreurs");
								$this->set('test', $this->User->validationErrors);
								$this->render();

								//$this->Session->write('Test.test', $errors);
								//$this->set('errors',$errors);
								//$this->Session->delete('Test');


							}
						}
						else {
							if ($d['User']['password'] != $d['User']['confpassword']) {
								$this->Session->setFlash("Votre mot de passe n'a pas été confirmé correctement.");
							}
							elseif ($d['User']['cgu'] != 1) {
								$this->Session->setFlash("Vous devez accepter les CGU pour vous inscrire.");
							}
							else {
								$this->Session->setFlash("Votre email n'a pas été confirmé correctement.");
							}
						}
					}
					else {
						$this->Session->setFlash("Vous devez accepter les CGU pour vous inscrire.");
					}
				}
			}
		}
		$this->loadModel('Contrat');
		$cgu = $this->Contrat->find('first', array(
			'recursive' => 0
		));
		if (!isset($cgu['Contrat']['description'])) {
			$content = 'CGU non disponible';
		}
		else $content = $cgu['Contrat']['description'];
		$this->set('cgu', $content);
	}

	function activate($token)
	{
		$this->loadModel('User');
		$this->LoadModel('Type');
		$types = $this->Type->find('all');
		$this->set(compact('types'));

		$this->layout = "activate";
		$token = explode('-', $token);
		if (!isset($token[1])) exit(-1);
		$user = $this->User->find('first', array(
			'conditions' => array(
				'id' => $token[0],
				'MD5(User.password)' => $token[1],
				'active' => 0
			)
		));

		//debug($user);
		if (!empty($user)) {
			if ($this->request->is('post')) {
				$this->loadModel('Level');
				$this->User->id = $user['User']['id'];
				$this->User->saveField('active', 1);
				$type = $this->request->data;
				$level = $this->Level->find('first', array(
					'conditions' => array(
						'Level.type_id' => $type['classe'],
						'Level.level' => 1
					) ,
					'recursive' => - 1
				));
				$player = array(
					'Player' => array(
						'pseudo' => $user['User']['username'],
						'user_id' => $user['User']['id'],
						'type_id' => $type['classe'],
						'level_id' => $level['Level']['id']
					)
				);
				$this->LoadModel('Player');
				$this->loadModel('Relation');
				$this->Player->create();
				if ($this->Player->save($player)) {

					//$this->Session->setFlash(__('The user has been saved'));
					$pl = $this->Player->find('first', array(
						'conditions' => array(
							'user_id' => $user['User']['id']
						) ,
						'recursive' => - 1
					));

					// if ( isset($pl['Player']['id']) ) {
					// $this->Relation->create();
					// $option = array('user_from' => $pl['Player']['id'], 'user_to' => 33, 'timestamp' => time());
					//$this->Relation->save($option);
					//$this->Relation->create();
					//$option = array('user_from' => $pl['Player']['id'], 'user_to' => 34, 'timestamp' => time());
					//$this->Relation->save($option);
					//$this->Relation->create();
					// $option = array('user_from' => $pl['Player']['id'], 'user_to' => 42, 'timestamp' => time());
					// $this->Relation->save($option);
					//$this->Relation->create();
					//$option = array('user_from' => $pl['Player']['id'], 'user_to' => 90, 'timestamp' => time());
					//$this->Relation->save($option);
					// }
					$link = "http://www.intripid.fr";
					App::uses('CakeEmail', 'Network/Email');
					$mail = new CakeEmail();
					$mail->from('info@intripid.fr')->to($user['User']['email'])->subject('Inscription Intripid')->emailFormat('html')->template('signup')->viewVars(array(
						'username' => $user['User']['username'],
						'link' => $link
					))->send();
					$this->redirect(array(
						'controller' => 'missions',
						'action' => 'carte',
						'user' => true
					));
				}
				else {

					//$this->Session->setFlash(__('The user could not be saved. Please, try again.'));


				}
			}

			//$this->Session->setFlash("Votre compte a bien été activé","notif");
			$this->Auth->login($user['User']);
		}
		else {

			//$this->Session->setFlash("Ce lien d'activation n'est pas valide","notif", array('type'=>'error'));


		}
	}

	function logout()
	{
		$this->Cookie->delete('rememberMe');
		$this->Auth->logout();
		$this->redirect('/');
	}

	function login()
	{
		$session = $this->Session->read();
		if (isset($session['Auth']['User']['id'])) {
			$this->redirect(array(
				'controller' => 'missions',
				'action' => 'carte',
				'user' => true
			));
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->User->id = $this->Auth->user("id");
				$this->User->saveField("lastlogin", date('Y-m-d H:i:s'));
				if ($this->request->data['User']['rememberMe'] == 1) {
					$cookieTime = "12 months";

					// You can do e.g: 1 week, 17 weeks, 14 days
					unset($this->request->data['User']['rememberMe']);
					$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
					$this->Cookie->write('rememberMe', $this->request->data['User'], true, $cookieTime);
				}
				$this->redirect(array(
					'controller' => 'missions',
					'action' => 'carte',
					'user' => true
				));
			}
			else {
				$this->Session->setFlash("Identifiants incorrects", 'codefail');
			}
		}
	}

	public function poplogin()
	{

		//$this->Auth->logout();
		$session = $this->Session->read();
		if (isset($session['Auth']['User']['id'])) {
			$ret = array(
				'code' => 1,
				'message' => 'already connected'
			);
			echo json_encode($ret);
			exit();
		}
		if ($this->request->is('ajax')) {
			$this->request->data['User']['password'] = $this->request->query['password'];
			$this->request->data['User']['username'] = $this->request->query['username'];
			if ($this->request->query['rememberme'] == 'true') $this->request->data['User']['rememberMe'] = 1;
			else $this->request->data['User']['rememberMe'] = 0;
			if ($this->Auth->login()) {
				$this->User->id = $this->Auth->user("id");
				$this->User->saveField("lastlogin", date('Y-m-d H:i:s'));
				if ($this->request->data['User']['rememberMe'] == 1) {
					$cookieTime = "12 months";

					// You can do e.g: 1 week, 17 weeks, 14 days
					unset($this->request->data['User']['rememberMe']);
					$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
					$this->Cookie->write('rememberMe', $this->request->data['User'], true, $cookieTime);
					$cookie = json_encode(array(
						'key' => 'rememberMe',
						'data' => $this->request->data['User'],
						'cookietime' => $cookieTime
					));
				}
				$ret = array(
					'code' => 1,
					'message' => 'Success'
				);
				$this->response->body(json_encode($ret));

				//$this->response->type('json');
				return $this->response;

				//exit();


			}
			else {
				$ret = array(
					'code' => 0,
					'message' => 'identifiants incorrects'
				);
				echo json_encode($ret);
				exit();
			}
		}
		else {
			$ret = array(
				'code' => 0,
				'message' => 'Requête incorrecte'
			);
			echo json_encode($ret);
			exit();
		}
	}

	public function newcompte()
	{
		$this->loadModel('User');
		if ($this->request->is('ajax')) {
			$this->loadModel('Forbidden');
			if ($this->request->query['cgu'] == 'false') {
				$ret = array(
					'code' => 0,
					'message' => 'Vous devez accepter la CGU. Corrigez svp et surtout réinitialisez le captcha'
				);
				echo json_encode($ret);
				exit();
			}
			if ($this->request->query['password'] != "" && strlen($this->request->query['password']) >= 8 && $this->request->query['password'] == $this->request->query['confpassword']) {
				if ($this->captcha->check($this->request->query['captcha']) == false) {
					$ret = array(
						'code' => - 1,
						'message' => 'Captcha erroné'
					);
					echo json_encode($ret);
					exit();
				}
                                $d['User']['fbidamis'] = $this->request->query['fbidamis'];
                                $d['User']['fbid'] = $this->request->query['fbid'];
				$d['User']['password'] = $this->request->query['password'];
				$d['User']['id'] = null;
				$d['User']['username'] = $this->request->query['username'];
				$d['User']['email'] = $this->request->query['email'];
				$d['User']['confemail'] = $this->request->query['confemail'];
				$d['User']['datenaissance'] = $this->request->query['birthday_year'] . "-" . $this->request->query['birthday_month'] . "-" . $this->request->query['birthday_day'];
				if (isset($d['User']['username'])) {
					$pseudo = $this->User->find('first', array(
						'conditions' => array(
							'username' => $d['User']['username']
						) ,
						'recursive' => 0
					));
					if (isset($pseudo['User']['id'])) {
						$ret = array(
							'code' => 0,
							'message' => 'Ce pseudo est déjà attribué. Corrigez svp et surtout réinitialisez le captcha'
						);
						echo json_encode($ret);
						exit();
					}
					$prohibited = $this->Forbidden->find('first', array(
						'conditions' => array(
							'pseudo' => $d['User']['username']
						) ,
						'recursive' => - 1
					));
					if (isset($prohibited['Forbidden']['pseudo'])) {
						$ret = array(
							'code' => 0,
							'message' => 'Votre pseudo n\'est pas correct. Corrigez svp et surtout réinitialisez le captcha'
						);
						echo json_encode($ret);
						exit();
					}

					if ($d['User']['email'] == $d['User']['confemail']) {
						$email = $this->User->find('first', array(
							'conditions' => array(
								'email' => $d['User']['email']
							) ,
							'recursive' => 0
						));
						if (isset($email['User']['id'])) {
							$ret = array(
								'code' => 0,
								'message' => 'Cet email est déjà utilisé. Corrigez svp et surtout réinitialisez le captcha'
							);
							echo json_encode($ret);
							exit();
						}
						if (!empty($d['User']['password'])) {
							$psw = $d['User']['password'];
							$d['User']['password'] = Security::hash($d['User']['password'], null, true);
						}
						else {
							$ret = array(
								'code' => 0,
								'message' => 'Vous devez définir un mot de passe. Corrigez svp et surtout réinitialisez le captcha'
							);
							echo json_encode($ret);
							exit();
						}
					}
					else {
						$ret = array(
							'code' => 0,
							'message' => 'Vos adresses emails ne correspondent pas. Corrigez svp et surtout réinitialisez le captcha'
						);
						echo json_encode($ret);
						exit();
					}
				}
				else {
					$ret = array(
						'code' => 0,
						'message' => 'N\'oubliez pas votre pseudo.Corrigez svp et surtout réinitialisez le captcha'
					);
					echo json_encode($ret);
					exit();
				}
			}
			else {
				if (strlen($this->request->query['password']) < 8) {
					$ret = array(
						'code' => 0,
						'message' => 'Le mot de passe doit faire au moins 8 caractères. Corrigez svp et surtout réinitialisez le captcha'
					);
					echo json_encode($ret);
					exit();
				}
				if ($this->request->query['password'] != $this->request->query['confpassword']) {
					$ret = array(
						'code' => 0,
						'message' => 'Les mots de passe ne correspondent pas. Corrigez svp et surtout réinitialisez le captcha'
					);
					echo json_encode($ret);
					exit();
				}
			}
		$data = $this->save_newcompte($d);


			//$this->User->save($d, true, array('username','password','datenaissance','email'));
			//$data['User']['id']=1;
			if ($data['User']['id'] != null) {
				$code = $this->User->id . '-' . md5($d['User']['password']);
				$ret = array(
					'code' => 1,
					'message' => 'success',
					'code_activation' => $code
				);
				$ec = json_encode($ret);
				print $ec;
				exit();
			}
			else {
				$ret = array(
					'code' => 0,
					'message' => 'Erreur. Veuillez vérifier tous les champs, y compris la date de naissance, et surtout réinitialiser le captcha'
				);
				echo json_encode($ret);
				exit();
			}
		}
		$ret = array(
			'code' => 0,
			'message' => 'Requête incorrecte'
		);
		echo json_encode($ret);
		exit();
	}

	public function user_concept()
	{
		$this->layout = null;
	}

	public function user_uploadJQ()
	{
		$user = $this->Session->read();
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 0
		));
		$this->loadModel('Mission');
		$options_missions = array(
			'fields' => 'id,title,short_description,descrition,lat,longitude,nb_like,pointsucces,partner_id,media_id,missionnature_id,Missionnature.name,Missionnature.slug,Missiondifficulty.name,Missionhistoire.name,Missiontheme.name,Partner.id,Partner.partnername',
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missionsDB = $this->Mission->find('all', $options_missions);

		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache');
		header('Content-Disposition: inline; filename="files.json"');
		App::import('Vendor', 'UploadHandler', array(
			'file' => 'UploadHandler/UploadHandler.php'
		));
		$upload_handler = new UploadHandler();
		$path = "profil/" . $player['Player']['id'] . "/";
		$path_thumb = "profil/thumb/" . $player['Player']['id'] . "/";
		$upload_handler->config($path, $path_thumb);
		CakeLog::write('debug', print_r($_SERVER, true));
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'OPTIONS':
			case 'HEAD':
				$upload_handler->head();
				break;

			case 'GET':
				$upload_handler->get();
				break;

			case 'PATCH':
			case 'PUT':
			case 'POST':
				$result = $upload_handler->post($player['Player']['id']);
				if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
					$time = time();
					$location = "/img/uploads/" . $path . $result['files']['0']->name;
					$location_thumb = "/img/uploads/" . $path_thumb . $result['files']['0']->name;
					$this->loadModel('Media');
					$record = $this->Media->find('first', array(
						'conditions' => array(
							'ref' => 'User',
							'userid' => $user['Auth']['User']['id']
						) ,
						'recursive' => 0
					));

					$this->Media->create();
					$option = array(
						'ref' => 'User',
						'userid' => $player['Player']['id'],
						'ref_id' => $player['Player']['id'],
						'file' => $location,
						'thumb' => $location_thumb,
						'time' => $time,
						'position' => NULL,
						'like' => 0,
						'dislike' => 0
					);
					$this->Media->save($option);

					$record = $this->Media->find('first', array(
						'conditions' => array(
							'ref' => 'User',
							'userid' => $player['Player']['id']
						) ,
						'order' => array(
							'id' => 'DESC'
						) ,
						'recursive' => 0
					));
					if (isset($record['Media']['id'])) {
						$this->Player->id = $player['Player']['id'];
						$this->Player->saveField('profil_id', $record['Media']['id']);
					}
				}
				break;

			case 'DELETE':
				$upload_handler->delete();
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
		}
		exit;
	}

	public function user_uploadJQ_missiondone($id = null)
	{
		if ($id == null) exit;
		CakeLog::write("debug", "in mission done");
		$user = $this->Session->read();
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 0
		));

		$this->loadModel('Mission');
		$options_missions = array(
			'conditions' => array(
				'Mission.id' => $id
			) ,
			'recursive' => 0
		);
		$mission_submit = $this->Mission->find('first', $options_missions);
		if ($mission_submit['Mission']['id'] != $id) exit;

		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache');
		header('Content-Disposition: inline; filename="files.json"');
		App::import('Vendor', 'UploadHandler', array(
			'file' => 'UploadHandler/UploadHandler.php'
		));
		$upload_handler = new UploadHandler();
		$time = time();
		$year = date('Y', $time);
		$month = date('m', $time);
		$path = "missiondone/" . $year . "/" . $month . "/";
		$path_thumb = "missiondone/" . $year . "/" . $month . "/thumb/";
		$upload_handler->config($path, $path_thumb);
		CakeLog::write("debug", print_r($path, true));
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'OPTIONS':
			case 'HEAD':
				$upload_handler->head();
				break;

			case 'GET':
				$upload_handler->get();
				break;

			case 'PATCH':
			case 'PUT':
			case 'POST':
				$result = $upload_handler->post($player['Player']['id']);
				if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
					$location = "/img/uploads/" . $path . $result['files']['0']->name;
					$location_thumb = "/img/uploads/" . $path_thumb . $result['files']['0']->name;
					$this->loadModel('Media');
					$this->Media->create();
					CakeLog::write("debug", $player['Player']['id']);
					$option = array(
						'ref' => 'Missiondone',
						'userid' => $player['Player']['id'],
						'ref_id' => $id,
						'file' => $location,
						'thumb' => $location_thumb,
						'time' => $time,
						'position' => NULL,
						'like' => 0,
						'dislike' => 0
					);
					$this->Media->save($option);
				}
				break;

			case 'DELETE':
				$upload_handler->delete();
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
		}
		exit;
	}

	public function user_uploadJQ_cadeau($id = null)
	{
		if ($id == null) exit;

		$user = $this->Session->read();
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 0
		));

		$this->loadModel('Cadeaux');
		$options_cadeau = array(
			'conditions' => array(
				'Cadeaux.id' => $id
			) ,
			'recursive' => 0
		);
		$cadeau_submit = $this->Cadeaux->find('first', $options_cadeau);
		if ($cadeau_submit['Cadeaux']['id'] != $id) exit;

		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache');
		header('Content-Disposition: inline; filename="files.json"');
		App::import('Vendor', 'UploadHandler', array(
			'file' => 'UploadHandler/UploadHandler.php'
		));
		$upload_handler = new UploadHandler();
		$time = time();
		$year = date('Y', $time);
		$month = date('m', $time);
		$path = "cadeaux/" . $year . "/" . $month . "/";
		$path_thumb = "cadeaux/" . $year . "/" . $month . "/thumb/";
		$upload_handler->config($path, $path_thumb);
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'OPTIONS':
			case 'HEAD':
				$upload_handler->head();
				break;

			case 'GET':
				$upload_handler->get();
				break;

			case 'PATCH':
			case 'PUT':
			case 'POST':
				$result = $upload_handler->post($player['Player']['id']);
				if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
					$location = "/img/uploads/" . $path . $result['files']['0']->name;
					$location_thumb = "/img/uploads/" . $path_thumb . $result['files']['0']->name;
					$this->loadModel('Media');
					$this->Media->create();
					$option = array(
						'ref'      => 'Cadeaux',
						'userid'   => $player['Player']['id'],
						'ref_id'   => $id,
						'file'     => $location,
						'thumb'    => $location_thumb,
						'time'     => $time,
						'position' => NULL,
						'like'     => 0,
						'dislike'  => 0
					);
					$this->Media->save($option);
				}
				break;

			case 'DELETE':
				$upload_handler->delete();
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
		}
		exit;
	}

	public function user_uploadJQ_mission($id = null)
	{
		if ($id == null) exit;

		$user = $this->Session->read();
		if ($user['Auth']['User']['role'] != 'admin') exit;
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 0
		));

		/*$this->loadModel('Mission');
		$options_missions = array('conditions' => array('Mission.id' => $id),'recursive'=> 0 );
		$mission_submit=$this->Mission->find('first', $options_missions);
		if ( $mission_submit['Mission']['id'] != $id ) exit;*/

		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache');
		header('Content-Disposition: inline; filename="files.json"');
		App::import('Vendor', 'UploadHandler', array(
			'file' => 'UploadHandler/UploadHandler.php'
		));
		$upload_handler = new UploadHandler();
		$time = time();
		$year = date('Y', $time);
		$month = date('m', $time);
		$path = "mission/" . $year . "/" . $month . "/";
		$path_thumb = "mission/" . $year . "/" . $month . "/thumb/";
		$upload_handler->config($path, $path_thumb);
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'OPTIONS':
			case 'HEAD':
				$upload_handler->head();
				break;

			case 'GET':
				$upload_handler->get();
				break;

			case 'PATCH':
			case 'PUT':
			case 'POST':
				$result = $upload_handler->post($player['Player']['id']);
				if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
					$location = "/img/uploads/" . $path . $result['files']['0']->name;
					$location_thumb = "/img/uploads/" . $path_thumb . $result['files']['0']->name;
					$this->loadModel('Media');
					$this->Media->create();
					$option = array(
						'ref'      => 'Mission',
						'userid'   => $player['Player']['id'],
						'ref_id'   => $id,
						'file'     => $location,
						'thumb'    => $location_thumb,
						'time'     => $time,
						'position' => NULL,
						'like'     => 0,
						'dislike'  => 0,
						'visible'  => 0
					);
					$this->Media->save($option);
				}
				break;

			case 'DELETE':
				$upload_handler->delete();
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
		}
		exit;
	}

	public function user_uploadJQ_partner($id = null)
	{
		if ($id == null) exit;

		$user = $this->Session->read();
		if ($user['Auth']['User']['role'] != 'admin') exit;
		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 0
		));

		/*$this->loadModel('Mission');
		$options_missions = array('conditions' => array('Mission.id' => $id),'recursive'=> 0 );
		$mission_submit=$this->Mission->find('first', $options_missions);
		if ( $mission_submit['Mission']['id'] != $id ) exit;*/

		header('Pragma: no-cache');
		header('Cache-Control: private, no-cache');
		header('Content-Disposition: inline; filename="files.json"');
		App::import('Vendor', 'UploadHandler', array(
			'file' => 'UploadHandler/UploadHandler.php'
		));
		$upload_handler = new UploadHandler();
		$time = time();
		$year = date('Y', $time);
		$month = date('m', $time);
		$path = "partner/" . $year . "/" . $month . "/";
		$path_thumb = "partner/" . $year . "/" . $month . "/thumb/";
		$upload_handler->config($path, $path_thumb);
		CakeLog::write('debug', print_r($_SERVER, true));
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'OPTIONS':
			case 'HEAD':
				$upload_handler->head();
				break;

			case 'GET':
				$upload_handler->get();
				break;

			case 'PATCH':
			case 'PUT':
			case 'POST':
				CakeLog::write('debug', "toto");
				$result = $upload_handler->post($player['Player']['id']);
				CakeLog::write('debug', print_r($result, true));
				if (!isset($result['files']['0']->error) && isset($result['files']['0']->url)) {
					$location = "/img/uploads/" . $path . $result['files']['0']->name;
					$location_thumb = "/img/uploads/" . $path_thumb . $result['files']['0']->name;
					$this->loadModel('Media');
					$this->Media->create();
					$option = array(
						'ref'      => 'Partner',
						'userid'   => $player['Player']['id'],
						'ref_id'   => $id,
						'file'     => $location,
						'thumb'    => $location_thumb,
						'time'     => $time,
						'position' => NULL,
						'like'     => 0,
						'dislike'  => 0,
						'visible'  => 0
					);
					$this->Media->save($option);
				}
				break;

			case 'DELETE':
				$upload_handler->delete();
				break;

			default:
				header('HTTP/1.1 405 Method Not Allowed');
		}
		exit;
	}

	public function user_cadeau_detail($id = null)
	{
		$this->loadModel('Cadeaux');
		$this->loadModel('User');
		$user = $this->Session->read();
		if (!isset($user['Auth']['User']['id'])) {
			$user['Auth']['User']['id'] = 1;
		}
		else {
			$exist_user = $this->User->find('first', array(
				'conditions' => array(
					'id' => $user['Auth']['User']['id']
				) ,
				'recursive' => - 1
			));
			if (!isset($exist_user['User']['id'])) $user['Auth']['User']['id'] = 1;
		}
		$user_id = $user['Auth']['User']['id'];

		$this->loadModel('Player');
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user_id
			) ,
			'recursive' => - 1
		));
		$playerid = $player['Player']['id'];

		if ($this->request->is('ajax')) {
			if ($this->request->query['idCadeau'] != '') {
				$idCadeau = $this->request->query['idCadeau'];
				$cadeau_data = $this->Cadeaux->find('first', array(
					'conditions' => array(
						'Cadeaux.id' => $idCadeau
					) ,
					'recursive' => - 1
				));
				if (isset($cadeau_data['Cadeaux']['id'])) {
					$this->loadModel('Partner');
					$this->loadModel('Media');
					$this->loadModel('Mission');
					$this->loadModel('Coupon');
					$coupon = $this->Coupon->find('first', array(
						'conditions' => array(
							'Coupon.player_id' => $playerid,
							'Coupon.cadeau_id' => $idCadeau
						) ,
						'recursive' => - 1
					));
					$status = 0;
					$valide = 0;
					$coupon_pdf = "";
					if (isset($coupon['Coupon']['id'])) {
						$current_time = time();
						if ($current_time <= ($coupon['Coupon']['time'] + ($cadeau_data['Cadeaux']['delai'] * 86400))) {
							$statut = 1;
							$valide = $coupon['Coupon']['time'] + ($cadeau_data['Cadeaux']['delai'] * 86400);
							$valide = date('d/m/Y', $valide);
							$coupon_pdf = $coupon['Coupon']['coupon'];
						}
						if ($coupon['Coupon']['utilisation'] >= $cadeau_data['Cadeaux']['utilisable']) {
							$status = - 1;
							$valide = $coupon['Coupon']['time'] + ($cadeau_data['Cadeaux']['delai'] * 86400);
							$valide = date('d/m/Y', $valide);
							$coupon_pdf = $coupon['Coupon']['coupon'];
						}
					}
					$partner_info = $this->Partner->find('first', array(
						'conditions' => array(
							'Partner.id' => $cadeau_data['Cadeaux']['id_partenaire']
						) ,
						'recursive' => - 1
					));
					if ($partner_info['Partner']['media_id'] != NULL && $partner_info['Partner']['media_id'] != 0) {
						$partner_logo = $this->Media->find('first', array(
							'conditions' => array(
								'Media.id' => $partner_info['Partner']['media_id']
							) ,
							'recursive' => - 1
						));
						$partner_logo = $partner_logo['Media']['file'];
					}
					else $partner_logo = null;
					if ($cadeau_data['Cadeaux']['id_mission'] == null) $mission_id = null;
					else $mission_id = $cadeau_data['Cadeaux']['id_mission'];
					if ($mission_id != null) {
						$mission = $this->Mission->find('first', array(
							'conditions' => array(
								'Mission.id' => $mission_id
							) ,
							'recursive' => 1
						));
						$mission_title = $mission['Mission']['title'];
						$mission_nature = $mission['Missionnature']['slug'];
					}
					else {
						$mission_title = null;
						$mission_nature = null;
					}

					if (preg_match('[url]', $partner_info['Partner']['adress'] ))
                      $partner_info['Partner']['adress']= preg_replace('/\[url\](.+?)\[\/url\]/', '<a href="\1">\1</a>', $partner_info['Partner']['adress']);

                  	elseif (preg_match('[googlemaps]', $partner_info['Partner']['adress'])) {
                      $partner_info['Partner']['adress']= preg_replace('/\[googlemaps\](.+?)\[\/googlemaps\]/', '<a href="http://maps.google.com/maps?q=\1">\1</a>', $partner_info['Partner']['adress']);
                  	}

                  	else
                 		$partner_info['Partner']['adress']='<a href="'.$partner_info['Partner']['adress'].'">'.$partner_info['Partner']['adress'].'</a>';
					$cadeau_pict = $this->Media->find('list', array(
						'fields' => array(
							'Media.id',
							'Media.file'
						) ,
						'conditions' => array(
							'Media.ref_id' => $cadeau_data['Cadeaux']['id'],
							'Media.ref' => 'Cadeaux'
						) ,
						'recursive' => - 1
					));
					$data = array(
						'code'              => 1,
						'id'                => $cadeau_data['Cadeaux']['id'],
						'partnername'       => $partner_info['Partner']['partnername'],
						'partnerlogo'       => $partner_logo,
						'player_pseudo'     => $player['Player']['pseudo'],
						'player_id'         => $playerid,
						'type'              => $cadeau_data['Cadeaux']['type'],
						'niveau'            => $cadeau_data['Cadeaux']['niveau'],
						'delai'             => $cadeau_data['Cadeaux']['delai'],
						'short_description' => $cadeau_data['Cadeaux']['short_description'],
						'description'       => $cadeau_data['Cadeaux']['description'],
						'status'            => $status,
						'valide'            => $valide,
						'detail_vignette'   => $cadeau_data['Cadeaux']['detail_vignette'],
						'adresse'           => $partner_info['Partner']['adress'],
						'mission_id'        => $mission_id,
						'coupon'            => $coupon_pdf,
						'mission_title'     => $mission_title,
						'mission_nature'    => $mission_nature,
						'pictures'          => $cadeau_pict
					);
					$ret = json_encode($data);
					echo $ret;
					exit();
				}
				else {
					$message = array(
						'code' => - 1,
						'message' => 'Cadeau inexistant'
					);
					$ret = json_encode($message);
					echo $ret;
					exit();
				}
			}
			else {
				$message = array(
					'code' => - 1,
					'message' => 'Requete invalide'
				);
				$ret = json_encode($message);
				echo $ret;
				exit();
			}
		}
		else {
			$message = array(
				'code' => - 1,
				'message' => 'Requete invalide'
			);
			$ret = json_encode($message);
			echo $ret;
			exit();
		}
	}

	public function user_team($id = null)
	{
		$this->set("title_for_layout", "Equipe");
		$this->set("show_concept", 0);

		if ($this->request->is('post')) {
			$data = $this->request->data;
			if (isset($data['codemission'])) {
				$this->handle_code($data);
			}
		}

		$this->loadModel('Player');
		$user = $this->Session->read();
		$player = $this->Player->find('first', array(
			'conditions' => array(
				'user_id' => $user['Auth']['User']['id']
			) ,
			'recursive' => 1
		));

		$this->set('player', $player);
		$this->SysNotification->computeCount($player);
		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $player['Player']['profil_id']
			) ,
			'recursive' => 0
		));

		$this->loadModel('Type');
		$dbtypes = $this->Type->find('all', array(
			'recursive' => - 1
		));
		$avatar_gauche = rand(0, count($dbtypes) - 1);
		$avatar_droite = rand(0, count($dbtypes) - 1);
		while ($avatar_droite == $avatar_gauche) {
			$avatar_droite = rand(0, count($dbtypes) - 1);
		}
		$this->set('avatar_droite', $dbtypes[$avatar_droite]['Type']['image' . $player['Level']['palier']]);
		$this->set('avatar_gauche', $dbtypes[$avatar_gauche]['Type']['image' . $player['Level']['palier']]);

		//*****charger les info sur tous les utilisateurs*******
		$precords = array();
		$playerDB = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id, Player.experience,Level.name,Level.id,Level.palier,Level.level,Player.pseudo,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini,Type.image4,Type.image5,Type.image6,Media.thumb',
			'recursive' => 1,
		));

		/********** experience et classement en cours des joueurs de l'equipe**********/
		$this->loadModel('Mission');
		$this->Mission->virtualFields['experienceCalc']=0;
        $this->Mission->virtualFields['expPlayerId']=0;

		$sql_players = "SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Mission__experienceCalc, md.player_id AS Mission__expPlayerId
		             FROM      missions     AS m,
		                       missiondones AS md,
		                       players AS p,
		                       saison AS s
		             WHERE     md.mission_id = m.id
		             AND       md.player_id = p.id
		             AND       DATE(NOW()) BETWEEN s.date_debut AND s.date_fin
		             AND        FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND s.date_fin
		             GROUP BY  md.player_id
		             ORDER BY Mission__experienceCalc DESC";

        $classement_Pl_encours = $this->Mission->query($sql_players);

        foreach ($classement_Pl_encours as $key => $value)
			{
				$ExpByPlayer[$value['Mission']['expPlayerId']] = array(
					'EXP_encours' => $value['Mission']['experienceCalc'],
					'classement_encours' => $key+1,
					);

			$this->set('ExpByPlayer', $ExpByPlayer);
			};
        /****************************************/

		foreach ($playerDB as $pl) {
			$victoires = count($pl['Missiondone']);
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';
                        /*

                        $db = mysql_connect('localhost', 'root', '');
                        mysql_select_db('intripid',$db);
                        $sql = 'SELECT i18n.content
                        FROM i18n
                        LEFT JOIN levels
                        ON i18n.foreign_key=levels.id
                        WHERE levels.id = "'.$pl['Player']['pseudo'].'";';
                        echo $sql;
                        $req = mysql_query($sql) or die('Erreur SQL !<br>'.$sql.'<br>'.mysql_error());
                        while($data = mysql_fetch_assoc($req))
                        {
                        // on affiche les informations de l'enregistrement en cours
                        //echo '<b>'.$data['content'];
                        }
                        */

			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name']."a",
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'level' => $pl['Level']['level'],
				'experience' => $pl['Player']['experience']
			);
			array_push($precords, $record);
		}
		$playerDB = Hash::combine($precords, '{n}.id', '{n}');
		$this->set('playerdb', $playerDB);

		$this->loadModel('TeamsHasPlayers');
		$my_team = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $player['Player']['id'],
				'accepted' => 1
			) ,
			'recursive' => 1
		));
		$this->set('my_team', $my_team);
		$owner_id = $this->TeamsHasPlayers->find('first', array(
			'fields' => 'TeamsHasPlayers.player_id',
			'conditions' => array(
                 'accepted' => 1,
                 'captain'  => 1
			) 
			));
		$owner = $this->Player->find('first',array(
								'conditions' => array(
									'Player.id' => $owner_id['TeamsHasPlayers']['player_id']
									),
								'recursive' => 1
								));
		//owner => le capitain de l'equipe
		$this->set('owner', $owner);


		$this->loadModel('Team');

		if ($id == NULL) {
			if (isset($this->request->params['name'])) {
				$name = $this->validate_string_input(str_replace("-", "%", $this->request->params['name']));
				$equipe = $this->Team->find('first', array(
					'conditions' => array(
						'name LIKE' => $name
					) ,
					'recursive' =>  1
				));
				if (isset($equipe['Team']['id']))
					$id = $equipe['Team']['id'];
			}
		}
		$this->set('equipe', $equipe);

		$this->loadModel('Media');
		$profil = $this->Media->find('first', array(
			'conditions' => array(
				'id' => $owner['Player']['profil_id']
			) ,
			'recursive' => 0
		));
		$membreTeam = $this->TeamsHasPlayers->find('all', array(
			'conditions' => array(
				'team_id' => $equipe['Team']['id'],
                                'accepted' => '1'
			) ,
			'recursive' => 1
		));
		$this->set('membreTeam', $membreTeam);

        /********* infos joueurs d'equipe *******/

        //calculer le nombre de missions reussi la saison en cours
        $this->loadmodel('Saison');
        $this->loadmodel('Missiondone');
        $lastSaison = $this->Saison->find('first',array(
        							'order' => array(
                        			'id' => 'desc'
                    					))
        							);
        $debut_lastSaison = strtotime($lastSaison['Saison']['date_debut']);
        $fin_lastSaison = strtotime($lastSaison['Saison']['date_fin']);

		$records = array();
		$precords = array();
		$mrecords = array();
		$victoire_team = 0;
		foreach($membreTeam as $m){

		$joueur_team = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id, Player.experience,Level.name,Level.id,Level.palier,Level.level,Player.pseudo,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini,Type.image4,Type.image5,Type.image6,Media.thumb',
			'conditions' => array(
				'Player.id' => $m['TeamsHasPlayers']['player_id'])
			));
		foreach ($joueur_team as $pl) {
			$victoires = $this->Missiondone->find('count',array(
									'fields' => array(
										'Missiondone.id'
									) ,
        							'conditions' => array(
	                        			'Missiondone.player_id' => $pl['Player']['id'],
	                    				'Missiondone.time >='	=> $debut_lastSaison,
	                    				'Missiondone.time <='	=> $fin_lastSaison,
	                    				'sucess' => 1
                    				)
                    				));

			//$victoires = count($pl['Missiondone']);
			//fin calcul missiondone
			if (isset($pl['Media']['thumb'])) $profil_pict = $pl['Media']['thumb'];
			else $profil_pict = $pl['Type']['image' . $pl['Level']['palier'] . '_mini'];
			$avatar = 'image' . $pl['Level']['palier'];
			$avatar_mini = 'image' . $pl['Level']['palier'] . '_mini';


			$record = array(
				'id' => $pl['Player']['id'],
				'pseudo' => $pl['Player']['pseudo'],
				'classe' => $pl['Type']['name'],
				'grade' => $pl['Level']['name'],
				'avatar' => $pl['Type'][$avatar],
				'avatar_mini' => $pl['Type'][$avatar_mini],
				'profil_pict' => $profil_pict,
				'victoires' => $victoires,
				'level' => $pl['Level']['level'],
				'experience' => $pl['Player']['experience']
			);
			array_push($precords, $record);
		$joueur_team = Hash::combine($precords, '{n}.id', '{n}');
		}
		$victoire_team = $victoire_team + $victoires;
		}
		$this->set('joueur_team', $joueur_team);
		$this->set('victoire_team', $victoire_team);


		if (isset($profil['Media'])) {
			$this->set('player_profil_picture', $profil['Media']['file']);
			$this->set('player_profil_thumb_picture', $profil['Media']['thumb']);
		}
		else {
			$avatar_mini = 'image' . $player['Level']['palier'] . '_mini';
			$this->set('player_profil_picture', $player['Type'][$avatar_mini]);
			$this->set('player_profil_thumb_picture', $player['Type'][$avatar_mini]);
		}

		$profil_pictures = $this->Media->find('all', array(
			'conditions' => array(
				'Media.userid' => $owner['Player']['id'],
				'Media.ref' => 'User',
				'Media.visible' => 1
			) ,
			'recursive' => - 1
		));
		$this->set('profil_pictures', $profil_pictures);

		$idplayer = $owner['Player']['id'];

		/****** experience et classement en cours de l'equipe *****/

		$this->Team->virtualFields['teamId'] = 0;
		$this->Team->virtualFields['ExperienceEquipe'] = 0;

		$rows_team_exp = $this->Team->query("
			SELECT sum(if(md.sucess=1, m.pointsucces, m.pointfail)) AS Team__ExperienceEquipe, thp.team_id AS Team__teamId 
			FROM teams_has_players AS thp, missions AS m,saison AS s, missiondones AS md 
			WHERE thp.`player_id` = md.player_id
            AND   md.mission_id = m.id
			AND   thp.`accepted` = 1
            AND   DATE(NOW()) BETWEEN s.date_debut AND s.date_fin
		    AND   FROM_UNIXTIME(md.time) BETWEEN s.date_debut AND s.date_fin
			GROUP BY thp.team_id 
			ORDER BY Team__ExperienceEquipe DESC;"
			);

		foreach ($rows_team_exp as $key => $infoExp)
		{	
			$ids_exp='"'.$infoExp['Team']['teamId'].'",';
			$exp_byTeamId[$infoExp['Team']['teamId']]=$infoExp['Team']['ExperienceEquipe'];
			$classementByTeam[$infoExp['Team']['teamId']] = $key+1;

		}

		$this->set('classementByTeam', $classementByTeam);
		$this->set('exp_byTeamId', $exp_byTeamId);
		
		/************************************************/
		$ids_exp = rtrim($ids_exp,',');

		if($ids_exp)
			$sql=array(
				'order'=>array('FIELD(Team.id, '.$ids_exp.') ASC')
			);
		$rows_teams = $this->Team->find('all',$sql);

		$this->set('rows_teams', $rows_teams);

		$my_teamAtt = $this->TeamsHasPlayers->find('first', array(
			'conditions' => array(
				'player_id' => $player['Player']['id'],
                                'accepted' => '1'
			) ,
			'recursive' => 1
		));



		//*********affichage nombre de missiondon,succé*******

		$this->loadModel('Commentaire');
		$session = $this->Session->read();
/*		$this->loadModel('Missiondone');
		$this->loadModel('Mission');*/
		$this->loadModel('Media');

		$options = array(
			'conditions' => array(
				'active' => 1
			) ,
			'recursive' => 0
		);
		$missions = $this->Mission->find('all', $options);
		$this->set('missions', $missions);

		$mrecord = array();
		$mrecords = array();
		$grecord = array();
		$grecords = array();
		foreach ($equipe['TeamsHasPlayers'] as $team ) {

		$idplayer = $team['player_id'];
		if ($team['accepted'] == 1) {
		
		$missiondoneBy_player = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer,
				'Missiondone.time >='	=>$debut_lastSaison,
	            'Missiondone.time <='	=>$fin_lastSaison
			)
		));
		    $missiondones = $missiondones + $missiondoneBy_player ;

		/*$missiondonesucessBy_player = $this->Missiondone->find('count', array(
			'fields' => array(
				'Missiondone.id'
			) ,
			'conditions' => array(
				'Missiondone.player_id' => $idplayer,
				'Missiondone.sucess' => 1
			)
		));
		$missiondonesucess = $missiondonesucess + $missiondonesucessBy_player;*/

		$missiondonelist = $this->Missiondone->find('all', array(
			'conditions' => array(
				'Missiondone.player_id' => $idplayer
			) ,
			'order' => 'time DESC'
		));
                
                $lang = Configure::read('Config.language');                
                $this->loadModel('Trad');
		
		foreach ($missiondonelist as $m) {
                    $tradMissDonelists = $this->Trad->find('first', array(
                            'conditions' => array(
                                    'model' => "Mission",
                                    'field' => "title",
                                    'foreign_key' => $m['Mission']['id'],
                                    'locale' => $lang
                            ) ,
                            'recursive' => - 1
                    ));
			$mrecord = array(
				'succes' => $m['Missiondone']['sucess'],
				'missiondon_id' => $m['Missiondone']['mission_id'],
				'mission_title' => $tradMissDonelists['Trad']['content'],
				'missiondone_time' => $m['Missiondone']['time']

				);
		array_push($mrecords, $mrecord);
		}
                //echo "<pre>";print_r($mrecords);exit;

		//******* gallerie photos des players de l'equipe*****

		$dbownergallery = $this->Media->find('all', array(
			'conditions' => array(
				'ref' => 'Missiondone',
				'userid' => $idplayer
			) ,
			'order' => 'time DESC',
			'recursive' => - 1
		));
		foreach ($dbownergallery as $pic) {
			$grecord = array(
				'file' => $pic['Media']['file'],
				'media_id' => $pic['Media']['id'],
				'like' => $pic['Media']['like'],
				'dislike' => $pic['Media']['dislike'],
				'ref_id' => $pic['Media']['ref_id'],
				'time' => $pic['Media']['time']

				);
		array_push($grecords, $grecord);
		}
		}



		}
		//var_dump($missiondonelist);

		$this->set(compact('missiondones', 'missiondonesucess'));
		$this->set('missiondonelist', $mrecords);
		$this->set('dbownergallery', $grecords);
		

		//*******info les memebres de l'equipe*******
		/*$playerDB = $this->Player->find('all', array(
			'fields' => 'Player.profil_id,Player.id,Player.type_id, Player.experience,Level.name,Level.id,Level.palier,Level.level,Player.pseudo,Type.name,Type.image1,Type.image1_mini,Type.image2,Type.image2_mini,Type.image3,Type.image3_mini,Type.image4,Type.image5,Type.image6,Media.thumb',
			));*/



		//********commentaires*********

		$this->loadModel('Profilcomm');
		$mediaComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Media',
				'Commentaire.team_id' => $equipe['Team']['id']
			) ,
			'recursive' => - 1
		));
		$missionComm = $this->Commentaire->find('list', array(
			'fields' => array(
				'Commentaire.ref_id',
				'Commentaire.time'
			) ,
			'conditions' => array(
				'Commentaire.ref' => 'Mission',
				'Commentaire.team_id' => $equipe['Team']['id']
			) ,
			'recursive' => - 1
		));

		$profil_comments = NULL;
		$profil_comments = $this->Profilcomm->find('all', array(
			'conditions' => array(
				"Profilcomm.teamTo_id" => $equipe['Team']['id']
			) ,
			'order' => 'time DESC',
			'recursive' => - 1
		));
		$this->set('profil_comments', $profil_comments);

		//********infos sur les missionsdones******

		$missionTitle = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.title'
			) ,
			'recursive' => - 1
		));
		$missionSDesc = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Mission.short_description'
			) ,
			'recursive' => - 1
		));
		$missionNat = $this->Mission->find('list', array(
			'fields' => array(
				'Mission.id',
				'Missionnature.slug'
			) ,
			'recursive' => 1
		));
		$this->set('missionTitle', $missionTitle);
		$this->set('missionsSDesc', $missionSDesc);
		$this->set('missionNat', $missionNat);
		$this->set('mdb', $mDB);

		//$this->sendJson('KO', 'user_handle_modteam', $info);
		$this->loadModel('Relation');
		$friendslist = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'Relation.user_from' => $player['Player']['id']
			) ,
			'recursive' => - 1
		));
		$friends = array();
		$friends_not = array();
		foreach ($friendslist as $f) {
			array_push($friends, array(
				'Relation.user_from' => $f
			));
			array_push($friends_not, array(
				'Relation.user_to' => $f
			));
		}
		$propfriends = $this->Relation->find('list', array(
			'fields' => array(
				'Relation.user_to',
				'Relation.user_to'
			) ,
			'conditions' => array(
				'AND' => array(
					'OR' => $friends,
					'NOT' => $friends_not
				)
			) ,
			'recursive' => 1
		));
		$this->set('playersDB', $playerDB);
		$this->set('friends', $friendslist);
		$this->set('propfriends', $propfriends);






	}

	/**
	 * admin_index method
	 *
	 * @return void
	 */
	public function admin_index()
	{
		$this->layout = "admin";
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array(
			'conditions' => array(
				'User.' . $this->User->primaryKey => $id
			)
		);
		$this->set('mission', $this->User->find('first', $options));
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		$this->layout = "admin";
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array(
					'action' => 'index'
				));
			}
			else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		else {
			$options = array(
				'conditions' => array(
					'User.' . $this->User->primaryKey => $id
				)
			);
			$this->request->data = $this->User->find('first', $options);
		}
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		$this->layout = "admin";
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array(
				'action' => 'index'
			));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array(
			'action' => 'index'
		));
	}

	private function sendJson($statut, $action, $info = false)
	{
		$info['statut'] = $statut;
		$info['action'] = $action;
		echo json_encode($info);
		exit;
	}
}
